package com.creo.gostories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import java.util.concurrent.TimeUnit;

/**
 * Created by NishantThite on 27/08/15.
 */
public class AudioPlayerTest extends View implements SeekBar.OnSeekBarChangeListener
{
    final  String playBtnTitle = CommonVariable.playBtnTitle;
    final  String pauseBtnTitle = CommonVariable.pauseBtnTitle;
    static  Context apContext;
    static SeekBar audioSeekBar;
    BroadcastReceiver receiver, mediaPlayerErrorReceiver;
    ProgressDialog pdInAudioPlayer;
    Activity ownerActivity;
    public TextView bufferTextView;
    public TextView bufferText;
    public TextView playTextView;
    public TextView playText;
    public Button playOrPauseBtn;
    static String SongUrl = "";
    static String progress = "";
    private double  timeElapsed1 = CommonVariable.timeElapsed;
    public Button stopPlayer;
    RelativeLayout audioPlayerLayout;
    public LinearLayout linearLayout;
    static boolean dialogOnce = true;
    static boolean dialogExceptionOnce = true;


    public AudioPlayerTest(Context context) {
        super(context);
        this.apContext = context;
    }

    public void playAudioFile(){
            showMyPD();
          //  Log.d(PlayAudioService.LOG_ID, "........PLAY clicked......");
            Intent objIntent = new Intent(apContext, PlayAudioService.class);
            ownerActivity.startService(objIntent);
        }


    public void initBroadcastReceiver() {
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                try {
                    String str = intent.getStringExtra(PlayAudioService.NISHANT_SEEKBAR_PROGRESS);
                    String str1 = intent.getStringExtra(PlayAudioService.NISHANT_BUFFER_PROGRESS);
                    String str2 = intent.getStringExtra(PlayAudioService.NISHANT_DURATION);
                    String isAudioPlayCompleted = intent.getStringExtra(PlayAudioService.NISHANT_COMPLETION);
//                    Log.d(HomeActivity.LogTag, str + str1 + str2);
                    progress = str;
                    myBroadcastAction(str, str2);
                    myBroadcastAction1(str1);
                    myBroadcastAction2(str, str2);
                    myBroadcastAction3(isAudioPlayCompleted);
                } catch (Exception e) {
                    e.printStackTrace();
                    String errStr = "GS Error-1141 : " + e.getMessage();
                    Log.e(HomeActivity.LogTag, errStr);
                    stopAudioPlayer();
                }
            }

        };
        mediaPlayerErrorReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                try {
                    String error = intent.getStringExtra(PlayAudioService.NISHANT_ERROR);
//                    Log.d(HomeActivity.LogTag, error + " mediaPlayerErrorReceiver");
                    myBroadcastActionError(error);
//                    Toast.makeText(ownerActivity, "\n^^^^^^^^^ onError ^^^^^\n", Toast.LENGTH_SHORT).show();
                    PlayAudioService.mpError = "";

                } catch (Exception e) {
                    e.printStackTrace();
                    String errStr = "GS Error-1142 : " + e.getMessage();
                    Log.e(HomeActivity.LogTag, errStr);
                    stopAudioPlayer();
                    if (dialogExceptionOnce) {
                        dialogExceptionOnce = false;
                        Utility.showDialog(ownerActivity, "Audio can not be played at this time.\ne.getMessage()\nPlease try after some time.", "GS Error-1142");
                    }
                }
                //   Log.d(PlayAudioService.LOG_ID, "........BroadcastReceiver initialized ......");
            }
        };
    }
        void myBroadcastActionError(String error) {
            if (pdInAudioPlayer != null) {
                pdInAudioPlayer.dismiss();
                stopAudioPlayer();
                if (dialogOnce) {
                    dialogOnce = false;
                    Utility.showDialog(ownerActivity, "Audio can not be played at this time.\nPlease try after some time.", "GS Error-2001");
                }
            }
        }

    public void registerBroadcastReceiver(){
        LocalBroadcastManager.getInstance(apContext).registerReceiver((receiver),
                new IntentFilter(PlayAudioService.NISHANT_NOTIFICATION)
        );

        LocalBroadcastManager.getInstance(apContext).registerReceiver((mediaPlayerErrorReceiver),
                new IntentFilter(PlayAudioService.EROOR_NOTIFICATION)
        );
       // Log.d(PlayAudioService.LOG_ID, "........BroadcastReceiver registered ......");
    }

    void myBroadcastAction(String str, String str2){
        if (pdInAudioPlayer != null){
            pdInAudioPlayer.dismiss();

            makeAudioUIVisible();
            int timeDuration = Integer.parseInt(str2);
            audioSeekBar.setMax(timeDuration);
        }
     //   Log.d(PlayAudioService.LOG_ID, "........myBroadcastAction : " + str + "  ......");
        audioSeekBar.setProgress(Integer.parseInt(str));
    }

    void myBroadcastAction1(String str){

     //   Log.d(PlayAudioService.LOG_ID, "........myBroadcastAction1 : " + str + "  ......");
        bufferTextView.setText(str);
    }

    void myBroadcastAction2(String str, String str2){
        long timeDuration = Long.parseLong(str2);
        long position = Long.parseLong(str);
      //  Log.d(PlayAudioService.LOG_ID, "........myBroadcastAction2 : " + timeDuration + "  ......");
      //  Log.d(PlayAudioService.LOG_ID, "........position : " + position + "  ......");
       // totalTime.setText(getTimeString(timeDuration));
        playTextView.setText(getTimeString(position));
    }

    void myBroadcastAction3(String isAudioPlayCompleted){
       // Log.d(PlayAudioService.LOG_ID, "........completion : " + str3 + "  ......");
        if(isAudioPlayCompleted.equals("true")) {
            makeAudioUIInvisible();
        }
    }
    public void makeAudioUIInvisible() {
        audioSeekBar.setVisibility(View.INVISIBLE);
        playOrPauseBtn.setVisibility(View.INVISIBLE);
        bufferTextView.setVisibility(View.INVISIBLE);
        bufferText.setVisibility(View.INVISIBLE);
        playTextView.setVisibility(View.INVISIBLE);
        stopPlayer.setVisibility(View.INVISIBLE);
        playText.setVisibility(View.INVISIBLE);
        audioPlayerLayout.setVisibility(View.INVISIBLE);
        linearLayout.setVisibility(View.INVISIBLE);
    }

    public void makeAudioUIVisible() {
      //  Log.d(PlayAudioService.LOG_ID, "........ makeAudioUIVisible  ......");
        audioSeekBar.setVisibility(View.VISIBLE);
        playOrPauseBtn.setVisibility(View.VISIBLE);
        bufferTextView.setVisibility(View.VISIBLE);
        bufferText.setVisibility(View.VISIBLE);
        playTextView.setVisibility(View.VISIBLE);
        stopPlayer.setVisibility(View.VISIBLE);
        playText.setVisibility(View.VISIBLE);
        audioPlayerLayout.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
    }



    public void handleAudioViewAppearance(){
        // Log.d(HomeActivity.LogTag, " --- handleAudioViewAppearance -- ");
        if (PlayAudioService.mp == null){
            playOrPauseBtn.setBackgroundResource(R.drawable.play);
            playOrPauseBtn.setText(playBtnTitle);
            audioSeekBar.setProgress(0);
        }else {
            if (PlayAudioService.mp.isPlaying()){
                playOrPauseBtn.setBackgroundResource(R.drawable.pause);
                playOrPauseBtn.setText(pauseBtnTitle);
            }else {
                playOrPauseBtn.setBackgroundResource(R.drawable.play);
                playOrPauseBtn.setText(playBtnTitle);
            }
        }
    }

    private String getTimeString(long millis) {
        StringBuffer buf = new StringBuffer();

        int hours = (int) (millis / (1000 * 60 * 60));
        int minutes = (int) ((millis % (1000 * 60 * 60)) / (1000 * 60));
        int seconds = (int) (((millis % (1000 * 60 * 60)) % (1000 * 60)) / 1000);

        buf.append(String.format("%02d", hours))
                .append(":")
                .append(String.format("%02d", minutes))
                .append(":")
                .append(String.format("%02d", seconds));

        return buf.toString();
    }
    void showMyPD(){
        pdInAudioPlayer = new ProgressDialog(ownerActivity);
        pdInAudioPlayer.setMessage("Streaming..");
        pdInAudioPlayer.setCancelable(false);
        pdInAudioPlayer.show();
    }

    public void playOrPauseAudioPlayer(){

        ConnectivityManager connMgr = (ConnectivityManager)
                apContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

            if (playOrPauseBtn.getText().toString().equalsIgnoreCase(playBtnTitle)) {
                if (PlayAudioService.mp == null) {
                 //   Log.d(PlayAudioService.LOG_ID, "........mp = null......");
                    playAudioFile();
//                    bufferTextView.setVisibility(View.VISIBLE);
//                    bufferText.setVisibility(View.VISIBLE);
//                    playTextView.setVisibility(View.VISIBLE);
//                    playText.setVisibility(View.VISIBLE);
                    //                   PlayAudioService.durationHandler.postDelayed(PlayAudioService.updateSeekBarTime, 200);
                    playOrPauseBtn.setBackgroundResource(R.drawable.pause);
                    playOrPauseBtn.setText(pauseBtnTitle);
                } else {

                //    Log.d(PlayAudioService.LOG_ID, "........mp != null......");
                    PlayAudioService.mp.getCurrentPosition();
                    PlayAudioService.mp.start();
                    PlayAudioService.durationHandler.postDelayed(PlayAudioService.updateSeekBarTime, 200);
                    playOrPauseBtn.setBackgroundResource(R.drawable.pause);
                    playOrPauseBtn.setText(pauseBtnTitle);
                }


            } else if (playOrPauseBtn.getText().toString().equalsIgnoreCase(pauseBtnTitle)) {
                if (PlayAudioService.mp != null && PlayAudioService.mp.isPlaying()) {
             //       Log.d(PlayAudioService.LOG_ID, "........mp != null pause......");
                    //audioSeekBar.setProgress(PlayAudioService.mp.getCurrentPosition());

                    PlayAudioService.mp.pause();
                    //                   PlayAudioService.durationHandler.postDelayed(PlayAudioService.updateSeekBarTime, 200);
            //        Log.d("progress" + progress, "........is......");
                    audioSeekBar.setProgress(Integer.parseInt(progress));
                    //                   PlayAudioService.mp.seekTo(Integer.parseInt(progress));

                }
                playOrPauseBtn.setText(playBtnTitle);
                playOrPauseBtn.setBackgroundResource(R.drawable.play);

            } else {
                if (PlayAudioService.mp != null) {
            //        Log.d(PlayAudioService.LOG_ID, "........mp != null reset......");
                    PlayAudioService.mp.reset();
                }
                playOrPauseBtn.setText(playBtnTitle);
                playOrPauseBtn.setBackgroundResource(R.drawable.play);

            }
            }
        else {

            String errMsg = "";
            if (networkInfo != null){
                errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
            }else{
                errMsg = "\n\n No connection.. \n\n";

//                Toast.makeText(apContext, "Please check internet connection", Toast.LENGTH_LONG).show();
            }
        }
        }

    public void stopAudioPlayer(){
        // Toast.makeText(context, "\n^^^^^^^^^ stopAudioPlayer ^^^^^\n", Toast.LENGTH_SHORT).show();
        Intent objIntent = new Intent(ownerActivity, PlayAudioService.class);
        ownerActivity.stopService(objIntent);
        playTextView.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed1), TimeUnit.MILLISECONDS.toSeconds((long) timeElapsed1) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed1))));
//        bufferTextView.setVisibility(View.INVISIBLE);
//        bufferText.setVisibility(View.INVISIBLE);
//        playTextView.setVisibility(View.INVISIBLE);
//        playText.setVisibility(View.INVISIBLE);
        playOrPauseBtn.setText(playBtnTitle);
        playOrPauseBtn.setBackgroundResource(R.drawable.play);
        PlayAudioService.bufferPercent = 0;
        bufferTextView.setText("" + PlayAudioService.bufferPercent + "%");
        audioSeekBar.setProgress(0);
        makeAudioUIInvisible();
     //   Log.d(PlayAudioService.LOG_ID, "........STOP clicked......");


        //audioSeekBar.setSecondaryProgress( PlayAudioService.bufferPercent);

//        playOrPauseBtn.setText(pauseBtnTitle);
//        playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_pause);
//        Log.d("completion ........in......","makeAudioUIInvisible");
//        if (PlayAudioService.mp != null){
//
//            PlayAudioService.durationHandler.removeCallbacks(PlayAudioService.updateSeekBarTime);
//            PlayAudioService.mp.stop();
//            PlayAudioService.mp.release();
//            PlayAudioService.mp = null;
//            Log.d("completion ........in......","mp is null");
        //       }

    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (PlayAudioService.mp == null){
            seekBar.setProgress(0);
        }else {
            if (fromUser){
                if (PlayAudioService.mp.isPlaying()){
                    PlayAudioService.mp.seekTo(progress);
                } else {
                    seekBar.setProgress(PlayAudioService.mp.getCurrentPosition());
                }
            }else {
                // nothing to do
            }
        }
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}