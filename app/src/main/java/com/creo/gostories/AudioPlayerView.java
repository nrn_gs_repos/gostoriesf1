package com.creo.gostories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnBufferingUpdateListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

/**
 * Created by Nishant Thite on 01/07/15.
 */
public class AudioPlayerView extends View implements
        MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener,
        SeekBar.OnSeekBarChangeListener, OnBufferingUpdateListener {

    int bufferPercent = CommonVariable.bufferPercent;
    int playedPercent = CommonVariable.playedPercent;
    MediaPlayer mp;
    SeekBar audioSeekBar;
    String SongUrl = null;
    final  String playBtnTitle = CommonVariable.playBtnTitle;
    final  String pauseBtnTitle = CommonVariable.pauseBtnTitle;
    boolean streamMediaPlayerFromZero = true;
    Context context;
    Button playOrPauseBtn;
    boolean bufferingInProgress = true;
    TextView bufferTextView;
    TextView bufferText;
    TextView playTextView;
    TextView playText;
    ProgressDialog pd = null;

    private android.os.Handler durationHandler = new android.os.Handler();
    private double timeElapsed = CommonVariable.timeElapsed, finalTime = CommonVariable.finalTime, timeElapsed1 = CommonVariable.timeElapsed;

    public AudioPlayerView(Context context) {
        super(context);
        this.context = context;

    }
    public void streamAudioPlayer() {
        try
        {
            bufferTextView.setVisibility(View.VISIBLE);
            bufferText.setVisibility(View.VISIBLE);
            playTextView.setVisibility(View.VISIBLE);
            playText.setVisibility(View.VISIBLE);
            bufferingInProgress = true;
            playedPercent = 0;
            bufferPercent = 0;
            bufferTextView.setText(""+bufferPercent+"%");
            streamMediaPlayerFromZero = false;
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                if (pd != null){
                    pd = null;
                }
                pd = new ProgressDialog(context);
                pd.setMessage("Buffering...");
                pd.setCancelable(false);
                pd.show();

                audioSeekBar.setProgress(0);
                audioSeekBar.setSecondaryProgress(0);

                try
                {
                    if (mp == null)
                        mp = new MediaPlayer();
                    else
                        mp.reset();

                    mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mp.setOnPreparedListener(this);
                    mp.setOnErrorListener(this);
                    mp.setDataSource(SongUrl);
                    mp.setOnBufferingUpdateListener(this);
                    mp.setOnCompletionListener(this);

                    mp.prepareAsync();
                }

                catch(Exception error110) {
                    String errStr = "error110 : " + error110.getMessage();
                    //this.errorTextView.setText(errStr);
                    Log.e(HomeActivity.LogTag, errStr);
                    Toast.makeText(context, errStr, Toast.LENGTH_LONG).show();
                }

            } else {
                // display error
                String errMsg = "";
                if (networkInfo != null){
                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                }else{
                    errMsg = "\n\n No connection.. \n\n";
                }

                Log.e(HomeActivity.LogTag, errMsg);
                Toast.makeText(context, errMsg, Toast.LENGTH_LONG).show();
                // String[] errArr = {errMsg};
                // updateListViewWithDataAndBackGroundColor(errArr, Color.RED);
            }
        }
        catch (Exception e)
        {
            String errStr = "\n\n Error in : " +e.getLocalizedMessage()+"\n\n";
            // this.errorTextView.setText(errStr);
            Log.e(HomeActivity.LogTag, errStr);
        }
    }


    public void stopAudioPlayer(){
        // Toast.makeText(context, "\n^^^^^^^^^ stopAudioPlayer ^^^^^\n", Toast.LENGTH_SHORT).show();

        if (mp != null){
            mp.stop();
            mp = null;
            // mp.reset();
            streamMediaPlayerFromZero = true;
            playTextView.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed1), TimeUnit.MILLISECONDS.toSeconds((long) timeElapsed1) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed1))));
        }
        bufferTextView.setVisibility(View.INVISIBLE);
        bufferText.setVisibility(View.INVISIBLE);
        playTextView.setVisibility(View.INVISIBLE);
        playText.setVisibility(View.INVISIBLE);
        playOrPauseBtn.setText(playBtnTitle);
        playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);
        bufferPercent = 0;
        bufferTextView.setText(""+bufferPercent+"%");
        audioSeekBar.setProgress(0);
        audioSeekBar.setSecondaryProgress( bufferPercent);

    }

    public void playOrPauseAudioPlayer(){
        if (playOrPauseBtn != null){
            if (playOrPauseBtn.getText().toString().equalsIgnoreCase(playBtnTitle)){
                if (streamMediaPlayerFromZero == true) {
                    streamMediaPlayerFromZero = false;
                    streamAudioPlayer();
                    durationHandler.postDelayed(updateSeekBarTime, 100);

                    playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_pause);
                }else {
                    try{
                        mp.start();
                        durationHandler.postDelayed(updateSeekBarTime, 100);
                        playOrPauseBtn.setText(pauseBtnTitle);
                        playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_pause);

                    }catch (Exception error111){
                        String errStr = "Error in loadJsonString: " + error111;
                        Log.e(HomeActivity.LogTag, errStr);
                        streamAudioPlayer();
                    }
                }
            }else if (playOrPauseBtn.getText().toString().equalsIgnoreCase(pauseBtnTitle)){
                if (mp != null){
                    mp.pause();
                }
                playOrPauseBtn.setText(playBtnTitle);
                playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);

            }else {
                if (mp != null){
                    mp.reset();
                }
                playOrPauseBtn.setText(playBtnTitle);
                playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);

            }
        }
    }

    class TeleListener extends PhoneStateListener {
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    // CALL_STATE_IDLE;
//                    Toast.makeText(getApplicationContext(), "CALL_STATE_IDLE",
//                            Toast.LENGTH_LONG).show();
                    if (mp == null) {

                    }
                    else{
                        mp.start();
                        playOrPauseBtn.setText(pauseBtnTitle);
                        playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_pause);
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    // CALL_STATE_OFFHOOK;
//                    Toast.makeText(getApplicationContext(), "CALL_STATE_OFFHOOK",
//                            Toast.LENGTH_LONG).show();
                    if (mp != null) {
                        mp.pause();
                        playOrPauseBtn.setText(playBtnTitle);
                        playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);

                    }
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    // CALL_STATE_RINGING
//                    Toast.makeText(getApplicationContext(), incomingNumber,
//                            Toast.LENGTH_LONG).show();
//                    Toast.makeText(getApplicationContext(), "CALL_STATE_RINGING",
//                            Toast.LENGTH_LONG).show();
                    if (mp != null) {
                        mp.pause();
                        playOrPauseBtn.setText(playBtnTitle);
                        playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);

                    }
                    break;
                default:
                    break;
            }
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        audioSeekBar.setOnSeekBarChangeListener(this);
        audioSeekBar.setProgress(0);
        audioSeekBar.setSecondaryProgress(bufferPercent);
        audioSeekBar.setMax(mp.getDuration());
        audioSeekBar.postDelayed(onEverySecond, 1000);
        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        TelephonyMgr.listen(new TeleListener(),
                PhoneStateListener.LISTEN_CALL_STATE);
        //Toast.makeText(context, "Prepare finished", Toast.LENGTH_LONG).show();
        Log.i(HomeActivity.LogTag, "Prepare finished");
        pd.setMessage("Playing.....");
        pd.dismiss();
        mp.start();
        playOrPauseBtn.setText(pauseBtnTitle);
        playTextView.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed), TimeUnit.MILLISECONDS.toSeconds((long) timeElapsed) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed))));
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {

        String errorStr = "MediaPlayer mpError : "+ what+" and "  + extra;
        Log.e(HomeActivity.LogTag, errorStr);
        //mainActivityObj.errorTextView.setText(errorStr);
        Toast.makeText(context, errorStr, Toast.LENGTH_LONG).show();
        if (pd != null)
            pd.dismiss();

        audioSeekBar.setProgress(0);
        bufferPercent = 0 ;
        audioSeekBar.setSecondaryProgress(bufferPercent);

        mp.reset();
        // streamAudioPlayer();

        return false;
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (pd != null)
            pd.dismiss();

        playOrPauseBtn.setText(playBtnTitle);
        playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);

        stopAudioPlayer();

        // mp.reset();
        //Toast.makeText(getApplicationContext(), "Completed", Toast.LENGTH_LONG).show();
    }

    private Runnable onEverySecond=new Runnable() {


        @Override
        public void run() {

            if(audioSeekBar != null && mp != null && mp.isPlaying()) {
                audioSeekBar.setProgress(mp.getCurrentPosition());
                audioSeekBar.postDelayed(onEverySecond, 1000);
                String updateMsg = (mp.getCurrentPosition()*100)/mp.getDuration() +"";
                //  playTextView.setText(""+updateMsg+"% updateMsg");
             //   Log.d(HomeActivity.LogTag, updateMsg);


            }
        }
    };


    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {
            if (mp != null) {
                //get current position
                timeElapsed = mp.getCurrentPosition();
                //set seekbar progress
                audioSeekBar.setProgress((int) timeElapsed);
                //set time remaing
                double timeRemaining = finalTime - timeElapsed;
                playTextView.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed), TimeUnit.MILLISECONDS.toSeconds((long) timeElapsed) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed))));

                //repeat yourself that again in 100 miliseconds
                durationHandler.postDelayed(this, 100);
            } else{
                audioSeekBar.setProgress(0);
            }
        }
    };

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser) {
            // this is when actually seekbar has been seeked to a new position
            if (mp != null) {
//               if(mp.isPlaying() == false){
//                    mp.start();
//                }
                mp.seekTo(progress);
            }else{
                seekBar.setProgress(0);
            }
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        bufferPercent = percent;
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (bufferingInProgress == true) {
                    // audioSeekBar.setIndeterminate(false);
                    audioSeekBar.setSecondaryProgress(bufferPercent);
                    String updateMsg = "\n ************ buffer Percent: " + bufferPercent + " ************\n\n";
                    bufferTextView.setText("" + bufferPercent + "%");
                    // mainActivityObj.errorTextView.setText(updateMsg);
                //    Log.d(HomeActivity.LogTag, updateMsg);
                    if (bufferPercent == 100) {
                        bufferingInProgress = false;
                    }

                }
            }
        });
    }
}
