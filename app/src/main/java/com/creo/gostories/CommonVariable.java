package com.creo.gostories;

/**
 * Created by Admin_2 on 25-09-2015.
 */
public class CommonVariable {

     static String playBtnTitle = "Play";
     static String pauseBtnTitle = "Pause";
     static double timeElapsed = 0;
     static int bufferPercent = 0;
     static int playedPercent = 0;
     static double finalTime = 0;
     static String APP_ID = "appa611adf5992346e7b7";
     static String ZONE_ID = "vz23f08aec55fe40c9ac";
     static String errorMessage = "ErrorMessage";
     static String PromoCodeAlreadyApplied = "PromoCodeAlreadyApplied";
     static String errorCode = "ErrorCode";
     static String congratulations = "AWESOME";
     static int PrevBalance = 0;
     static int NewBalance = 0;
     static String videoAdPoint = "100";
     static int DEFAULT_VIDEO_AD_REWARD = 100;
     static int inAppPurchasePoint = 0;
     static String details = "MY INFO";
     static String targetPromoURL = "http://gostories.co.in/Sandeep/GoStoriesHost/Nikhil/gostories/applyForPromoCode.php";
     static String targetUpdateBalanceURL = "http://gostories.co.in/Sandeep/GoStoriesHost/Nikhil/gostories/balanceUpdation.php";
     static String APPLYED_PROMOCODE = "APPLYED_PROMOCODE";
     static String applyedPromoCode = "applyedPromoCode";
     static String LogTag = "GoStoriesLogTag";
     static String  playStoreLink = "https://play.google.com/store/apps/details?id=com.sandeep.gostories3&hl=en";
     static int SWIPE_MIN_DISTANCE = 120;
     static int SWIPE_THRESHOLD_VELOCITY = 200;
     static String url = "http://gostories.co.in/Sandeep/prastawana.mp3";
     static String contactUsContent = "Unable to reach server";
     static String latestPlayStoreVersion = "27";
     static String currentPlayStoreVersion = "27";
     static String finalMsg = "Updated version of GoStories is available on play store ";
     static int currentPlayStoreVersionValue = 0;
     static int latestPlayStoreVersionValue = 0;
     static int RC_REQUEST = 10001;
     static String developerPayload = "NishantDeveloperPayload";
     static String TAG = "InAppBilling_LOG_TAG";
     static int purchasedPointsBasket = 0;
     static int InAppBillingAPIServicesVersion = 3;
     static int newPointsToBeAdded = 0;
     static String author_info = "author_info";
     static String about_artist = "about_artist";
     static int timeElapsedInt = 0;
     static String EROOR_NOTIFICATION = "com.javapapers.android.playaudio.EROOR_NOTIFICATION";
     static String NISHANT_NOTIFICATION = "com.javapapers.android.playaudio.NISHANT_NOTIFICATION";
     static String NISHANT_SEEKBAR_PROGRESS = "SEEKBAR_PROGRESS";
     static String NISHANT_BUFFER_PROGRESS = "BUFFER_PROGRESS";
     static String NISHANT_DURATION = "DURATION";
     static String NISHANT_COMPLETION = "COMPLETION";
     static String NISHANT_ERROR = "ERROR";
     static String preview_story = "preview_story";
     static String story_url = "story_url";
     static int WalletBalance = 0;
     static String goStoriesPoints = " GoStories Points" ;
     static final int DEFAULT_STORY_COST = 1000;
     static int ONE_STORY_COST = 100;
     static String purchasedStoryListName = "purchasedStoryListAsString";
     static String No = "No";
     static int GET = 1;
     static int POST = 2;
     static int SPLASH_TIME_OUT = 1000;
     static int newBackGrounfColorForStoryList = 0;
     static String story_name = "story_name";
     static String story_Id = "_id";
     static String book_name = "book_name";
     static String director = "director";
     static String author_point = "author_point";
     static String author_name = "author_name";
     static String author_id = "_id";
     static String artist_id = "_id";
     static String artistCompare = "Artist";
     static String autherCompare = "Author";
     static String directorCompare = "Director";
     static String story_duration = "story_duration";
     static String voice_artist = "voice_artist";
     static String artist_name = "artist_name";
     static String pointerStroryListApiBaseUrlStr = "http://gostories.co.in/Sandeep/Data/PointerStoryList.json";
     static String SELF_PROMO_CODE = "SELF_PROMO_CODE";
     static String gs_error_contact_msg = "If this error persists, please contact GoStories support team with this error code.";
     static String gss_error301 = "GSS Error-1301 : Contact GoStories support team with this error code.friendz@gostories.co.in";
     static String promo_code = "promo_code";
     static String total_point = "total_point";
     static String WALLET_BALANCE = "WALLET_BALANCE";
     static String userRegistrationUrl = "http://gostories.co.in/Sandeep/GoStoriesHost/Nikhil/gostories/userCreation.php";
     static String not_connected = "You are not connected";
     static String connected = "You are connected";
     static String success = "Success";
     static String error = "Error";
     static String MY_PREFS_NAME = "MY_PREFS_NAME";
     static String email = "email";
     static String terms = "Terms and Conditions";
     static String ALGO = "AES";
     static final String prevBalance = "PrevBalance";
     static final String newBalance = "NewBalance";
     static final String TAG_EMAIL_ID = "RegisteredEmailId";
     static final String  EMAIL_ID = "E";
     static final String TAG_PROMO_CODE = "FRIENDS_REFERRAL_CODE";
     static final String RegisteredUDId = "RegisteredUDId";
     static final String RegisteredPhoneNumber = "RegisteredPhoneNumber";
     static final String StoryID = "StoryID";
     static final String isPurchased = "IsPurchased";
     static String isPurchasedStoryURL = "http://gostories.co.in/Sandeep/GoStoriesHost/Nikhil/gostories/isPurchasedStory.php";
     static String purchaseStoryURL = "http://gostories.co.in/Sandeep/GoStoriesHost/Nikhil/gostories/purchaseStory.php";
     static byte[] keyValue =
             new byte[] { 'N', 'I', 'S', 'H', 'A', 'N', 'T',
                     'R', 'A', 'M', 'E','S', 'H', 'N', 'I', 'R' };
     static String success_msg = "Congratulations you have successfully registered. your Wallet Balance is : ";
     static String msg = " It is important that you read all the terms and conditions carefully.\n" +
             "    The following specifies the terms and conditions under which you may use the “Gostories” App, service, or product. By using, visiting or browsing the \"Gostories\" App, services or product, you accept and agree to be bound by the Terms of Use. If you don’t agree to the Terms of Use, you should not use the website or \"Gostories\" Application or service or product. We may revise the terms at any time by updating the Terms of Use on our site, \"Gostories\" Application, service or product. Continued use of the site, \"Gostories\" App, services or product after such modifications constitutes your acceptance of the revised terms updated by you. You should visit this page periodically to review the Terms.\n" +
             "    \n" +
             "    Electronic Communication\n" +
             "    \n" +
             "    When you visit \"Gostories\" App, service or product or send emails to us, you are communicating with us electronically and you likewise consent to receive communications from us. We will communicate with you by email or by posting notices on this site, \"Gostories\" App, service or product. You agree and admit that all agreements, notices, disclosures and other communications sent by you to you electronically satisfy the legal requirements that such communications are in writing. Your consent to electronic agreements is necessary for use of the \"Gostories\" App, service or product.\n" +
             "        Copyright\n" +
             "        \n" +
             "        Content from \"Gostories\" App, service or product may be used and played only for your personal use and you undertake not to use the same for any commercial purpose or any purpose whatsoever. You agree not to modify, reproduce, retransmit, transfer, distribute, sell, broadcast or otherwise exploit the content of “Gostories” app service or product for any commercial purpose without the express written consent of Vision Future Combine. All content included on the \"Gostories\" App, service or product such as text, graphics, logos, audio clips, video clips, digital downloads, data compilations and software is the sole property of Vision Future Combine and any infringement by any person will expose themselves action under Copyright Law.\n" +
             "            Usage Requirements\n" +
             "            \n" +
             "            You must be at least 18 years old to access the sections other than kids collection. You must have internet access, a valid email address, and a valid credit/debit card to use website, \"Gostories\" App, service or product. If you use a password to access this site, \"Gostories\" Application, service or product or any portion of it, then you are responsible for maintaining the confidentiality of the password and for restricting access to your computer, mobile or device and you agree to accept responsibility for all activities that occur on your account or with your password. If you are under 18, you may use \"Gostories\" App, service or product only with involvement of a parent or guardian. You agree to ensure that all personal details provided by you will be true, current, complete and accurate and you agree to maintain and update the information on your “Edit Profile” page as required. You agree that we may store and use information you provide (including credit / debit card information) for use in maintaining your account(s) and billing \"Gostories\" App, services, product fees to your credit / debit card.\n" +
             "                \n" +
             "                Reviews and comments\n" +
             "                \n" +
             "                We may permit visitors to post reviews and/or comments on the \"Gostories\" App, service or product. You agree that any content you contribute will not be illegal, obscene, threatening, defamatory, invasive of privacy, infringing of intellectual property rights, or otherwise objectionable and does not consist of or certain software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any form of “spam”. We reserve the right to remove or edit such content.\n" +
             "                Cancellation Policy\n" +
             "                \n" +
             "                You may cancel your membership at any time. Your credit / debit card will not be charged during the next billing cycle. To cancel your Membership Plan, you may do so from “Edit Profile” page within the \"Gostories\" App, service or product after you are signed in.\n" +
             "                    Refund Policy\n" +
             "                    \n" +
             "                    \n" +
             "                        Termination\n" +
             "                        \n" +
             "                        We reserve the right to terminate your use of \"Gostories\" App, service or product, without notice, at any time if we determine, in our sole discretion, that you have violated these Terms of Use or any other policy posted on this \"Gostories\" App, service or product.\n" +
             "                            Limitation of Liability\n" +
             "                            \n" +
             "                            The \"Gostories\" App, service, product and content are provided on an “As Is” and “As Available” basis. We make no representation or warranties on any kind, express or implied, as to the operation of the \"Gostories\" App service product and content. Neither we nor any of our supplier will be liable to you under any circumstances for any damages arising in connection with \"Gostories\" App service product and/or content.\n" +
             "                                Privacy Policy\n" +
             "                                \n" +
             "                                We do not / will not sell, rent, share or disclose personal information to third parties without prior consent, except in the limited circumstances where we may contract with others to perform tasks or services on our behalf. For example, we might retain a company to process credit / debit card payments.\n" +
             "                                    Security\n" +
             "                                    \n" +
             "                                    This “Gostories” app, service or product and its content are well protected under Copyright Law. You agree that you are not authorized to log into server or account, access data or materials not intended for you, test the vulnerability of our network and security systems or otherwise breach or circumvent our security in any way.";


    public static String dataString = "D";
   // public static String exceptionURL = "http://gostories.co.in/Sandeep/GoStoriesHost/DataCollection/testUpload.php";
    public static String exceptionURL = "http://gostories.co.in/Sandeep/GoStoriesHost/ExceptionDataCollection/uploadData.php";
    public static String userActivityURL = "http://gostories.co.in/Sandeep/GoStoriesHost/UserActivityDataCollection/uploadData.php";
    public static String userTechnicalDetailyURL = "http://gostories.co.in/Sandeep/GoStoriesHost/UserTechnicalDetails/uploadData.php";
    public static String negativeBalanceURL = "http://gostories.co.in/Sandeep/GoStoriesHost/NegativeBalanceDataCollection/uploadData.php";
    public static String userRegisterationURL = "http://gostories.co.in/Sandeep/GoStoriesHost/UserRegisterationDataCollection/uploadData.php";
    public static String registrationDetailsURL = "http://27.251.53.163:81/GoStories/RegistrationDetails/uploadData.php";
}

