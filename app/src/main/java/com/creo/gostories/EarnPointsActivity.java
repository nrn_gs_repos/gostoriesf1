package com.creo.gostories;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyAd;
import com.jirbo.adcolony.AdColonyAdAvailabilityListener;
import com.jirbo.adcolony.AdColonyAdListener;
import com.jirbo.adcolony.AdColonyVideoAd;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by Admin_2 on 20-08-2015.
 */
public class EarnPointsActivity extends Activity implements AdColonyAdListener, AdColonyAdAvailabilityListener {
//    final static String APP_ID = "app185a7e71e1714831a49ec7";
//    final static String ZONE_ID = "vz06e8c32a037749699e7050";
    final static String APP_ID = CommonVariable.APP_ID;
    final static String ZONE_ID = CommonVariable.ZONE_ID;
    String localStorageStr = "";
    boolean loadingOneVideoRewardAdPoint = false;
    boolean loadingSkuItemsArrFromServer = false;
    Boolean success_response = false;
    Boolean error_response = false;
    Boolean parsing_error = false;
    Boolean in_Promocode = false;
    Boolean in_BalenceError = false;
    Boolean in_BalanceUpdate = false;
    Boolean in_info= false;
    Boolean applyePromoCode= false;
    static SharedPreferences pref,prefPromo;
    Handler button_text_handler;
    Runnable button_text_runnable;
    Button video_button, share_button,applyPromoCode;
    static TextView myMsg;
    static String finalMsg = "";
    static String walletBalance = "",email_ID = "",promo_code = "",total_point = "",success_msg = "",errorValue = "",errorMessageValue = "",prevBalanceStr = "",NewBalanceStr = "";
    static  String errorMessage = CommonVariable.errorMessage;
    static  String PromoCodeAlreadyApplied = CommonVariable.PromoCodeAlreadyApplied;
    static  String errorCode = CommonVariable.errorCode;
    static  String congratulations = CommonVariable.congratulations;
    static String resultJson = "";
    static String msg = "" ;
    static int PrevBalance = CommonVariable.PrevBalance;
    static int NewBalance = CommonVariable.NewBalance;
    static String videoAdPoint = CommonVariable.videoAdPoint;
    static final int DEFAULT_VIDEO_AD_REWARD = CommonVariable.DEFAULT_VIDEO_AD_REWARD;
    static  int inAppPurchasePoint = CommonVariable.inAppPurchasePoint;
    Context context = this;
    String details = CommonVariable.details;
    static String targetPromoURL = CommonVariable.targetPromoURL;
    static String targetUpdateBalanceURL = CommonVariable.targetUpdateBalanceURL;
    Button inAppPurchaseBtn;
    static String promoCode = "";
    ProgressDialog pdInPromoList;
    ImageView imageView;
    static String APPLYED_PROMOCODE = CommonVariable.APPLYED_PROMOCODE;
    static String applyedPromoCode = CommonVariable.applyedPromoCode;
    static String applyedPromoCodeValue = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promo);
        UserRegisterationActivity.inUserRegistration = false;
        prefPromo=getApplication().getSharedPreferences(APPLYED_PROMOCODE, MODE_PRIVATE);
        pref=getApplication().getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);
        email_ID = pref.getString(UserRegisterationActivity.email, "");
        promo_code = pref.getString(UserRegisterationActivity.promo_code, "");
        total_point = pref.getString(UserRegisterationActivity.total_point, "");

//        Log.d(HomeActivity.LogTag, "email in promo" + email_ID);
//        Log.d(HomeActivity.LogTag, "promo_code in promo" + promo_code);
//        Log.d(HomeActivity.LogTag, "toal_point in promo" + total_point);

        inAppPurchaseBtn = (Button) findViewById(R.id.inAppPurchaseBtn);
        video_button = (Button) findViewById(R.id.video_button);
        share_button = (Button) findViewById(R.id.share_button);
        imageView = (ImageView) findViewById(R.id.info);
        video_button.setVisibility(View.INVISIBLE);

        applyedPromoCodeValue = pref.getString(applyedPromoCode, "");
//        Log.d(HomeActivity.LogTag, "applyedPromoCodeValue" + applyedPromoCodeValue);

        applyPromoCode = (Button) findViewById(R.id.apply);
        if(applyedPromoCodeValue.length()>2){
            applyPromoCode.setVisibility(View.INVISIBLE);
        }

        applyPromoCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.d(HomeActivity.LogTag, "promocode outside " + promoCode);
                dailogBoxGetPromoCode();
//                HashMap<String, String> details = new HashMap<String, String>();
//                details.put(Utility.TAG_EMAIL_ID, UserRegisterationActivity.email);
//                details.put(Utility.TAG_PROMO_CODE, promoCode);
//
//                loadStoryAsAsyncTask(details);
            }
        });
//        Log.d(HomeActivity.LogTag, "email in promo with success in oncreate" + email_ID);
//        Log.d(HomeActivity.LogTag, "promo_code in promo with success in oncreate" + promo_code);
//        Log.d(HomeActivity.LogTag, "toal_point in promo with success in oncreate" + total_point);

        AdColony.configure(this, "version:1.0,store:google", APP_ID, ZONE_ID);
        // version - arbitrary application version
        // store   - google or amazon
        // Add ad availability listener
        AdColony.addAdAvailabilityListener(this);

        // Disable rotation if not on a tablet-sized device (note: not
        // necessary to use AdColony).
        if (!AdColony.isTablet()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


        // Handler and Runnable for updating button text based on ad availability listener
        button_text_handler = new Handler();
        button_text_runnable = new Runnable() {
            public void run() {
                video_button.setVisibility(View.VISIBLE);
               // video_button.setText("Show Video");
                video_button.setOnClickListener(
                        new View.OnClickListener() {
                            public void onClick(View v) {
                                ConnectivityManager connMgr = (ConnectivityManager)
                                        getSystemService(Context.CONNECTIVITY_SERVICE);
                                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                                if (networkInfo != null && networkInfo.isConnected()) {
                                    AdColonyVideoAd ad = new AdColonyVideoAd(ZONE_ID).withListener(EarnPointsActivity.this);
                                    ad.show();
                                    //calculate();
                                }
                                else {
                                    String errMsg = "";
                                    if (networkInfo != null){
                                        errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                                    }else{
                                        errMsg = "\n\n No connection.. \n\n";
                                        pdInPromoList.dismiss();
                                        Utility.showDialog(EarnPointsActivity.this);
                                      // Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
   /*                         private void calculate() {
                                totalPoint = Integer.parseInt(UserRegisterationActivity.total_point);
                                totalPoint = totalPoint+videoAdPoint;
                                SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                                editor.putString(UserRegisterationActivity.total_point,String.valueOf(totalPoint));
                                editor.commit();
                                UserRegisterationActivity.total_point = pref.getString(UserRegisterationActivity.total_point, "");
                                Log.d(HomeActivity.LogTag, "toal_point in promo" + UserRegisterationActivity.total_point);

                                Log.d(HomeActivity.LogTag, "total_point in promo after video ad" + totalPoint);
                            }*/
                        });

            }
        };

        share_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                promo_code = capitalizePromoCode(promo_code);
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");

                String shareBody = "Hi,\n" +
                        "Download GoStories:\n\n"+HomeActivity.playStoreLink +"\n\n" + "\nApply my following promo code to earn FREE points:"+"\n\n"+ promo_code ;
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                inAppPurchasePoint = 0;
                in_info= true;
                inAppPurchaseCall();
                /*myMsg = new TextView(PromoCodeActivity.this);
                myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
                myMsg.setTextSize(20);
                myMsg.setText(details);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setCustomTitle(myMsg);
                msg = " Email ID : "+email_ID + "\n"+" Promo Code : "+promo_code + "\n"+" Wallet_Balence : "+total_point ;
                // set dialog message
                alertDialogBuilder.setMessage(msg);
                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
                alertDialog.getWindow().setLayout(400,600);*/

            }
        });

        inAppPurchaseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                 inAppPurchasePoint = 100;
//                inAppPurchaseCall();
                Intent intent = new Intent(EarnPointsActivity.this, InAppBillingActivity.class);
                startActivity(intent);
            }
        });
        getSkuListFromServer();
    }

    private String capitalizePromoCode(String promo_code) {

        String value = promo_code;
        StringBuilder sb = new StringBuilder(value);
        for (int index = 0; index < sb.length(); index++) {
            char c = sb.charAt(index);
            if (Character.isLowerCase(c)) {
                sb.setCharAt(index, Character.toUpperCase(c));
            }
        }
        return sb.toString();
    }

    public void getSkuListFromServer(){
        InAppBillingActivity.skuItemsArrFromServer = null;
        loadingOneVideoRewardAdPoint = false;
        loadingSkuItemsArrFromServer = true;
    }

    private void inAppPurchaseCall() {
        total_point = pref.getString(UserRegisterationActivity.total_point, "");

        prevBalanceStr = total_point;
    //    Log.d(HomeActivity.LogTag, "total_point " + prevBalanceStr);
    //    Log.d(HomeActivity.LogTag, "prevBalanceStr " + prevBalanceStr);
        PrevBalance = Integer.parseInt(total_point);
        NewBalance = PrevBalance + inAppPurchasePoint;
        NewBalanceStr = String.valueOf(NewBalance);
    //    Log.d(HomeActivity.LogTag, "NewBalanceStr " + NewBalanceStr);

        HashMap<String, String> details = new HashMap<String, String>();
        details.put(Utility.TAG_EMAIL_ID, email_ID);
        details.put(Utility.PrevBalance, prevBalanceStr);
        details.put(Utility.NewBalance, NewBalanceStr);
        loadAsAsyncTask(details, targetUpdateBalanceURL);
    }

    private void dailogBoxGetPromoCode() {

        // TODO Auto-generated method stub

        final Dialog dialog = new Dialog(EarnPointsActivity.this);
        //Window window = dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // window.setBackgroundDrawable(new
        // ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialogpromo);

        final EditText promo_Code = (EditText) dialog
                .findViewById(R.id.editTextPromoCode);

        Button apply = (Button) dialog.findViewById(R.id.apply);

        apply.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                promoCode = promo_Code.getText().toString();
                // promoList = Utility.detailsToSendForPromoCode(UserRegisterationActivity.email,promoCode);
                HashMap<String, String> details = new HashMap<String, String>();
                details.put(Utility.TAG_EMAIL_ID, email_ID);
                details.put(Utility.TAG_PROMO_CODE, promoCode);
                dialog.dismiss();
                loadAsAsyncTask(details, targetPromoURL);
//                Log.d(HomeActivity.LogTag, "promocode " + promoCode);
            }
        });

        Button cancelbtn = (Button) dialog.findViewById(R.id.cancel);
        cancelbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void onPause() {
        super.onPause();
        AdColony.pause();
    }

    public void onResume() {
        super.onResume();
        AdColony.resume(this);
        if(InAppBillingActivity.purchasedPointsBasket > 0){
            inAppPurchasePoint = InAppBillingActivity.purchasedPointsBasket;
            inAppPurchaseCall();
            InAppBillingActivity.purchasedPointsBasket = 0;
        }
    }
    //Ad Started Callback - called only when an ad successfully starts playing
    public void onAdColonyAdStarted(AdColonyAd ad) {
     //   Log.d("AdColony", "onAdColonyAdStarted");
        loadingOneVideoRewardAdPoint = true;
        loadingSkuItemsArrFromServer = false;
        loadDataFromServer("http://gostories.co.in/Sandeep/DynamicData/OneVideoRewardInPoints.txt");
    }

    //Ad Attempt Finished Callback - called at the end of any ad attempt - successful or not.
    public void onAdColonyAdAttemptFinished(AdColonyAd ad) {
        // You can ping the AdColonyAd object here for more information:
        // ad.shown() - returns true if the ad was successfully shown.
        // ad.notShown() - returns true if the ad was not shown at all (i.e. if onAdColonyAdStarted was never triggered)
        // ad.skipped() - returns true if the ad was skipped due to an interval play setting
        // ad.canceled() - returns true if the ad was cancelled (either programmatically or by the user)
        // ad.noFill() - returns true if the ad was not shown due to no ad fill.
    //    Log.d("AdColony", "ad.shown: "+String.valueOf(ad.shown()));
    //    Log.d("AdColony", "ad.notShown: "+String.valueOf(ad.notShown()));
    //    Log.d("AdColony", "ad.skipped: "+String.valueOf(ad.skipped()));
    //    Log.d("AdColony", "ad.canceled: "+String.valueOf(ad.canceled()));
     //   Log.d("AdColony", "ad.noFill: "+String.valueOf(ad.noFill()));
        Boolean adComplete = ad.shown();
        if(adComplete){
            inAppPurchaseCall();
        }
    }

    private void loadDataFromServer(final String urlStr) {
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    localStorageStr = "";

                    URL url = new URL(urlStr);
                    //    Log.d(HomeActivity.LogTag, "\nLoding Story data from server\n");

                    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

                    String str;
                    while ((str = in.readLine()) != null) {
                        localStorageStr = str;
                    }
                    //    Log.d(HomeActivity.LogTag, "\nvideoAdPoint: " + videoAdPoint + "\n");
                    // strJson = str;
                    in.close();

                    if (loadingOneVideoRewardAdPoint) {
                        loadingOneVideoRewardAdPoint = false;
                        videoAdPoint = localStorageStr;
                        if ( videoAdPoint != null) {
                            if(videoAdPoint.length() > 0)
                                inAppPurchasePoint = Integer.parseInt(videoAdPoint);
                        } else {
                            inAppPurchasePoint = DEFAULT_VIDEO_AD_REWARD;
                        }
                    } else if (loadingSkuItemsArrFromServer){
//                        Log.d(InAppBillingActivity.TAG, " --- "+localStorageStr);
                        // parse json array here and populate InAppBillingActivity.skuItemsArrFromServer
                    }

                } catch (Exception error1150) {
                    String errStr = "Error in fetching dynamic data: " + error1150;
                    // this.errorTextView.setText(errStr);
                    Log.e(HomeActivity.LogTag, errStr);
                    Utility.appendExceptionLogsOnServer(context, error1150, Utility.exceptionURL);
                }
            }
        });
        thread.start();
    }

    //Ad Availability Change Callback - update button text
    public void onAdColonyAdAvailabilityChange(boolean available, String zone_id) {

        if (available) button_text_handler.post(button_text_runnable);
    }
    public void loadAsAsyncTask(HashMap<String, String> details, String targetURL){


        pdInPromoList = new ProgressDialog(this);
        pdInPromoList.setMessage("Loading ...");
        pdInPromoList.setCancelable(false);
        pdInPromoList.show();
        try{
            ConnectivityManager connMgr = (ConnectivityManager)
            getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new SendHttpRequestTask(details,targetURL).execute();
            }
            else {

                String errMsg = "";
                if (networkInfo != null){
                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                }else{
                    errMsg = "\n\n No connection.. \n\n";
                    pdInPromoList.dismiss();
                    Utility.showDialog(EarnPointsActivity.this);
                //    Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();

                }
            //    Log.d(HomeActivity.LogTag, errMsg);


            }
        } catch (Exception error_107) {
            String errStr = "\n\n Error in loadStoryAsAsyncTask: " + error_107+"\n\n";
         //   Log.d(HomeActivity.LogTag,"\n  error_107. : "+ error_107);

            Log.e(HomeActivity.LogTag,"\n  error_107. : "+  errStr);
        }

    }

    private class SendHttpRequestTask extends AsyncTask<String, Void, String> {
        HashMap<String, String> newDetails;
        String targetURL;

        public SendHttpRequestTask(HashMap<String, String> details, String targetURL) {
            this.newDetails = details;
            this.targetURL = targetURL;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                UserRegisterationActivity.inUserRegistration = false;
                 resultJson = Utility.sendAsHtmlMultipartForm(newDetails, targetURL);
                 success_response = false;
                 error_response = false;
                 parsing_error = false;
                 in_Promocode = false;
                 in_BalenceError = false;
                 in_BalanceUpdate = false;
                 Utility.connection_error = false;

                if(targetURL.equals(targetPromoURL)){
                    parsePromoCodeResponce();
                    in_Promocode = true;
                }

                if(targetURL.equals(targetUpdateBalanceURL)){
                    parseUpdateBalanceResponce();
                    in_BalanceUpdate = true;
                }

            } catch (Exception error_301) {
                error_301.printStackTrace();
               Log.e(HomeActivity.LogTag, "\n  error_301. : " + error_301);
                parsing_error = true;
            }
            return null;
        }

        private void parseUpdateBalanceResponce() {
            try {

                in_BalenceError = false;
                JSONObject jsonObject = new JSONObject(resultJson);

                if (jsonObject.has(UserRegisterationActivity.success)) {

                SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString(UserRegisterationActivity.total_point, NewBalanceStr);
                editor.commit();
                total_point = pref.getString(UserRegisterationActivity.total_point, "");
//                Log.e(HomeActivity.LogTag, "toal_point in In App Purchase" + total_point);

                 success_response = true;
                    in_BalenceError = false;
                }

                if (jsonObject.has(UserRegisterationActivity.error)) {

                    JSONObject errorObj = jsonObject.getJSONObject(UserRegisterationActivity.error);
                //    Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.error. : " + UserRegisterationActivity.error);

                    if(errorObj.has(UserRegisterationActivity.WALLET_BALANCE)) {
                //        Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.WALLET_BALANCE. : " + UserRegisterationActivity.WALLET_BALANCE);


                        walletBalance = errorObj.optString(UserRegisterationActivity.WALLET_BALANCE);
                        total_point = walletBalance;
                        SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString(UserRegisterationActivity.total_point, total_point);
                        editor.commit();
                //        Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.WALLET_BALANCE. : " + total_point);

                        in_BalenceError = true;
                    }

                    if (errorObj.has(errorCode)) {
                        errorValue = errorObj.optString(errorCode);
                //        Log.d(HomeActivity.LogTag, "\n  errorValue. : " + errorValue);
                        errorMessageValue = errorObj.optString(errorMessage);
                //        Log.d(HomeActivity.LogTag, "\n  errorMessageValue. : " + errorMessageValue);
                        error_response = true;
                    }

                }



            } catch (JSONException error_302) {
                error_302.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_302. : " + error_302);
                parsing_error = true;
            }
        }
        private void parsePromoCodeResponce() {
            try {
                JSONObject jsonObject = new JSONObject(resultJson);

                if (jsonObject.has(UserRegisterationActivity.success)) {
                    JSONObject successObj = jsonObject.getJSONObject(UserRegisterationActivity.success);

                    walletBalance = successObj.optString(UserRegisterationActivity.WALLET_BALANCE);

            //        Log.d(HomeActivity.LogTag, "\n  walletBalance. : " + walletBalance);
                    SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString(UserRegisterationActivity.total_point, walletBalance);
                    editor.putString(applyedPromoCode, promoCode);
                    editor.commit();

//                    SharedPreferences.Editor editor1 = getSharedPreferences(APPLYED_PROMOCODE, MODE_PRIVATE).edit();
//                    editor1.putString(applyedPromoCode, promoCode);
//                    editor1.commit();
//                    Log.d(HomeActivity.LogTag, "\n  promoCode. : " + promoCode);
                    success_response = true;
                }

                if (jsonObject.has(UserRegisterationActivity.error)) {

                    JSONObject errorObj = jsonObject.getJSONObject(UserRegisterationActivity.error);
//                    Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.error. : " + UserRegisterationActivity.error);
                    if(errorObj.has(errorCode)) {
                        errorValue = errorObj.optString(errorCode);
                //        Log.d(HomeActivity.LogTag, "\n  errorValue. : " + errorValue);
                        errorMessageValue = errorObj.optString(errorMessage);
                        Log.d(HomeActivity.LogTag, "\n  errorMessageValue. : " + errorMessageValue);
                    }
                    if(errorObj.has(PromoCodeAlreadyApplied)) {
                //        Log.d(HomeActivity.LogTag, "\n  PromoCodeAlreadyApplied. : " + PromoCodeAlreadyApplied);
                        errorValue = UserRegisterationActivity.error;
                        errorMessageValue = PromoCodeAlreadyApplied;
                        SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                        editor.putString(applyedPromoCode, promoCode);
                        editor.commit();
                        applyePromoCode = true;
                    }

                    error_response = true;
                }

            } catch (JSONException error_302) {
                error_302.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_302. : " + error_302);
                parsing_error = true;
            }
        }

        @Override
        protected void onPostExecute(String data) {

            if (pdInPromoList != null){
                pdInPromoList.dismiss();
            }
            total_point = pref.getString(UserRegisterationActivity.total_point, "");
        //    Log.d(HomeActivity.LogTag, "toal_point in promo with success" + total_point);

            myMsg = new TextView(EarnPointsActivity.this);
            myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            myMsg.setTextSize(20);

            if(in_Promocode) {

                if (success_response) {

                    applyPromoCode.setVisibility(View.INVISIBLE);
                    myMsg.setText(congratulations);

                    success_msg = "Congratulations Your Wallet Balance is " + total_point+" "+SelectedStoryActivity.goStoriesPoints+".";
                    String final_msg = success_msg;
                    finalMsg = final_msg;

                } else if (error_response) {
                    if(applyePromoCode){
                        applyPromoCode.setVisibility(View.INVISIBLE);
                    }
                    myMsg.setText(errorValue);
                    finalMsg = errorMessageValue;

                } else if (Utility.connection_error) {

                    myMsg.setText("GS Error - C2");
                    finalMsg = UserRegisterationActivity.gss_error301;
                } else if (parsing_error){
                    // set title
                    myMsg.setText("GS Error - P2");
                    finalMsg = UserRegisterationActivity.gss_error301;
                }
            }


            if(in_BalanceUpdate) {

                if (success_response) {
                    if(!in_info ) {
                        myMsg.setText(congratulations);

                        success_msg = "Congratulations you have earn "+inAppPurchasePoint+" "+SelectedStoryActivity.goStoriesPoints+"."+ "\n"+ "Your Wallet Balance is " +total_point+" "+SelectedStoryActivity.goStoriesPoints+".";
                        String final_msg = success_msg;
                        finalMsg = final_msg;
                    }
                    if(in_info ) {

                        myMsg.setText(details);
                        msg = "Email ID : "+email_ID+ "."+"\n" + "\n"+"Promo Code : "+promo_code+ "\n" + "\n"+"Wallet Balance : "+total_point+" "+SelectedStoryActivity.goStoriesPoints+"." ;
                        finalMsg = msg;
                    }
                    in_info= false;

                } else if (error_response) {

                    myMsg.setText(errorValue);
                    finalMsg = errorMessageValue;

                } else if (Utility.connection_error) {

                    myMsg.setText("GS Error - C2");
                    finalMsg = UserRegisterationActivity.gss_error301;
                } else if (parsing_error){
                    // set title
                    myMsg.setText("GS Error - P2");
                    finalMsg = UserRegisterationActivity.gss_error301;
                }
            }

                if(in_BalenceError){
                    inAppPurchaseCall();
                }
            else {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    alertDialogBuilder.setCustomTitle(myMsg);
                    // set dialog message
                    alertDialogBuilder.setMessage(finalMsg);
                    alertDialogBuilder
                            .setCancelable(false)
                            .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
               }

        }
    }

}
