package com.creo.gostories;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class HomeActivity extends Activity implements OnClickListener{

    //  private static final java.lang.String STATE_SCORE = "Home";
    private Button play,pause;
   // static  AudioPlayerView audioPlayerViewObj;
    static AudioPlayerTest audioPlayerTest;
    static final String LogTag = CommonVariable.LogTag;
   // static Boolean showVersionUpdate = false;
   // Button mainButton ;
    Button stopAudioBtn;
    static  String  playStoreLink = CommonVariable.playStoreLink;
    static Button playPauseAudioBtn;
    private static final int SWIPE_MIN_DISTANCE = CommonVariable.SWIPE_MIN_DISTANCE;
    private static final int SWIPE_THRESHOLD_VELOCITY = CommonVariable.SWIPE_THRESHOLD_VELOCITY;
    private ViewFlipper mViewFlipper;
    private AnimationListener mAnimationListener;
    private Context mContext;
    InterstitialAd mInterstitialAd;
    Button mNewGameButton, Prastawana;
    Context context = this;
    String url = CommonVariable.url;
    LinearLayout bottomll;
    static  String contactUsContent = CommonVariable.contactUsContent;
    static  String latestPlayStoreVersion = CommonVariable.latestPlayStoreVersion;
    static  String currentPlayStoreVersion = CommonVariable.currentPlayStoreVersion;
    static  String deviceTechnicalDetailsString = "";
    static  String finalMsg = CommonVariable.finalMsg;
    static int currentPlayStoreVersionValue = CommonVariable.currentPlayStoreVersionValue;
    static int latestPlayStoreVersionValue = CommonVariable.latestPlayStoreVersionValue;
    static SharedPreferences pref;
    static  boolean technicalDetail = false;
    //    private MediaPlayer mediaPlayer;
//    public TextView songName, duration;
//    private double timeElapsed = 0, finalTime = 0;
//    private int forwardTime = 2000, backwardTime = 2000;
//    private android.os.Handler durationHandler = new android.os.Handler();
      static SeekBar audioSeekBar;
//int mCurrentScore;
    private final GestureDetector detector = new GestureDetector(new SwipeGestureDetector());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
//        AdView mAdView = (AdView) findViewById(R.id.adView);//
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
        UserRegisterationActivity.inUserRegistration = false;
        pref = getApplication().getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);
        Utility.emailID = pref.getString(UserRegisterationActivity.email, "");
        Utility.deviceID = Utility.getDeviceID(context);
        stopAudioBtn = (Button) findViewById(R.id.stopAudioBtnId);
        play = (Button) findViewById(R.id.play);
        pause =	(Button) findViewById(R.id.stop);
        Prastawana = (Button) findViewById(R.id.Prastawana);
        basicSetupForAudioView();
        bottomll = (LinearLayout) findViewById(R.id.bottomll);
        currentPlayStoreVersion = String.valueOf(BuildConfig.VERSION_CODE);
        technicalDetail =  pref.getBoolean("technicalDetail", false);
        Log.d(HomeActivity.LogTag, "technicalDetail in home activity" + technicalDetail);
        if (!technicalDetail) {
//maybe you want to check it by getting the sharedpreferences. Use this instead if (locked)
// if (prefs.getBoolean("locked", locked) {
            String activeNetwork = "AN  " + Utility.getActiveNetworkType(context);
            String deviceName ="DMN  " + android.os.Build.MODEL;
            String deviceMan = "DM  " + android.os.Build.MANUFACTURER;
            deviceTechnicalDetailsString = String.valueOf(BuildConfig.VERSION_NAME);
            deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + "\n" +Utility.getOSDetails() +"\n" + "\n" + deviceMan +"\n" + "\n" + deviceName+"\n" + "\n" + activeNetwork;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MODEL;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MANUFACTURER;
            String msg = "CAV  " + deviceTechnicalDetailsString;
            Utility.appendLogsOnServerForTechDetail(context, msg, Utility.userTechnicalDetailURL);
        }

        if(PlayAudioService.mp == null) {
            audioPlayerTest.stopAudioPlayer();;
        }
        if(PlayAudioService.mp != null) {
            if (!PlayAudioService.mp.isPlaying()) {
            //    Log.d(PlayAudioService.LOG_ID,"........check mp......"+PlayAudioService.mp);
                audioPlayerTest.stopAudioPlayer();
            //    Log.d(PlayAudioService.LOG_ID, "........check mp2......"+PlayAudioService.mp);
            }
        }
        Thread thread1 = new Thread(new Runnable(){
            @Override
            public void run() {
                loadContactUsFromServer();
            }
        });

        thread1.start();

        Thread thread2 = new Thread(new Runnable(){
            @Override
            public void run() {
                loadAndroidLatestPlayStoreVersionFromServer();
            }
        });

        thread2.start();

        SharedPreferences.Editor editor = getSharedPreferences(EarnPointsActivity.APPLYED_PROMOCODE, MODE_PRIVATE).edit();
        editor.putString(EarnPointsActivity.applyedPromoCode, EarnPointsActivity.applyedPromoCodeValue);
        editor.commit();

        String str=Utility.decodeFile( "pointerStroyListJson.txt");
        Log.d(HomeActivity.LogTag, "string in home activity" + str);
        mNewGameButton = (Button) findViewById(R.id.interst);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        audioSeekBar = (SeekBar) findViewById(R.id.audioSeekBarId);

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                //beginPlayingGame();
            }
        });

        requestNewInterstitial();
        mNewGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    beginPlayingGame();
                }
            }
        });

        beginPlayingGame();
        audioPlayerTest = new AudioPlayerTest(this);
       // mainButton = (Button)findViewById(R.id.streamBtn);
       // mainButton.setOnClickListener(this);
      //  mainButton.setVisibility(View.INVISIBLE);
        //audioPlayerViewObj.mainActivityObj = this;
        stopAudioBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //audioPlayerViewObj.stopAudioPlayer();
//                audioPlayerTest.audioSeekBar.setProgress(0);
//                PlayAudioService.bufferPercent = 0;
                audioPlayerTest.stopAudioPlayer();
            }
        });


//        stopAudioBtn.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                //audioPlayerViewObj.stopAudioPlayer();
//
////                audioPlayerTest.audioSeekBar.setProgress(0);
////                PlayAudioService.bufferPercent = 0;
//                audioPlayerTest.stopAudioPlayer();
//                audioSeekBar.setProgress(0);
//                Intent objIntent = new Intent(HomeActivity.this, PlayAudioService.class);
//                Log.d(PlayAudioService.LOG_ID,"........STOP clicked......");
//                stopService(objIntent);
//
//                }
//            });

//            playPauseAudioBtn.setOnClickListener(new OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if(SelectedStoryActivity.audioPlayerViewObj != null){
//
//                    Log.d("HomeActivityaudioPlayerViewObj.mp", String.valueOf(SelectedStoryActivity.audioPlayerViewObj.mp));
//                    SelectedStoryActivity.audioPlayerViewObj.stopAudioPlayer();
//                    Log.d("HomeActivityaudioPlayerViewreleseObj.mp", String.valueOf(SelectedStoryActivity.audioPlayerViewObj.mp));
//                        audioPlayerTest.playOrPauseAudioPlayer();
//                }
//            }
//        });

        //initialize views
        // initializeViews();
        mContext = this;
        mViewFlipper = (ViewFlipper) this.findViewById(R.id.view_flipper);
        mViewFlipper.setAutoStart(true);

        mViewFlipper.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View view, final MotionEvent event) {
                detector.onTouchEvent(event);
                return true;
            }
        });

        findViewById(R.id.play).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //sets auto flipping
                mViewFlipper.setAutoStart(true);
                mViewFlipper.setFlipInterval(3000);
                mViewFlipper.startFlipping();
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.INVISIBLE);

            }
        });

        findViewById(R.id.stop).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //stop auto flipping
                mViewFlipper.stopFlipping();
                play.setVisibility(View.VISIBLE);
                pause.setVisibility(View.INVISIBLE);
            }
        });
        //animation listener
        mAnimationListener = new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                //animation started event
            }
            public void onAnimationRepeat(Animation animation) {
            }
            public void onAnimationEnd(Animation animation) {
                //TODO animation stopped event
            }
        };

        Prastawana.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                if (networkInfo != null && networkInfo.isConnected()) {
                    stopService();
                    audioPlayerTest.SongUrl = url;
                    audioPlayerTest.playOrPauseBtn.setText(audioPlayerTest.pauseBtnTitle);
                    audioPlayerTest.playOrPauseBtn.setBackgroundResource(R.drawable.pause);
                    //        visible();
                    audioPlayerTest.playAudioFile();
                }
                else {
                    String errMsg = "";
                    if (networkInfo != null){
                        errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                    }else{
                        errMsg = "\n\n No connection.. \n\n";
                        Utility.showDialog(HomeActivity.this);
                      //  Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        audioPlayerTest.stopAudioPlayer();
//    }

    private void stopService() {
        audioPlayerTest.stopAudioPlayer();
    }

    void basicSetupForAudioView()
    {
        if (audioPlayerTest == null){
            audioPlayerTest = new AudioPlayerTest(this);
        }
        audioPlayerTest.initBroadcastReceiver();
        audioPlayerTest.registerBroadcastReceiver();
        audioPlayerTest.audioSeekBar = (SeekBar) findViewById(R.id.audioSeekBarId);
        audioPlayerTest.audioSeekBar.setOnSeekBarChangeListener(audioPlayerTest);
        audioPlayerTest.bufferTextView = (TextView) findViewById(R.id.bufferTextViewId);
        audioPlayerTest.bufferText = (TextView) findViewById(R.id.bufferTextView);
        audioPlayerTest.playTextView = (TextView) findViewById(R.id.playedTextViewId);
        audioPlayerTest.playText = (TextView) findViewById(R.id.playedTextView);
        playPauseAudioBtn = (Button) findViewById(R.id.playAudioBtnId);
        audioPlayerTest.ownerActivity = this;
        audioPlayerTest.playOrPauseBtn = playPauseAudioBtn;
        audioPlayerTest.stopPlayer = stopAudioBtn;
        audioPlayerTest.audioPlayerLayout = (RelativeLayout) findViewById(R.id.playerLayout);
        audioPlayerTest.linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        audioPlayerTest.handleAudioViewAppearance();
//        audioPlayerTest.bufferTextView.setVisibility(View.INVISIBLE);
//        audioPlayerTest.bufferText.setVisibility(View.INVISIBLE);
//        audioPlayerTest.playTextView.setVisibility(View.INVISIBLE);
//        audioPlayerTest.playText.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResume(){
        super.onResume();
    //    Log.d(LogTag, "*************** Home onResume **************");
        basicSetupForAudioView();
        if(PlayAudioService.mp == null) {
            audioPlayerTest.stopAudioPlayer();
        }
      else  if(PlayAudioService.mp != null) {
            if (!PlayAudioService.mp.isPlaying()) {
      //          Log.d(PlayAudioService.LOG_ID, "........check mp......" + PlayAudioService.mp);
                audioPlayerTest.stopAudioPlayer();
      //          Log.d(PlayAudioService.LOG_ID, "........check mp2......" + PlayAudioService.mp);
            } else {
                audioPlayerTest.makeAudioUIVisible();
            }
        }
//        if(showVersionUpdate == false) {

            latestPlayStoreVersionValue = Integer.parseInt(latestPlayStoreVersion);
            currentPlayStoreVersionValue = Integer.parseInt(currentPlayStoreVersion);
            if (latestPlayStoreVersionValue > currentPlayStoreVersionValue) {
                Log.d(HomeActivity.LogTag, "\nlatestPlayStoreVersion: " + latestPlayStoreVersion + "\n" + "\ncurrentPlayStoreVersion: " + currentPlayStoreVersion);
                showVersionUpdateDialog();
//                showVersionUpdate = true;
//            }
        }
    }

    public void playAudio(View view) {
        audioPlayerTest.SongUrl = url;
            audioPlayerTest.playOrPauseAudioPlayer();

      //  Log.d(PlayAudioService.LOG_ID, "........PLAY clicked......");
    }
    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void beginPlayingGame() {
        // Play for a while, then display the New Game Button
    }




//    String sMyText = "some text";
//    int nMyInt = 10;

//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        // Save away the original text, so we still have it if the activity
//        // needs to be killed while paused.
//        outState.putString("my_text", sMyText);
//        outState.putInt("my_int", nMyInt);
//        Toast.makeText(this, "onSaveInstanceState()", Toast.LENGTH_LONG).show();
//        Log.i("onSaveInstanceState", "onSaveInstanceState()");
//    }
//
//    String sNewMyText = "";
//    int nNewMyInt = 0;
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        // restore saved values
//        sNewMyText = savedInstanceState.getString("my_text");
//        nNewMyInt = savedInstanceState.getInt("my_int");
//        Toast.makeText(this, "onRestoreInstanceState()", Toast.LENGTH_LONG).show();
//        Log.i("onRestoreInstanceState", "onRestoreInstanceState()");
//
//    }
//
//
//
//
//    void OnDestroy() { }

   /* @Override
    protected void onPause(){
        super.onPause();
        if (audioPlayerViewObj.mp != null) {
            audioPlayerViewObj.mp.pause();
            audioPlayerViewObj.playOrPauseBtn.setText(audioPlayerViewObj.playBtnTitle);
            audioPlayerViewObj.playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

            if (audioPlayerViewObj.mp == null) {

            }

           else{
            audioPlayerViewObj.mp.start();
                audioPlayerViewObj.playOrPauseBtn.setText(audioPlayerViewObj.pauseBtnTitle);
                audioPlayerViewObj.playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_pause);

            }


    }*/
    //    @Override
//    protected void onResume(){
//        super.onResume();
//        if (audioPlayerViewObj.mp == null) {
//            audioPlayerViewObj.mp.start();
//        }
//    }
    /*public void initializeViews(){
        songName = (TextView) findViewById(R.id.songName);
        mediaPlayer = MediaPlayer.create(this, R.raw.prastawana);
        finalTime = mediaPlayer.getDuration();
        duration = (TextView) findViewById(R.id.songDuration);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        songName.setText("");

        seekbar.setMax((int) finalTime);
        seekbar.setClickable(true);
    }


    // play mp3 song
    public void play(View view) {
        mediaPlayer.start();
        timeElapsed = mediaPlayer.getCurrentPosition();
        seekbar.setProgress((int) timeElapsed);
        durationHandler.postDelayed(updateSeekBarTime, 100);
    }

    //handler to change seekBarTime
    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {
            //get current position
            timeElapsed = mediaPlayer.getCurrentPosition();
            //set seekbar progress
            seekbar.setProgress((int) timeElapsed);
            //set time remaing
            double timeRemaining = finalTime - timeElapsed;
            duration.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed), TimeUnit.MILLISECONDS.toSeconds((long) timeRemaining) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining))));

            //repeat yourself that again in 100 miliseconds
            durationHandler.postDelayed(this, 100);
        }
    };

    // pause mp3 song
    public void pause(View view) {
        mediaPlayer.pause();
    }

    // go forward at forwardTime seconds
    public void forward(View view) {
        //check if we can go forward at forwardTime seconds before song endes
        if ((timeElapsed + forwardTime) <= finalTime) {
            timeElapsed = timeElapsed + forwardTime;

            //seek to the exact second of the track
            mediaPlayer.seekTo((int) timeElapsed);
        }
    }

    // go backwards at backwardTime seconds
    public void rewind(View view) {
        //check if we can go back at backwardTime seconds after song starts
        if ((timeElapsed - backwardTime) > 0) {
            timeElapsed = timeElapsed - backwardTime;

            //seek to the exact second of the track
            mediaPlayer.seekTo((int) timeElapsed);
        }
    }*/

 /*   @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
//        // Locate MenuItem with ShareActionProvider
//        MenuItem item = menu.findItem(R.id.menu_item_share);
//
//        // Fetch and store ShareActionProvider
//        mShareActionProvider = (ShareActionProvider) item.getActionProvider();
//
//        // Return true to display menu
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {

            case R.id.action_settings1:
                Intent intent = new Intent(HomeActivity.this,StoryListActivity.class);
                startActivity(intent);
               // finish();

                return true;

            case R.id.action_settings2:
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        loadDataFromServer();
                    }
                });

                thread.start();
                Intent intent1 = new Intent(HomeActivity.this,EarnPointsActivity.class);
                startActivity(intent1);

                return true;

            case R.id.action_settings3:
                showTermsAndCondition();

                return true;

            case R.id.action_settings4:

                Intent intent2 = new Intent(HomeActivity.this,FeedBackActivity.class);
                startActivity(intent2);
                return true;

            case R.id.action_settings5:

                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("plain/text");
                sendIntent.setData(Uri.parse("friends@GoStories.co.in"));
                sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"friends@GoStories.co.in"});
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");
//                startActivity(sendIntent);
                startActivity(Intent.createChooser(sendIntent, "Select Email Client"));
                return true;

            case R.id.action_settings6:
                String url = "http://gostories.co.in/Sandeep/DynamicData/AboutUs.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                return true;

            case R.id.action_settings7:
                String deviceName ="Device Model Name : " + android.os.Build.MODEL;
                String deviceMan = "Device Manufacturer : " + android.os.Build.MANUFACTURER;
                deviceTechnicalDetailsString = String.valueOf(BuildConfig.VERSION_NAME);
                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + "\n" +Utility.getOSDetails() +"\n" + "\n" + deviceMan +"\n" + "\n" + deviceName;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MODEL;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MANUFACTURER;
                String msg = "Current App Version : " + deviceTechnicalDetailsString;
                String title = "Technical Details";
                Utility.showDialog(HomeActivity.this, msg, title);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

     void loadDataFromServer() {

        try {
            URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/AndroidAppLink.txt");
        //    Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");

            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String str;
            while ((str = in.readLine()) != null) {
                if(playStoreLink != null) {
                    playStoreLink = str;
                }
            }
        //    Log.d(HomeActivity.LogTag, "\nplayStoreLink: " + playStoreLink + "\n");
            // strJson = str;
            in.close();

            if(playStoreLink.length()==0 || playStoreLink == null){

                playStoreLink = "https://play.google.com/store";
            }

        } catch (Exception error151) {
            String errStr = "Error in load play store link: " + error151;

            Log.e(HomeActivity.LogTag, errStr);
        }
    }

     void loadContactUsFromServer() {


        try {
            URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/ContactUs.txt");
            //    Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            StringBuilder sb = new StringBuilder();
            str = in.readLine();
            while ((str != null)) {

                sb.append(str);
                sb.append("\n");
                str = in.readLine();
                //    UserRegisterationActivity.termAndCondition = str;
            }
            if(contactUsContent != null) {
                contactUsContent = sb.toString();
             //   Log.d(HomeActivity.LogTag, "\nplayStoreLink about us: " + playStoreLink + "\n");
            }
            // strJson = str;
            in.close();
            if(contactUsContent.length()==0 || contactUsContent == null){
                contactUsContent = "Unable to reach server";
            }

        } catch (Exception error152) {
            String errStr = "Error in load play store link: " + error152;
            Log.e(HomeActivity.LogTag, errStr);
        }
    }

     void loadAndroidLatestPlayStoreVersionFromServer() {

        try {
            URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/AndroidLatestPlayStoreVersion.txt");
            //    Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while ((str = in.readLine()) != null) {
                if(latestPlayStoreVersion != null) {
                    latestPlayStoreVersion = str;
                    Log.d(HomeActivity.LogTag, "\nlatestPlayStoreVersion: " + latestPlayStoreVersion + "\n");
                    latestPlayStoreVersionValue = Integer.parseInt(latestPlayStoreVersion);
                    currentPlayStoreVersionValue = Integer.parseInt(currentPlayStoreVersion);
                    if (latestPlayStoreVersionValue > currentPlayStoreVersionValue) {
                        Log.d(HomeActivity.LogTag, "\nlatestPlayStoreVersion: in on create" + latestPlayStoreVersion + "\n" + "\ncurrentPlayStoreVersion: " + currentPlayStoreVersion);
                        showVersionUpdateDialog();
//                showVersionUpdate = true;
//            }
                    }
                }
            }
            //    Log.d(HomeActivity.LogTag, "\nlatestPlayStoreVersion: " + latestPlayStoreVersion + "\n");
            // strJson = str;
            in.close();
            if(latestPlayStoreVersion.length()== 0 || latestPlayStoreVersion == null){
                latestPlayStoreVersion = currentPlayStoreVersion;
            }
        } catch (Exception error153) {
            String errStr = "Error in load play store link: " + error153;
            Log.e(HomeActivity.LogTag, errStr);
        }
    }
     void showVersionUpdateDialog() {

        UserRegisterationActivity.myMsg = new TextView(HomeActivity.this);
        UserRegisterationActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        UserRegisterationActivity.myMsg.setTextSize(20);
        UserRegisterationActivity.myMsg.setText("GoStories version updated");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCustomTitle(UserRegisterationActivity.myMsg);
        // set dialog message
        alertDialogBuilder.setMessage(finalMsg);
        alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton("Update Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        String url = "https://play.google.com/store/apps/details?id=com.sandeep.gostories3";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private void showTermsAndCondition() {
       UserRegisterationActivity.myMsg = new TextView(HomeActivity.this);
        UserRegisterationActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        UserRegisterationActivity.myMsg.setTextSize(20);
        UserRegisterationActivity.myMsg.setText(UserRegisterationActivity.terms);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCustomTitle(UserRegisterationActivity.myMsg);
        if(UserRegisterationActivity.termAndCondition.length()==0)
        {
            UserRegisterationActivity.termAndCondition = UserRegisterationActivity.msg;
        }
        // set dialog message
        alertDialogBuilder.setMessage(UserRegisterationActivity.termAndCondition);
        alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.tandc);
    }

    class SwipeGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_in));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_out));
                    // controlling animation
                    mViewFlipper.getInAnimation().setAnimationListener(mAnimationListener);
                    mViewFlipper.showNext();
                    return true;
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_in));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext,R.anim.right_out));
                    // controlling animation
                    mViewFlipper.getInAnimation().setAnimationListener(mAnimationListener);
                    mViewFlipper.showPrevious();
                    return true;
                }
            } catch (Exception error101) {
                error101.printStackTrace();
                Log.e(HomeActivity.LogTag,"\n  error101. : "+ error101);
            }
            return false;
        }
    }

   // public void startAudioStreaming(View v){ audioPlayerViewObj.streamAudioPlayer();}

    @Override
    public void onClick(View v) {
       // startAudioStreaming(v);
    }

//    // Call to update the share intent
//    private void setShareIntent(Intent shareIntent) {
//        if (mShareActionProvider != null) {
//            mShareActionProvider.setShareIntent(shareIntent);
//        }
//    }
}


