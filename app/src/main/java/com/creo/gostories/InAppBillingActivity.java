package com.creo.gostories;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.creo.gostories.util.IabHelper;
import com.creo.gostories.util.IabResult;
import com.creo.gostories.util.Inventory;
import com.creo.gostories.util.Purchase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;


public class InAppBillingActivity extends Activity {

    static final int RC_REQUEST = CommonVariable.RC_REQUEST;
    String developerPayload = CommonVariable.developerPayload;
    IabHelper mHelper;
    public final static String TAG = CommonVariable.TAG;
    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener;
    IabHelper.QueryInventoryFinishedListener mQueryFinishedListener;
    public ListView skuItemsListView;
    ArrayAdapter<String>  iapListArrayAdapter;
    public static ArrayList<String> skuItemsArrFromServer;
    public ArrayList<String> customIAPArrList = new ArrayList<>();
    ProgressDialog progressDialog;
    static public int purchasedPointsBasket = CommonVariable.purchasedPointsBasket;
    String localStorageStr = "";
    IInAppBillingService mService;
    ServiceConnection mServiceConn;
    final int InAppBillingAPIServicesVersion = CommonVariable.InAppBillingAPIServicesVersion;
    Button syncPurchaseBtn;
    ProgressDialog pdInPromoList;
    Boolean success_response = false;
    Boolean error_response = false;
    Boolean parsing_error = false;
    Boolean in_BalenceError = false;
    static String resultJson = "";
    SharedPreferences pref;
    static String email_ID = "";
    static String  total_point = "";
    static String prevBalanceStr = "", NewBalanceStr = "";
    static int PrevBalance = CommonVariable.PrevBalance;
    static int NewBalance = CommonVariable.NewBalance;
    static String walletBalance = "";
    static String success_msg = "",errorValue = "",errorMessageValue = "";
    static int newPointsToBeAdded = CommonVariable.newPointsToBeAdded;
    static TextView myMsg;
    static String finalMsg = "";
    Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.in_app_billing_layout);
        syncPurchaseBtn = (Button) findViewById(R.id.syncPurchaseButtonId);
       // syncPurchaseBtn.setVisibility(View.INVISIBLE);
        UserRegisterationActivity.inUserRegistration = false;
        loadDataFromServer("http://gostories.co.in/Sandeep/DynamicData/AndroIABProductIdentifiersArr.json");
        //syncPurchase(null);

        customIAPArrList.add("Nishant");
        skuItemsListView = (ListView) findViewById(R.id.skuListView);
        setupListArrayAdapter();
        skuItemsListView.setAdapter(iapListArrayAdapter);
        skuItemsListView.setVisibility(View.INVISIBLE);
        skuItemsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
            // showToastWithMessage("Currently payment gateway is busy.\nPlease try after some time","Can not proceed");
            launchPurchaseFlowForSelectedItem(skuItemsArrFromServer.get(position));
            }
        });

        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjLHoRm2jUumo9CH++q3i8v6tzFZcTYio0IotpL1/RgI20mt0GWHfIZChMN5a6cs3GEy7H9WrRSe73n71cWrMS0X9Amv67l4A/DAZf9mNOxcPL58ykTz+yF06RjwB+Ps4aQUPVPdO8n9dOxrnX32ojzskQjaYOZ5VClwhnlNQ4jJpJocNLXhE3aozlycAB8KQbIZ3e63dFVFEalomv2Vic3013l09IsQIFG9uWFf9CRY2Eecaquiiz6E3hyM94rzg6bj9ixTFSKR1DbCrk6VPoXpIvFKS/CzchV2/kkP56R1FrzqoauX5v7+iLGc2hUVR1oETmdkqnS5vaEjBlY/UHQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);

        startSetupOfIABHelper();
        setupQueryFinishedListener();
        setupPurchaseFinishedListener();
    }

    void setupListArrayAdapter(){
        iapListArrayAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, customIAPArrList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                TextView text = (TextView) view.findViewById(android.R.id.text1);
                text.setTextColor(Color.WHITE);
                text.setPadding(15, 15, 15, 15);
                //    text.setBackgroundResource(R.color.graytransparent);
                text.setBackgroundResource(R.color.transparent);
                return view;
            }
        };
    }

    void showProgressDialogWithMessage(final String msg){
        (InAppBillingActivity.this).runOnUiThread(new Runnable() {
            @Override
            public void run() {
            progressDialog = new ProgressDialog(InAppBillingActivity.this);
            progressDialog.setMessage(msg);
            progressDialog.setCancelable(false);
            progressDialog.show();
            }
        });
    }

    private void updatePointsOnServer() {

        pref = getApplication().getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);
        email_ID = pref.getString(UserRegisterationActivity.email, "");
        total_point = pref.getString(UserRegisterationActivity.total_point, "");
        prevBalanceStr = total_point;
        //    Log.d(HomeActivity.LogTag, "total_point " + prevBalanceStr);
        //    Log.d(HomeActivity.LogTag, "prevBalanceStr " + prevBalanceStr);
        PrevBalance = Integer.parseInt(total_point);
        NewBalance = PrevBalance + newPointsToBeAdded;
        NewBalanceStr = String.valueOf(NewBalance);
        //    Log.d(HomeActivity.LogTag, "NewBalanceStr " + NewBalanceStr);
        HashMap<String, String> details = new HashMap<String, String>();
        details.put(Utility.TAG_EMAIL_ID, email_ID);
        details.put(Utility.PrevBalance, prevBalanceStr);
        details.put(Utility.NewBalance, NewBalanceStr);
        loadAsAsyncTask(details, EarnPointsActivity.targetUpdateBalanceURL);
        newPointsToBeAdded = 0;

    }

    public void loadAsAsyncTask(HashMap<String, String> details, String targetURL){
        pdInPromoList = new ProgressDialog(this);
        pdInPromoList.setMessage("Loading ...");
        pdInPromoList.setCancelable(false);
        pdInPromoList.show();
        try{
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new SendHttpRequestTask(details,targetURL).execute();
            }
            else {

                String errMsg = "";
                if (networkInfo != null){
                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                }else{
                    errMsg = "\n\n No connection.. \n\n";
                    pdInPromoList.dismiss();
                    Utility.showDialog(InAppBillingActivity.this);
                    //    Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();

                }
                //    Log.d(HomeActivity.LogTag, errMsg);
            }
        } catch (Exception error_161) {
            String errStr = "\n\n Error in loadStoryAsAsyncTask: " + error_161+"\n\n";
            //   Log.d(HomeActivity.LogTag,"\n  error_107. : "+ error_107);
            Log.e(HomeActivity.LogTag,"\n  error_161. : "+  errStr);
        }
    }

    private class SendHttpRequestTask extends AsyncTask<String, Void, String>
    {
        HashMap<String, String> newDetails;
        String targetURL;

        public SendHttpRequestTask(HashMap<String, String> details, String targetURL) {
            this.newDetails = details;
            this.targetURL = targetURL;
        }
        @Override
        protected String doInBackground(String... params) {

            try {
                UserRegisterationActivity.inUserRegistration = false;
                resultJson = Utility.sendAsHtmlMultipartForm(newDetails, targetURL);
                success_response = false;
                error_response = false;
                parsing_error = false;
                in_BalenceError = false;
                Utility.connection_error = false;
                parseUpdateBalanceResponse();
            }
            catch (Exception error_301) {
                error_301.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_301. : " + error_301);
                 parsing_error = true;
            }
            return null;
        }

        private void parseUpdateBalanceResponse() {
            try {

                JSONObject jsonObject = new JSONObject(resultJson);

                if (jsonObject.has(UserRegisterationActivity.success)) {

                    SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                    editor.putString(UserRegisterationActivity.total_point, NewBalanceStr);
                    editor.commit();
                    String total_point = pref.getString(UserRegisterationActivity.total_point, "");
                    Log.e(HomeActivity.LogTag, "total_point in In App Purchase" + total_point);
                    success_response = true;
                }

                if (jsonObject.has(UserRegisterationActivity.error))
                {
                    JSONObject errorObj = jsonObject.getJSONObject(UserRegisterationActivity.error);
                    //    Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.error. : " + UserRegisterationActivity.error);
                    if(errorObj.has(UserRegisterationActivity.WALLET_BALANCE)) {
                        //        Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.WALLET_BALANCE. : " + UserRegisterationActivity.WALLET_BALANCE);

                   walletBalance = errorObj.optString(UserRegisterationActivity.WALLET_BALANCE);
                   total_point = walletBalance;
                   SharedPreferences.Editor editor = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE).edit();
                   editor.putString(UserRegisterationActivity.total_point, total_point);
                   editor.commit();
                   //        Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.WALLET_BALANCE. : " + total_point);
                   in_BalenceError = true;
                }
                    if (errorObj.has(EarnPointsActivity.errorCode))
                    {
                        errorValue = errorObj.optString(EarnPointsActivity.errorCode);
                        //        Log.d(HomeActivity.LogTag, "\n  errorValue. : " + errorValue);
                        errorMessageValue = errorObj.optString(EarnPointsActivity.errorMessage);
                        //        Log.d(HomeActivity.LogTag, "\n  errorMessageValue. : " + errorMessageValue);
                        error_response = true;
                    }
                }
            } catch (JSONException error_302) {
                error_302.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error_302. : " + error_302);
                parsing_error = true;
            }
        }

        @Override
        protected void onPostExecute(String data) {

            if (pdInPromoList != null){
                pdInPromoList.dismiss();
            }
            total_point = pref.getString(UserRegisterationActivity.total_point, "");
            //    Log.d(HomeActivity.LogTag, "toal_point in promo with success" + total_point);

            myMsg = new TextView(InAppBillingActivity.this);
            myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            myMsg.setTextSize(20);

                if (success_response) {
                        myMsg.setText(EarnPointsActivity.congratulations);
                        success_msg = "Congratulations you have earn "+newPointsToBeAdded+" "+SelectedStoryActivity.goStoriesPoints+"."+ "\n"+ "Your Wallet Balance is " +total_point+" "+SelectedStoryActivity.goStoriesPoints+".";
                        purchasedPointsBasket = 0;
                        String final_msg = success_msg;
                        finalMsg = final_msg;
                    }

                else if (error_response) {

                    myMsg.setText(errorValue);
                    finalMsg = errorMessageValue;

                } else if (Utility.connection_error) {

                    myMsg.setText("GS Error - C2");
                    finalMsg = UserRegisterationActivity.gss_error301;
                } else if (parsing_error){
                    // set title
                    myMsg.setText("GS Error - P2");
                    finalMsg = UserRegisterationActivity.gss_error301;
                }


            if(in_BalenceError){
                updatePointsOnServer();
            }
            else {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setCustomTitle(myMsg);
                // set dialog message
                alertDialogBuilder.setMessage(finalMsg);
                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }

        }
    }

    public void syncPurchase(View v){
        showProgressDialogWithMessage("Syncing..");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Thread initConsumptionThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        initIAPConsumption();
                    }
                });
                initConsumptionThread.run();
            }
        }, 2000);
    }

    void initIAPConsumption(){
        try {
            Log.d(TAG, "initIAPConsumption");
            mServiceConn = new ServiceConnection() {
                @Override
                public void onServiceDisconnected(ComponentName name) {
                    mService = null;
                    Log.d(TAG, "--------- mService on Service Disconnected ----------- ");
                }

                @Override
                public void onServiceConnected(ComponentName name,
                                               IBinder service) {
                    mService = IInAppBillingService.Stub.asInterface(service);
                    Log.d(TAG, "--------- mService initialized ----------- ");
                    try {
                        checkAndRedeemPurchaseOnGooglePlay();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                        String errMsg = "Unable to consume item. " + e.getLocalizedMessage()+"\nContact GoStories support team.";
                        Log.e(TAG, errMsg);
                        showToastWithMessage(errMsg, "IAP Error - 59");
                    }
                }
            };

            Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
            serviceIntent.setPackage("com.android.vending");
            bindService(serviceIntent, mServiceConn, Context.BIND_AUTO_CREATE);

        } catch (Exception e) {
            e.printStackTrace();
            String errMsg = "Unable to consume previous items. "+e.getLocalizedMessage()+"\nContact GoStories support team.";
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg, "IAP Error - 61");
        }
    }

    public void checkAndRedeemPurchaseOnGooglePlay() throws RemoteException {
        Log.d(TAG, "checkAndRedeemPurchaseOnGooglePlay");
        Bundle ownedItems = mService.getPurchases(3, getPackageName(), "inapp", null);

        // Check response
        int responseCode = ownedItems.getInt("RESPONSE_CODE");
        if (responseCode != 0) {
            String errMsg = "Unable to retrieve owned items. "+"\nContact GoStories support team.";
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg, "IAP Error - 67");
            return;
        }

        try {
            // Get the list of purchased items
            ArrayList<String> purchaseDataList =
                    ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");

            Log.e(TAG, purchaseDataList.toString());
            String purchasedProducts = "Purchased Products:\n";
            if (purchaseDataList.size()>0) {
                syncPurchaseBtn.setVisibility(View.VISIBLE);
                for (String purchaseData : purchaseDataList) {
                    JSONObject o = new JSONObject(purchaseData);
                    String thisPurchasedProductId = o.optString("productId") ;
                    purchasedProducts = purchasedProducts + thisPurchasedProductId + "\n";
                    String thisPurchaseToken = o.optString("token", o.optString("purchaseToken"));
                    consumeAlreadyPurchasedProducts(thisPurchaseToken, thisPurchasedProductId);
//                    if (consumeAlreadyPurchasedProducts(purchaseToken) == false)
//                    {
//                        Log.d(TAG, "purchaseToken: " + purchaseToken);
//                        break;
//                    }
                }
                if (purchasedPointsBasket>0) {
                    newPointsToBeAdded = purchasedPointsBasket;
                    purchasedPointsBasket = 0;
                    updatePointsOnServer();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            String errMsg = "Unable to process previous items "+e.getLocalizedMessage()+"\nContact GoStories support team.";
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg, "IAP Error - 71");
        }
        (InAppBillingActivity.this).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                loadDataFromServer("http://gostories.co.in/Sandeep/DynamicData/AndroIABProductIdentifiersArr.json");
            }
        });
    }

    public void consumeAlreadyPurchasedProducts(String purchaseToken, String purchasedProductId) throws RemoteException {
        try {
            int response = mService.consumePurchase(InAppBillingAPIServicesVersion, getPackageName(), purchaseToken);
            if (response == IabHelper.BILLING_RESPONSE_RESULT_OK){
                String points = getPointsFromProductId(purchasedProductId);
                //showToastWithMessage(points + " have been purchased.\nPlease click on Sync Purchase button to add it to your wallet", "Congrats");
                purchasedPointsBasket = purchasedPointsBasket + Integer.parseInt(points);
            } else{
                //Log.d(TAG, "consumeAlreadyPurchasedProducts response: "+response);
                String errMsg = "Could not consume product";
                switch (response){
                    case IabHelper.BILLING_RESPONSE_RESULT_USER_CANCELED :{
                        errMsg = errMsg + "BILLING_RESPONSE_RESULT_USER_CANCELED";
                    }
                    break;
                    case IabHelper.BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE :{
                        errMsg = errMsg + "BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE";
                    }
                    break;
                    case IabHelper.BILLING_RESPONSE_RESULT_DEVELOPER_ERROR :{
                        errMsg = errMsg + "BILLING_RESPONSE_RESULT_DEVELOPER_ERROR";
                    }
                    break;
                    case IabHelper.BILLING_RESPONSE_RESULT_ERROR :{
                        errMsg = errMsg + "BILLING_RESPONSE_RESULT_ERROR";
                    }
                    break;
                    case IabHelper.BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED :{
                        errMsg = errMsg + "BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED";
                    }
                    break;
                    case IabHelper.BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED :{
                        errMsg = errMsg + "BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED";
                    }
                    break;
                    default:{
                        errMsg = errMsg + "Unknown Error occurred";
                    }
                }

                Log.e(TAG, errMsg);
                showToastWithMessage(errMsg, "IAP Error - 63");
            }
        }catch (Exception e){
            e.printStackTrace();
            String errMsg = "Consumption not completed. "+e.getLocalizedMessage();
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg, "IAP Error - 65");
        }
    }

    public void startSetupOfIABHelper(){
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                // Log.d(TAG, "Setup finished.");

                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    String errMsg = "Basic setup of IAB helper Failed. " + result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg, "GS Error : IAP-41");
                    return;
                }

                // Have we been disposed of in the meantime? If so, quit.
                if (mHelper == null) {
                    String errMsg = "Billing engine could not be ignited";
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg, "GS Error : IAP-42");
                    return;
                }

                // IAB is fully set up. Now, let's get an inventory of stuff we own.
                //Log.d(TAG, "IabHelper Setup successful..");
                //launchPurchaseFlowForSelectedItem(POINTS_PACK_2000);
            }
        });
    }

    private void loadDataFromServer(final String urlStr) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Connecting to GS store..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                try {
                    localStorageStr = "";

                    URL url = new URL(urlStr);

                    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

                    String str;
                    while ((str = in.readLine()) != null) {
                        localStorageStr = str;
                    }

                    in.close();

                    // Log.d(TAG, " --- "+localStorageStr);
                        // parse json array here and populate InAppBillingActivity.skuItemsArrFromServer
                    // ["com.sandeep.gostories2.p2000","com.sandeep.gostories2.p5000","com.sandeep.gostories2.p12000"]

                    skuItemsArrFromServer = new ArrayList<> ();
                    JSONArray rootJsonArray = new JSONArray(localStorageStr);
                    for(int i=0; i < rootJsonArray.length(); i++){
                        skuItemsArrFromServer.add(rootJsonArray.getString(i).toString());
                    }

                    Log.d(TAG, " skuItemsArrFromServer: "+ skuItemsArrFromServer);
                } catch (Exception error150) {
                    String errStr = "Error in fetching dynamic data: " + error150;
                    // this.errorTextView.setText(errStr);
                    Log.e(TAG, errStr);
                }

                (InAppBillingActivity.this).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (progressDialog != null){
                            progressDialog.dismiss();
                        }
                        loadIAPItems(null);
                    }
                });
            }
        });
        thread.start();
    }

    public void setupQueryFinishedListener(){
        mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
            public void onQueryInventoryFinished(IabResult result, Inventory inventory)
            {
                if (progressDialog != null){
                    progressDialog.dismiss();
                }
//                 Log.d(TAG, "onQueryInventoryFinished...   inventory: " + inventory.toString());
                if (result.isFailure()) {
                    String errMsg = "Problem querying inventory. "+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg, "GS Error : IAP-38");
                    return;
                }

                try
                {
                    customIAPArrList.clear();
                    if (skuItemsArrFromServer.size()>0) {
                        Log.d(TAG, "skuItemsArrFromServer in QueryInventoryFinishedListener: " + skuItemsArrFromServer);
                        for (String skuItem : skuItemsArrFromServer) {
                            if (inventory.getSkuDetails(skuItem) != null) {
                                String price = inventory.getSkuDetails(skuItem).getPrice();
                                String title = inventory.getSkuDetails(skuItem).getTitle();
                                if (title != null && price != null) {
                                    String titleWithPrice = title + "  -  " + price;
                                    customIAPArrList.add(titleWithPrice);
                                }
                            }
                        }
                    }else {
                        customIAPArrList.add("Could not load.. Please try after some time");
                    }

                    Log.d(TAG, "customIAPArrList: " + customIAPArrList);
                    skuItemsListView.setVisibility(View.VISIBLE);
                    iapListArrayAdapter.notifyDataSetChanged();

                    // update the UI
                }
                catch (Exception e){
                    e.printStackTrace();
                    String errMsg = "Query Inventory Finishing Failed. "+e.getLocalizedMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg, "GS Error : IAP- 34");
                }
            }
        };
    }

    public String getPointsFromProductId(String productId){
        String kPoints = "111";
        try{
            String[] separated = productId.split("\\.");
   Log.d(TAG, "--------- productId: "+productId);
            Log.d(TAG, "--------- separated: "+separated);
            if ( separated.length > 0) {
                int lastObjectIndex = separated.length - 1;
             //   Log.d(TAG,"lastObjectIndex: "+ lastObjectIndex);
                kPoints = separated[lastObjectIndex];
                kPoints = separated[lastObjectIndex].substring(1);
            }else {
                String errMsg = "GS Error - IAP23\nCould not process purchased points\nContact GoStories support team.";
                Log.e(TAG, errMsg);
                showToastWithMessage(errMsg, "GS Error : IAP-46");
            }
        } catch (Exception e){
            String errMsg = "GS Error - IAP31\nCould not process purchased points\nContact GoStories support team.";
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg, "GS Error : IAP-48");
        }
       // Log.d(TAG,"kPoints: "+ kPoints+"*************** ");
        return kPoints;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String params = "onActivityResult(" + requestCode + "," + resultCode + "," + data;
        //showToastWithMessage(params, "onActivityResult");
        Log.d(TAG, params);

        // Pass on the activity result to the helper for handling
        if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
            // not handled, so handle it ourselves (here's where you'd
            // perform any handling of activity results not related to in-app
            // billing...
            super.onActivityResult(requestCode, resultCode, data);
            Log.e(TAG, "mHelper did not handle IAP response");
        }
        else {
            Log.d(TAG, "onActivityResult handled by IABUtil.");
           // showToastWithMessage("mHelper handled IAP response", "Congratulations.!");
        }
    }


    public void setupPurchaseFinishedListener(){
        // Callback for when a purchase is finished
        mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            //Log.d(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null){
                String errMsg = "Failed to process purchase "+result.getMessage()+"\n Immediately contact GoStories support team\nfriends@gostories.co.in";
                Log.e(TAG, errMsg);
                showToastWithMessage(errMsg, "GS Error : IAP-157");
                return;
            }

            if (result.isFailure()) {
                if (result.getResponse() == -1005){
                    String errMsg = "Failed to process purchase "+result.getMessage();
                    Log.e(TAG, errMsg);
                }else {
                    String errMsg = "Purchase not processed.\n"+result.getMessage();
                    Log.e(TAG, errMsg);
                    showToastWithMessage(errMsg, "GS Error : IAP-49");
                }
                return;
            }
            try {
                //consumeAlreadyPurchasedProducts(purchase.getToken(), purchase.getSku());
                syncPurchase(null);
                //syncPurchaseBtn.setVisibility(View.VISIBLE);
            }catch (Exception e){
                String errMsg = "Failed to process purchase "+result.getMessage()+"\n Immediately contact GoStories support team\nfriends@gostories.co.in";
                Log.e(TAG, errMsg);
                showToastWithMessage(errMsg, "GS Error : IAP-53");
            }
            }
        };
    }

    public void showToastWithMessage(final String msg, final String title) {
//        mToast = new Toast(InAppBillingActivity.this);
//        if (mToast == null){
//            mToast = new Toast(InAppBillingActivity.this);
//        }else{
//            mToast.cancel();
//        }
//        mToast.makeText(getApplicationContext(),
//                msg, Toast.LENGTH_LONG)
//                .show();


        (InAppBillingActivity.this).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                EarnPointsActivity.myMsg = new TextView(InAppBillingActivity.this);
                EarnPointsActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
                EarnPointsActivity.myMsg.setTextSize(20);
                EarnPointsActivity.myMsg.setText(title);
                Log.d(HomeActivity.LogTag, "Title" + title);
                AlertDialog.Builder builder = new AlertDialog.Builder(InAppBillingActivity.this);
                builder.setCustomTitle(EarnPointsActivity.myMsg);
                builder.setMessage(msg);
                builder.setPositiveButton("OK", null);

                AlertDialog alertDialog = builder.create();
                alertDialog.setCancelable(false);
                alertDialog.show();
//                Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
//                cancelButton.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        alertDialog.dismiss();
//                    }
//                });
            }
        });
    }

    public void queryAsync(){
        try{
            mHelper.queryInventoryAsync(true, skuItemsArrFromServer,
                    mQueryFinishedListener);
        }catch (Exception e){
            String errMsg = "Product inventory not available. "+e.getLocalizedMessage();
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg, "GS Error : IAP-54");

            if (progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

    public void launchPurchaseFlowForSelectedItem(String selectedItemId){
        try {
            if (mHelper.mAsyncInProgress){mHelper.flagEndAsync();}
            mHelper.launchPurchaseFlow(InAppBillingActivity.this, selectedItemId, RC_REQUEST, mPurchaseFinishedListener, developerPayload);
        }catch (Exception e){
            String errMsg = "Failed to start Purchase for: "+selectedItemId+"    "+e.getLocalizedMessage();
            Log.e(TAG, errMsg);
            showToastWithMessage(errMsg, "GS Error : IAP-55");
            if (progressDialog != null){
                progressDialog.dismiss();
            }
        }
    }

    public void loadIAPItems(View v){
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading IAP items..");
        progressDialog.setCancelable(false);
        progressDialog.show();
        queryAsync();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;

        if (mService != null)
            mService = null;
        purchasedPointsBasket = 0;
    }
}
