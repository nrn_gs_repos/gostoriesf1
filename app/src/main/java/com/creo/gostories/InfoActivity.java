package com.creo.gostories;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

/**
 * Created by Admin_2 on 29-07-2015.
 */
public class InfoActivity extends Activity{

    TextView info;
    TextView  name;
    final String author_info = CommonVariable.author_info;
    final String about_artist = CommonVariable.about_artist;
    Context context;
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);
//        AdView mAdView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
        context = getApplicationContext();
        Intent in = getIntent();
        String clickedId = in.getStringExtra("clickedId");
        String IdType = in.getStringExtra("IdType");
        info = (TextView) findViewById(R.id.info);
        name = (TextView) findViewById(R.id.name);
        image = (ImageView) findViewById(R.id.image);
        String artistnewid="a"+clickedId;
        Log.e(HomeActivity.LogTag, clickedId);
        int id = Utility.getImageById(artistnewid, context);
        image.setImageResource(id);

      if(Utility.isExternalStorageReadable()) {
          String str=Utility.decodeFile( "pointerStroyListJson.txt");
         // fatchMyStoryListJson("pointerStroyListJson.txt", clickedId, IdType);
          try {
              parseMyStoryListJson(str,clickedId,IdType);
          } catch (JSONException error112) {
              error112.printStackTrace();
              Log.e(HomeActivity.LogTag, "error112" + error112);
          }
      }
        else{
          try
          {
              fatchMyStoryListJsonFromIM("pointerStroyListJson.txt", clickedId, IdType);
          }
          catch (IOException error113) {
              error113.printStackTrace();
              Log.e(HomeActivity.LogTag, "error113" + error113);
          }
      }
    }


    private void fatchMyStoryListJsonFromIM(String fileName, String clickedId, String IdType) throws IOException {
        try {

            Log.e(HomeActivity.LogTag, "\n mInternalStorageAvailable: " + "true"+"\n");
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new
                    File(getFilesDir() + File.separator + fileName)));
            String read;
            StringBuilder builder = new StringBuilder("");

            while ((read = bufferedReader.readLine()) != null) {
                builder.append(read);
            }
            String jString = builder.toString();
        //    Log.d("Output", builder.toString());
            bufferedReader.close();
            parseMyStoryListJson(jString,clickedId,IdType);


        }catch (Exception error114)
        {
            error114.printStackTrace();
            Log.e(HomeActivity.LogTag, "error114" + error114);
        }


    }
    public  void fatchMyStoryListJson(String fileName,String clickedId,String IdType) {

        try {
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "true"+"\n");
            File dir = Environment.getExternalStorageDirectory();
            File jsonFile = new File(dir + File.separator + "GoStories" + File.separator + fileName);
            FileInputStream stream = new FileInputStream(jsonFile);
            String jString = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                /* Instead of using default, pass in a decoder. */
               jString = Charset.defaultCharset().decode(bb).toString();
            } finally {
                stream.close();
            }

            parseMyStoryListJson(jString,clickedId,IdType);



        }catch (Exception error115)
        {
            error115.printStackTrace();
            Log.e(HomeActivity.LogTag, "error115" + error115);
        }


    }

    private void parseMyStoryListJson(String jString, String clickedId, String IdType) throws JSONException {

        JSONArray jObject = new JSONArray(jString);
        if(IdType.equals(StoryListActivity.author_point)) {
            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jObject.length(); i++) {

                JSONObject jsonObject = jObject.getJSONObject(i);
                JSONObject author_pointJsonObject = jsonObject.getJSONObject(StoryListActivity.author_point);
                String authorId = author_pointJsonObject.optString(StoryListActivity.author_id);
                if(authorId.equals(clickedId)){
                    String authorinfo = author_pointJsonObject.optString(author_info);
                    String authorNameStr = author_pointJsonObject.optString(StoryListActivity.author_name);
                    name.setText(authorNameStr);
                    info.setText(authorinfo);

                }
            }
        }
        else {

            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jObject.length(); i++) {

                JSONObject jsonObject = jObject.getJSONObject(i);
                JSONObject voice_artistJsonObject = jsonObject.getJSONObject(StoryListActivity.voice_artist);
                String artistId = voice_artistJsonObject.optString(StoryListActivity.artist_id);
                if(artistId.equals(clickedId)){
                    String aboutartist = voice_artistJsonObject.optString(about_artist);
                    String artistnameStr = voice_artistJsonObject.optString(StoryListActivity.artist_name);
                    name.setText(artistnameStr);
                    info.setText(aboutartist);

                }
            }
        }
    }
}
