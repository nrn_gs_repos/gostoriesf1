package com.creo.gostories;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Admin_2 on 8/10/15.
 */
public class MenuActivity extends Activity implements View.OnClickListener {

    Button home, collection, earn_points, t_and_c, contact_us, feedback, about_us, technical_details;
    Context context = this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);
        home = (Button) findViewById(R.id.home);
        collection = (Button) findViewById(R.id.collection);
        earn_points = (Button) findViewById(R.id.earnpoints);
        t_and_c = (Button) findViewById(R.id.tandc);
        contact_us = (Button) findViewById(R.id.contactus);
        feedback = (Button) findViewById(R.id.feedback);
        about_us = (Button) findViewById(R.id.aboutus);
        technical_details = (Button) findViewById(R.id.technicaldetails);
        home.setOnClickListener(this);
        collection.setOnClickListener(this);
        earn_points.setOnClickListener(this);
        t_and_c.setOnClickListener(this);
        contact_us.setOnClickListener(this);
        feedback.setOnClickListener(this);
        about_us.setOnClickListener(this);
        technical_details.setOnClickListener(this);
        HomeActivity.currentPlayStoreVersion = String.valueOf(BuildConfig.VERSION_CODE);
        Log.d(HomeActivity.LogTag, "currentPlayStoreVersion  " + HomeActivity.currentPlayStoreVersion);

        Thread thread2 = new Thread(new Runnable(){
            @Override
            public void run() {

                loadAndroidLatestPlayStoreVersionFromServer();
            }
        });

        thread2.start();

        Thread thread1 = new Thread(new Runnable(){
            @Override
            public void run() {
                loadContactUsFromServer();
            }
        });

        thread1.start();
    }

    @Override
    public void onResume() {
        super.onResume();

        HomeActivity.latestPlayStoreVersionValue = Integer.parseInt(HomeActivity.latestPlayStoreVersion);
        HomeActivity.currentPlayStoreVersionValue = Integer.parseInt(HomeActivity.currentPlayStoreVersion);
        if (HomeActivity.latestPlayStoreVersionValue > HomeActivity.currentPlayStoreVersionValue) {
            Log.d(HomeActivity.LogTag, "\nlatestPlayStoreVersion: " + HomeActivity.latestPlayStoreVersion + "\n" + "\ncurrentPlayStoreVersion: " + HomeActivity.currentPlayStoreVersion);
            showVersionUpdateDialog();

        }
    }

    void loadContactUsFromServer() {


        try {
            URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/ContactUs.txt");
            //    Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            StringBuilder sb = new StringBuilder();
            str = in.readLine();
            while ((str != null)) {

                sb.append(str);
                sb.append("\n");
                str = in.readLine();
                //    UserRegisterationActivity.termAndCondition = str;
            }
            if(HomeActivity.contactUsContent != null) {
                HomeActivity.contactUsContent = sb.toString();
                //   Log.d(HomeActivity.LogTag, "\nplayStoreLink about us: " + playStoreLink + "\n");
            }
            // strJson = str;
            in.close();
            if(HomeActivity.contactUsContent.length()==0 || HomeActivity.contactUsContent == null){
                HomeActivity.contactUsContent = "Unable to reach server";
            }

        } catch (Exception error152) {
            String errStr = "Error in load play store link: " + error152;
            Log.e(HomeActivity.LogTag, errStr);
        }
    }

    void loadAndroidLatestPlayStoreVersionFromServer() {

        try {
            URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/AndroidLatestPlayStoreVersion.txt");
            //    Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while ((str = in.readLine()) != null) {
                if(HomeActivity.latestPlayStoreVersion != null) {
                    HomeActivity.latestPlayStoreVersion = str;
                    Log.d(HomeActivity.LogTag, "latestPlayStoreVersion: " + HomeActivity.latestPlayStoreVersion + "\n");
                    Log.d(HomeActivity.LogTag, "latestPlayStoreVersion: on menu" + HomeActivity.latestPlayStoreVersion + "\n" + "\ncurrentPlayStoreVersion: " + HomeActivity.currentPlayStoreVersion);
                    HomeActivity.latestPlayStoreVersionValue = Integer.parseInt(HomeActivity.latestPlayStoreVersion);
                    HomeActivity.currentPlayStoreVersionValue = Integer.parseInt(HomeActivity.currentPlayStoreVersion);
                    if (HomeActivity.latestPlayStoreVersionValue > HomeActivity.currentPlayStoreVersionValue) {
                        Log.d(HomeActivity.LogTag, "latestPlayStoreVersion: in on menu" + HomeActivity.latestPlayStoreVersion + "\n" + "\ncurrentPlayStoreVersion: " + HomeActivity.currentPlayStoreVersion);
                        showVersionUpdateDialog();
//                showVersionUpdate = true;
//            }
                    }
                }
            }
            //    Log.d(HomeActivity.LogTag, "\nlatestPlayStoreVersion: " + latestPlayStoreVersion + "\n");
            // strJson = str;
            in.close();
            if(HomeActivity.latestPlayStoreVersion.length()== 0 || HomeActivity.latestPlayStoreVersion == null){
                HomeActivity.latestPlayStoreVersion = HomeActivity.currentPlayStoreVersion;
            }
        } catch (Exception error153) {
            String errStr = "Error in load play store link: " + error153;
            Log.e(HomeActivity.LogTag, errStr);
        }
    }

    void showVersionUpdateDialog() {

        (MenuActivity.this).runOnUiThread(new Runnable() {
            @Override
            public void run() {


        Log.d(HomeActivity.LogTag, "in dialog from menu: " );
        UserRegisterationActivity.myMsg = new TextView(MenuActivity.this);
        UserRegisterationActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        UserRegisterationActivity.myMsg.setTextSize(20);
        UserRegisterationActivity.myMsg.setText("GoStories version updated");
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCustomTitle(UserRegisterationActivity.myMsg);
        // set dialog message
        alertDialogBuilder.setMessage(HomeActivity.finalMsg);
        alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton("Update Now", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        String url = "https://play.google.com/store/apps/details?id=com.sandeep.gostories3";
                        Intent i = new Intent(Intent.ACTION_VIEW);
                        i.setData(Uri.parse(url));
                        startActivity(i);
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
        Log.d(HomeActivity.LogTag, "in dialog show from menu: ");

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.home:
                Intent home_intent = new Intent(MenuActivity.this,HomeActivity.class);
                startActivity(home_intent);
                break;

            case R.id.collection:
                Intent collection_intent = new Intent(MenuActivity.this,StoryListActivity.class);
                startActivity(collection_intent);
                break;

            case R.id.earnpoints:
                Thread thread = new Thread(new Runnable(){
                    @Override
                    public void run() {
                        loadDataFromServer();
                    }
                });

                thread.start();
                Intent intent1 = new Intent(MenuActivity.this,EarnPointsActivity.class);
                startActivity(intent1);
                break;

            case R.id.tandc:
                showTermsAndCondition();
                break;
            case R.id.contactus:
                Intent intent2 = new Intent(MenuActivity.this,FeedBackActivity.class);
                startActivity(intent2);
                break;

            case R.id.feedback:
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("plain/text");
                sendIntent.setData(Uri.parse("friends@GoStories.co.in"));
                sendIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");
                sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"friends@GoStories.co.in"});
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");
//                startActivity(sendIntent);
                startActivity(Intent.createChooser(sendIntent, "Select Email Client"));
                break;

            case R.id.aboutus:
                String url = "http://gostories.co.in/Sandeep/DynamicData/AboutUs.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

                break;

            case R.id.technicaldetails:
                String deviceName ="Device Model Name : " + android.os.Build.MODEL;
                String deviceMan = "Device Manufacturer : " + android.os.Build.MANUFACTURER;
                HomeActivity.deviceTechnicalDetailsString = String.valueOf(BuildConfig.VERSION_NAME);
                HomeActivity.deviceTechnicalDetailsString = HomeActivity.deviceTechnicalDetailsString +"\n" + "\n" +Utility.getOSDetails() +"\n" + "\n" + deviceMan +"\n" + "\n" + deviceName;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MODEL;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MANUFACTURER;
                String msg = "Current App Version : " + HomeActivity.deviceTechnicalDetailsString;
                String title = "Technical Details";
                Utility.showDialog(MenuActivity.this, msg, title);
                break;
        }
    }

    void loadDataFromServer() {

        try {
            URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/AndroidAppLink.txt");
            //    Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");

            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String str;
            while ((str = in.readLine()) != null) {
                if(HomeActivity.playStoreLink != null) {
                    HomeActivity.playStoreLink = str;
                }
            }
            //    Log.d(HomeActivity.LogTag, "\nplayStoreLink: " + playStoreLink + "\n");
            // strJson = str;
            in.close();

            if(HomeActivity.playStoreLink.length()==0 || HomeActivity.playStoreLink == null){

                HomeActivity.playStoreLink = "https://play.google.com/store";
            }

        } catch (Exception error151) {
            String errStr = "Error in load play store link: " + error151;

            Log.e(HomeActivity.LogTag, errStr);
        }
    }

    private void showTermsAndCondition() {
        UserRegisterationActivity.myMsg = new TextView(MenuActivity.this);
        UserRegisterationActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        UserRegisterationActivity.myMsg.setTextSize(20);
        UserRegisterationActivity.myMsg.setText(UserRegisterationActivity.terms);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCustomTitle(UserRegisterationActivity.myMsg);
        if(UserRegisterationActivity.termAndCondition.length()==0)
        {
            UserRegisterationActivity.termAndCondition = UserRegisterationActivity.msg;
        }
        // set dialog message
        alertDialogBuilder.setMessage(UserRegisterationActivity.termAndCondition);
        alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.tandc);
    }

}
