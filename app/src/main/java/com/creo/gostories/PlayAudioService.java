package com.creo.gostories;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.SeekBar;

import java.util.concurrent.TimeUnit;

public class PlayAudioService extends Service implements MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener,
         MediaPlayer.OnBufferingUpdateListener
{

    static int timeElapsed = CommonVariable.timeElapsedInt;
    public static int bufferPercent = CommonVariable.bufferPercent;
   // boolean bufferingInProgress = true;
	public static final String LOG_ID = HomeActivity.LogTag;
	static MediaPlayer mp;
    static LocalBroadcastManager broadcaster;
    AudioPlayerTest audioPlayerTest;
    public static boolean playerShouldResumeAfterInteruption = false;
    public static boolean completeAudio = false;
    static android.os.Handler durationHandler = new android.os.Handler();
    private static int mediaFileLengthInMilliseconds;
    static final public String EROOR_NOTIFICATION = CommonVariable.EROOR_NOTIFICATION;
    static final public String NISHANT_NOTIFICATION = CommonVariable.NISHANT_NOTIFICATION;
    static final public String NISHANT_SEEKBAR_PROGRESS = CommonVariable.NISHANT_SEEKBAR_PROGRESS;
    static final public String NISHANT_BUFFER_PROGRESS = CommonVariable.NISHANT_BUFFER_PROGRESS;
    static final public String NISHANT_DURATION = CommonVariable.NISHANT_DURATION;
    static final public String NISHANT_COMPLETION = CommonVariable.NISHANT_COMPLETION;
    static final public String NISHANT_ERROR = CommonVariable.NISHANT_ERROR;
    public static String mpError = "mpError";
//    // indicates the state our service:
//    enum State {
//        Retrieving, // the MediaRetriever is retrieving music
//        Stopped, // media player is stopped and not prepared to play
//        Preparing, // media player is preparing...
//        Playing, // playback active (media player ready!). (but the media player may actually be
//        // paused in this state if we don't have audio focus. But we stay in this state
//        // so that we know we have to resume playback once we get focus back)
//        Paused
//        // playback paused (media player ready!)
//    };
//
//    State mState = State.Retrieving;

    public static void sendResult(String message, String bufferPercent, String duration, String complete) {
        Intent intent = new Intent(NISHANT_NOTIFICATION);
        if(message != null && bufferPercent !=null)
        intent.putExtra(NISHANT_SEEKBAR_PROGRESS, message);
        intent.putExtra(NISHANT_BUFFER_PROGRESS, bufferPercent);
        intent.putExtra(NISHANT_DURATION, duration);
        intent.putExtra(NISHANT_COMPLETION, complete);
        broadcaster.sendBroadcast(intent);
    }

    public static void sendErrorResult(String onError) {
        Intent intent = new Intent(EROOR_NOTIFICATION);
        intent.putExtra(NISHANT_ERROR, onError);
        broadcaster.sendBroadcast(intent);
    }

    public void onCreate(){
	    super.onCreate();
        audioPlayerTest = HomeActivity.audioPlayerTest;
        broadcaster = LocalBroadcastManager.getInstance(this);
	    //Log.d(LOG_ID, "Service Started!");
	}

    void playMyMediaPlayer(){
        completeAudio = false;
        //Log.d(LOG_ID, " In playMyMediaPlayer");
        try {
            if (mp == null) {
                mp = new MediaPlayer();
                mp.setOnPreparedListener(this);
                mp.setOnBufferingUpdateListener(this);
                mp.setOnCompletionListener(this);
                mp.setOnErrorListener(this);
            }
            else
                mp.reset();
                mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mp.setDataSource(AudioPlayerTest.SongUrl);
                mp.prepareAsync();
        }

        catch(Exception e) {
            String errStr = "GS Error-41 : " + e.getMessage();
            Log.e(LOG_ID, errStr);
        }
    }

    static Runnable updateSeekBarTime = new Runnable() {
        public void run() {
        try{
             timeElapsed = 0;
            if (mp != null) {
                timeElapsed = mp.getCurrentPosition();
                durationHandler.postDelayed(this, 100);
                sendResult(""+timeElapsed,""+bufferPercent,""+mediaFileLengthInMilliseconds,""+completeAudio);
            } else{
                // nothing to do
            }
        }catch (Exception e){
            if (mp != null){
                mp.release();
                mp = null;
            }
            durationHandler.removeCallbacks(updateSeekBarTime);
            String errStr = "GS Error-42 : " + e.getMessage();
            Log.e(LOG_ID, errStr);
        }
        }
    };


	public int onStartCommand(Intent intent, int flags, int startId){
        playMyMediaPlayer();
	    return 1;
	}

	public void onStop(){
		mp.stop();
		mp.release();
    }
	
	public static void onPause()
    {
        if (mp != null) {
            mp.pause();
            mp.release();
        }
	}
	
	public  void onDestroy(){
       // Log.d(LOG_ID, "In on Destroy");


        if (mp != null) {
            durationHandler.removeCallbacks(updateSeekBarTime);
            mp.stop();
            mp.release();
            //Log.d(LOG_ID, "Destroy completed in codition check");
        }

        mp = null;
        // Log.d(LOG_ID, "Destroy completed");
	}

	@Override
	public IBinder onBind(Intent objIndent) {
	    return null;
	}

    public void onPrepared(MediaPlayer mp) {
    //    Log.d(LOG_ID, " In onPrepared");
        mediaFileLengthInMilliseconds = mp.getDuration();
    //    Log.d(LOG_ID, "Duration" + mediaFileLengthInMilliseconds);
        // mp.start();
        durationHandler.postDelayed(updateSeekBarTime, 200);
        TelephonyManager TelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        TelephonyMgr.listen(new TeleListener(),
                PhoneStateListener.LISTEN_CALL_STATE);
        mp.start();
        if (audioPlayerTest != null) {
            audioPlayerTest.playOrPauseBtn.setText(audioPlayerTest.pauseBtnTitle);
            audioPlayerTest.playTextView.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed), TimeUnit.MILLISECONDS.toSeconds((long) timeElapsed) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed))));
        }
    }

    class TeleListener extends PhoneStateListener {
        public void onCallStateChanged(int state, String incomingNumber) {
            super.onCallStateChanged(state, incomingNumber);
            switch (state) {
                case TelephonyManager.CALL_STATE_IDLE:
                    // CALL_STATE_IDLE;
//                    Toast.makeText(getApplicationContext(), "CALL_STATE_IDLE",
//                            Toast.LENGTH_LONG).show();
                    if (mp != null && playerShouldResumeAfterInteruption) {
                        playerShouldResumeAfterInteruption = false;
                        mp.start();
                        audioPlayerTest.playOrPauseBtn.setText(audioPlayerTest.pauseBtnTitle);
                        audioPlayerTest.playOrPauseBtn.setBackgroundResource(R.drawable.pause);
                    }
                    break;
                case TelephonyManager.CALL_STATE_OFFHOOK:
                    // CALL_STATE_OFFHOOK;
//                    Toast.makeText(getApplicationContext(), "CALL_STATE_OFFHOOK",
//                            Toast.LENGTH_LONG).show();
                    if (mp != null) {
                        if (mp.isPlaying()) {
                            audioPlayerTest.playOrPauseBtn.setText(audioPlayerTest.playBtnTitle);
                            audioPlayerTest.playOrPauseBtn.setBackgroundResource(R.drawable.play);
                            mp.pause();
                            playerShouldResumeAfterInteruption = true;
                        }
                    }
                    break;
                case TelephonyManager.CALL_STATE_RINGING:
                    // CALL_STATE_RINGING
//                    Toast.makeText(getApplicationContext(), incomingNumber,
//                            Toast.LENGTH_LONG).show();
//                    Toast.makeText(getApplicationContext(), "CALL_STATE_RINGING",
//                            Toast.LENGTH_LONG).show();
                    if (mp != null) {
                        if (mp.isPlaying()) {
                            audioPlayerTest.playOrPauseBtn.setText(audioPlayerTest.playBtnTitle);
                            audioPlayerTest.playOrPauseBtn.setBackgroundResource(R.drawable.play);
                            mp.pause();
                            playerShouldResumeAfterInteruption = true;
                        }
                    }
                    break;
                default:
                    break;
            }
        }

    }
    public boolean onError(MediaPlayer mp, int what, int extra) {

         String errorStr = "MediaPlayer onError : "+ what+" and "  + extra;
         Log.e(LOG_ID, errorStr);
        //mainActivityObj.errorTextView.setText(errorStr);
         sendErrorResult(""+ mpError);
//        audioSeekBar.setProgress(0);
//        bufferPercent = 0 ;
//        audioSeekBar.setSecondaryProgress(bufferPercent);

         mp.reset();
        // streamAudioPlayer();

        return false;
    }

    public void onCompletion(MediaPlayer mp) {
        completeAudio = true;
        //durationHandler.postDelayed(updateSeekBarTime, 200);
       // audioPlayerTest.stopAudioPlayer();
        //Toast.makeText(getApplicationContext(), "Completed", Toast.LENGTH_LONG).show();
    }


    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if(fromUser) {
            // this is when actually seekbar has been seeked to a new position
            if (mp != null) {
//               if(mp.isPlaying() == false){
//                    mp.start();
//                }

                if (bufferPercent == 0) {
                    seekBar.setProgress(0);
                } else {

                    mp.seekTo(progress);
                    seekBar.setProgress(progress);
                    timeElapsed = progress;
                  //  System.out.println("Buffering: " + bufferPercent);
                }
            }
//            else{
//                seekBar.setProgress(0);
//            }
        }
    }

    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    public void onBufferingUpdate(MediaPlayer mp, int percent) {
        // TODO Auto-generated method stub
        bufferPercent = percent;
        if (percent < 0 && percent <= 100) {
            //System.out.println("Doing math: (" + (Math.abs(percent)-1)*100.0 + " / " +Integer.MAX_VALUE+ ")" );

            bufferPercent = (int) Math.round((((Math.abs(percent) - 1) * 100.0 / Integer.MAX_VALUE)));

           // durationHandler.postDelayed(updateSeekBarTime, 200);
        }
        //progressBar.setProgress(percent);
        //Log.d("Buffering! ", " " + bufferPercent);
       //System.out.println("Buffering: " +bufferPercent);

    }


//    private Runnable updateBufferTime = new Runnable() {
//        public void run() {
//            try{
//                int timeElapsed = 0;
//                if (bufferingInProgress == true) {
//                    timeElapsed = bufferPercent;
//                    // audioSeekBar.setIndeterminate(false);
//                    // mainActivityObj.errorTextView.setText(updateMsg);
//                    if (bufferPercent == 100) {
//                        bufferingInProgress = false;
//                }
//
//            }
//                sendResult("" + timeElapsed, "" + bufferPercent);
//            }
//
//            catch (Exception e){
//                if (mp != null){
//                    mp.release();
//                    mp = null;
//                }
//                durationHandler.removeCallbacks(updateBufferTime);
//                String errStr = "GS Error-43 : " + e.getMessage();
//                Log.e(LOG_ID, errStr);
//            }
//
//        }
//    };



}
