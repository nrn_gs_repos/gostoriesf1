package com.creo.gostories;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Admin_2 on 28-07-2015.
 */
public class SplashActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = CommonVariable.SPLASH_TIME_OUT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread thread1 = new Thread(new Runnable(){
            @Override
            public void run() {
                loadTandCFromServer();
            }
        });
        thread1.start();

        SharedPreferences pref = getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);
        final String restored = pref.getString(UserRegisterationActivity.email, "");
      //  pref.edit().putBoolean("technicalDetail", false).commit();
        new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if(restored.equals("")) {
                    Intent i = new Intent(SplashActivity.this, UserRegisterationActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }
                else{
                    Intent i = new Intent(SplashActivity.this, MenuActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    private void loadTandCFromServer() {
        try {
        URL url = new URL("http://gostories.co.in/Sandeep/DynamicData/TermsConitionsIndented.txt");
          //  Log.d(HomeActivity.LogTag, "\nLoding  data from server\n");
        BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
        String str = "";
        StringBuilder sb = new StringBuilder();
        str = in.readLine();
        while ((str != null)) {
            sb.append(str);
            sb.append("\n");
            str = in.readLine();
        //    UserRegisterationActivity.termAndCondition = str;
        }
            UserRegisterationActivity.termAndCondition = sb.toString();
            Log.d(HomeActivity.LogTag, "\n UserRegisterationActivity.termAndCondition: " +  UserRegisterationActivity.termAndCondition + "\n");
        // strJson = str;
        in.close();
        if( UserRegisterationActivity.termAndCondition.length()==0 ||  UserRegisterationActivity.termAndCondition == null){
            UserRegisterationActivity.termAndCondition =  UserRegisterationActivity.msg;
        }
    } catch (Exception error153) {
        String errStr = "Error in load play store link: " + error153;
        Log.e(HomeActivity.LogTag, errStr);
    }
    }

}

