package com.creo.gostories;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


public class StoryListActivity extends Activity {

    CustomAdapter storyArrayAdapter;
    public ArrayList<StoryListModel> CustomListViewValues = new ArrayList<StoryListModel>();
    ListView storyListView;
    ProgressDialog pdInStoryList;
    static String strJson = "";
  //  ArrayList<String> storyListValues=new ArrayList<String>();
    // ArrayAdapter<String> storyArrayAdapter;
    int newBackGrounfColorForStoryList = CommonVariable.newBackGrounfColorForStoryList;
//    final String Appery_Database_Request_Header = "X-Appery-Database-Id";
//    final String Appery_Database_Id = "55c068b9e4b01c10ea52b3d0";
    public  StoryListActivity storyList = null;
    //public DownloadWebpageTask storyListnew = null;
    static final String story_name = CommonVariable.story_name;
    static final String story_Id = CommonVariable.story_Id;
    static final String book_name = CommonVariable.book_name;
    static final String director = CommonVariable.director;
    static final String author_point = CommonVariable.author_point;
    static final String author_name = CommonVariable.author_name;
    static final String author_id = CommonVariable.author_id;
    static final String artist_id = CommonVariable.artist_id;
    LinearLayout linearLayout;
    Context context = this;
    ExpandableListAdapter listAdapter;
    static String artistCompare = CommonVariable.artistCompare;
    static String autherCompare = CommonVariable.autherCompare;
    static String directorCompare = CommonVariable.directorCompare;
    static String gs_error = CommonVariable.gs_error_contact_msg;
    List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    static final String story_duration = CommonVariable.story_duration;
    static final String voice_artist = CommonVariable.voice_artist;
    static final String artist_name = CommonVariable.artist_name;
    ImageView imageView;
    public static TextView authore,artist,story,filter,ade;
    Button buttonOpenDialog;
    String appVersionNameStr;
    static  String childitemvalue = "";
    static  String superitemvalue = "";
    //final String pointerStroryListApiBaseUrlStr = "http://gostories.co.in/Sandeep/Data/PointerStoryListOld.json";
    final String pointerStroryListApiBaseUrlStr = CommonVariable.pointerStroryListApiBaseUrlStr;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.story_list);
//        AdView mAdView = (AdView) findViewById(R.id.adView);
//        AdRequest adRequest = new AdRequest.Builder().build();
//        mAdView.loadAd(adRequest);
        imageView = (ImageView) findViewById(R.id.loadStoryButtonId);
        authore = (TextView) findViewById(R.id.authorname1);
        artist = (TextView) findViewById(R.id.artistname1);
        story = (TextView) findViewById(R.id.storyname1);
        filter = (TextView) findViewById(R.id.tv);
        UserRegisterationActivity.inUserRegistration = false;
        linearLayout = (LinearLayout) findViewById(R.id.ll1);
        //    onLoadHide();
       // String stringData = "";
//        Intent intent = getIntent();
//        if (null != intent) {
//            stringData = intent.getStringExtra("USERNAME");
//
//        }

        //  Log.d(MainActivity.LogTag, "stringData");
        storyListView = (ListView) findViewById(R.id.storyListViewId);
        // Defined Array values to show in ListView
      /*  storyListValues.add("Test - 1");
        storyListValues.add("Test - 2");
        storyListValues.add("Test - 3");
        storyListValues.add("Test - 4");
        storyListValues.add("Test - 5");
        storyListValues.add("Test - 6");
        storyListValues.add("Test - 7");
        storyListValues.add("Test - 9");
        storyListValues.add("Test - 10");
        storyListValues.add("Test - 11");
        storyListValues.add("Test - 12");
        storyListValues.add("Test - 13");
        storyListValues.add("Test - 14");
        storyListValues.add("Test - 15");*/
        storyList = this;

        storyArrayAdapter = new CustomAdapter(storyList, CustomListViewValues);
        // Assign adapter to ListView
        storyListView.setAdapter(storyArrayAdapter);
        //int versionCode = BuildConfig.VERSION_CODE;
        appVersionNameStr = BuildConfig.VERSION_NAME;
        // ListView Item Click Listener
        storyListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // ListView Clicked item index
      //          int itemPosition = position;
                StoryListModel tempValues = null;
                // ListView Clicked item value

                tempValues = (StoryListModel) CustomListViewValues.get(position);
               // String itemValue = (String) tempValues.getAuthorname();
                // Show Alert
               String storyID = tempValues.getStoryid();
               String storyName = tempValues.getStoryname();
//               Toast.makeText(getApplicationContext(),
//                       "App version : " + appVersionNameStr + "\nClicked on : " + itemValue, Toast.LENGTH_LONG).show();

                Intent intent = new Intent(StoryListActivity.this, SelectedStoryActivity.class);
                intent.putExtra("StoryList", tempValues);
                startActivity(intent);
                Utility.appendLogsOnServer(context, "SI  " + storyID + "\n" + storyName, Utility.userActivityURL);
            }

        });
        buttonOpenDialog = (Button) findViewById(R.id.opendialog);
        loadStoryAsAsyncTask(null);

        buttonOpenDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dailogBoxEnterQuantity();
        //        Log.d(HomeActivity.LogTag, "childitemvalue in activity1: " +childitemvalue);
                childitemvalue = filter.getText().toString();
        //        Log.d(HomeActivity.LogTag, "childitemvalue in activity2: " +childitemvalue);
            }
        });

        //StoryListActivity.childitemvalue = childitemvalue;

     //   Log.d(HomeActivity.LogTag, "childitemvalue in activity3: " +childitemvalue);

    }

    public void applyFilterOnMyStoryList()
    {
     //   Log.d(HomeActivity.LogTag,"\n  applyFilterOnMyStoryList. : "+ childitemvalue);
      //  Log.d(HomeActivity.LogTag,"\n  superitemvalue. : "+ superitemvalue);
        buttonOpenDialog.setBackgroundResource(R.drawable.smallbg);
        buttonOpenDialog.setText(childitemvalue);
        buttonOpenDialog.setTextColor(Color.parseColor("#ffffff"));
        if(superitemvalue.equals(autherCompare)) {

           int lengthAuthor = getAuthorLength(childitemvalue);
        //    Log.d(HomeActivity.LogTag,"\n  lengthAuthor. in filter: "+ lengthAuthor);

            try {
                JSONArray rootJsonArray = new JSONArray(strJson);

                //Iterate the jsonArray and print the info of JSONObjects
                int j = 0;
                StoryListModel[] newValueArr = new StoryListModel[lengthAuthor];
                for(int i=0; i < rootJsonArray.length(); i++){

                    JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                    JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                    String authorNameStr = author_pointJsonObject.optString(author_name);

                    if(childitemvalue.equals(authorNameStr)){

                        String storyNameStr = jsonObject.optString(story_name);
                        String booknameStr = jsonObject.optString(book_name);
                        String directorStr = jsonObject.optString(director);
                        String storyId = jsonObject.optString(story_Id);
                        String storyduration = jsonObject.optString(story_duration);
                        author_pointJsonObject = jsonObject.getJSONObject(author_point);
                        String authorId = author_pointJsonObject.optString(author_id);
                        JSONObject voice_artistJsonObject = jsonObject.getJSONObject(voice_artist);
                        String artistnameStr = voice_artistJsonObject.optString(artist_name);
                        String artistId = voice_artistJsonObject.optString(artist_id);

                //        Log.d(HomeActivity.LogTag,"\n  authorNameStr. : "+ authorNameStr);
                //        Log.d(HomeActivity.LogTag,"\n  storyNameStr. : "+ storyNameStr);
                        final StoryListModel sched1 = new StoryListModel();
                        newValueArr[j] = sched1;
                        /******* Firstly take data in model object ******/
                        sched1.setStoryid(storyId);
                        sched1.setStoryname(storyNameStr);
                        sched1.setBookname(booknameStr);
                        sched1.setDirector(directorStr);
                        sched1.setTimeduration(storyduration);
                        sched1.setAuthorname(authorNameStr);
                        sched1.setAuthoreid(authorId);
                        sched1.setArtistname(artistnameStr);
                        sched1.setArtistid(artistId);
                //        Log.d(HomeActivity.LogTag,"\n  sched. : "+ sched1);
                        j++;
                //        Log.d(HomeActivity.LogTag,"\n  j. : "+ j);
                    }
                }
                updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);

            } catch (JSONException error1102) {
               // String errStr = "Error in parseMyJson in StoryList: " + error102.getMessage();
                Log.e(HomeActivity.LogTag, "GS Error-1102. : " + error1102);
                Utility.appendExceptionLogsOnServer(context, error1102, Utility.exceptionURL);
                Utility.showDialog(StoryListActivity.this, "Filter can not be applied.\n"+error1102.getLocalizedMessage()+"\n"+gs_error, "GS Error-1102");
            }
        }

        if(superitemvalue.equals(artistCompare)) {

        int lengthArtist = getArtistLength(childitemvalue);
    //    Log.d(HomeActivity.LogTag,"\n  lengthAuthor. in filter: "+ lengthArtist);

        try {
            JSONArray rootJsonArray = new JSONArray(strJson);

            //Iterate the jsonArray and print the info of JSONObjects
            int j = 0;
            StoryListModel[] newValueArr = new StoryListModel[lengthArtist];
            for(int i=0; i < rootJsonArray.length(); i++){

                JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                JSONObject voice_artistJsonObject = jsonObject.getJSONObject(voice_artist);
                String artistnameStr = voice_artistJsonObject.optString(artist_name);

                if(childitemvalue.equals(artistnameStr)){

                    String storyNameStr = jsonObject.optString(story_name);
                    String booknameStr = jsonObject.optString(book_name);
                    String directorStr = jsonObject.optString(director);
                    String storyId = jsonObject.optString(story_Id);
                    String storyduration = jsonObject.optString(story_duration);
                    JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                    String authorNameStr = author_pointJsonObject.optString(author_name);
                    author_pointJsonObject = jsonObject.getJSONObject(author_point);
                    String authorId = author_pointJsonObject.optString(author_id);

                    String artistId = voice_artistJsonObject.optString(artist_id);

            //        Log.d(HomeActivity.LogTag,"\n  authorNameStr. : "+ authorNameStr);
            //        Log.d(HomeActivity.LogTag,"\n  storyNameStr. : "+ storyNameStr);
                    final StoryListModel sched1 = new StoryListModel();
                    newValueArr[j] = sched1;
                    /******* Firstly take data in model object ******/
                    sched1.setStoryid(storyId);
                    sched1.setStoryname(storyNameStr);
                    sched1.setBookname(booknameStr);
                    sched1.setDirector(directorStr);
                    sched1.setTimeduration(storyduration);
                    sched1.setAuthorname(authorNameStr);
                    sched1.setAuthoreid(authorId);
                    sched1.setArtistname(artistnameStr);
                    sched1.setArtistid(artistId);
            //        Log.d(HomeActivity.LogTag,"\n  sched. : "+ sched1);
                    j++;
            //        Log.d(HomeActivity.LogTag,"\n  j. : "+ j);
                }
            }
            updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);

        } catch (JSONException error1103) {
            // String errStr = "Error in parseMyJson in StoryList: " + error103.getMessage();
            Log.e(HomeActivity.LogTag,"\n  GS Error-1103. : "+ error1103);
            Utility.appendExceptionLogsOnServer(context, error1103, Utility.exceptionURL);
            Utility.showDialog(StoryListActivity.this, "Filter can not be applied.\n" + error1103.getLocalizedMessage() + "\n" + gs_error, "GS Error-1103");

        }
    }

        if(superitemvalue.equals(directorCompare)) {

            int lengthDirector = getDirectorLength(childitemvalue);
        //    Log.d(HomeActivity.LogTag,"\n  lengthAuthor. in filter: "+ lengthDirector);

            try {
                JSONArray rootJsonArray = new JSONArray(strJson);

                //Iterate the jsonArray and print the info of JSONObjects
                int j = 0;
                StoryListModel[] newValueArr = new StoryListModel[lengthDirector];
                for(int i=0; i < rootJsonArray.length(); i++){

                    JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                    String directorStr = jsonObject.optString(director);

                    if(childitemvalue.equals(directorStr)){
                        JSONObject voice_artistJsonObject = jsonObject.getJSONObject(voice_artist);
                        String artistnameStr = voice_artistJsonObject.optString(artist_name);
                        String storyNameStr = jsonObject.optString(story_name);
                        String booknameStr = jsonObject.optString(book_name);
                        String storyId = jsonObject.optString(story_Id);
                        String storyduration = jsonObject.optString(story_duration);
                        JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                        String authorNameStr = author_pointJsonObject.optString(author_name);
                        author_pointJsonObject = jsonObject.getJSONObject(author_point);
                        String authorId = author_pointJsonObject.optString(author_id);

                        String artistId = voice_artistJsonObject.optString(artist_id);

                //        Log.d(HomeActivity.LogTag,"\n  authorNameStr. : "+ authorNameStr);
                 //       Log.d(HomeActivity.LogTag,"\n  storyNameStr. : "+ storyNameStr);
                        final StoryListModel sched1 = new StoryListModel();
                        newValueArr[j] = sched1;
                        /******* Firstly take data in model object ******/
                        sched1.setStoryid(storyId);
                        sched1.setStoryname(storyNameStr);
                        sched1.setBookname(booknameStr);
                        sched1.setDirector(directorStr);
                        sched1.setTimeduration(storyduration);
                        sched1.setAuthorname(authorNameStr);
                        sched1.setAuthoreid(authorId);
                        sched1.setArtistname(artistnameStr);
                        sched1.setArtistid(artistId);
                //        Log.d(HomeActivity.LogTag,"\n  sched. : "+ sched1);
                        j++;
                //        Log.d(HomeActivity.LogTag,"\n  j. : "+ j);
                    }
                }
                updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);

            } catch (JSONException error1132) {
                // String errStr = "Error in parseMyJson in StoryList: " + error103.getMessage();
                Log.e(HomeActivity.LogTag,"\n  GS Error-1132. : "+ error1132);
                Utility.appendExceptionLogsOnServer(context, error1132, Utility.exceptionURL);
                Utility.showDialog(StoryListActivity.this, "Filter can not be applied.\n" + error1132.getLocalizedMessage() + "\n" + gs_error, "GS Error-1132");

            }
        }
    }

    private int getDirectorLength(String childitemvalue) {

        int lengthDirector = 0;
        try {

            JSONArray rootJsonArray = new JSONArray(strJson);
            //Iterate the jsonArray and print the info of JSONObjects

            for(int i=0; i < rootJsonArray.length(); i++){
                JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                String directorStr = jsonObject.optString(director);
                if(childitemvalue.equals(directorStr)){

                    lengthDirector++;
                }
            }

        } catch (JSONException error1131) {
            //String errStr = "Error in parseMyJson in StoryList: " + error104.getMessage();
            Log.e(HomeActivity.LogTag,"\n  GS Error-1131. : "+ error1131);
            Utility.appendExceptionLogsOnServer(context, error1131, Utility.exceptionURL);
            Utility.showDialog(StoryListActivity.this, "Can not get director length.\n" + error1131.getLocalizedMessage() + "\n" + gs_error, "GS Error-1131");

        }
    //    Log.d(HomeActivity.LogTag,"\n  lengthArtist. : "+ lengthDirector);
        return lengthDirector;
    }

    private int getArtistLength(String childitemvalue) {
        int lengthArtist = 0;
        try {

            JSONArray rootJsonArray = new JSONArray(strJson);
            //Iterate the jsonArray and print the info of JSONObjects

            for(int i=0; i < rootJsonArray.length(); i++){
                JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                JSONObject voice_artistJsonObject = jsonObject.getJSONObject(voice_artist);
                String artistnameStr = voice_artistJsonObject.optString(artist_name);

                if(childitemvalue.equals(artistnameStr)){

                    lengthArtist++;
                }
            }

        } catch (JSONException error1104) {
            //String errStr = "Error in parseMyJson in StoryList: " + error104.getMessage();
            Log.e(HomeActivity.LogTag,"\n  GS Error-1104. : "+ error1104);
            Utility.appendExceptionLogsOnServer(context, error1104, Utility.exceptionURL);
            Utility.showDialog(StoryListActivity.this, "Unable to get artist list length.\n" + error1104.getLocalizedMessage() + "\n" + gs_error, "GS Error-1104");

        }
    //    Log.d(HomeActivity.LogTag,"\n  lengthArtist. : "+ lengthArtist);
        return lengthArtist;

    }

    private int getAuthorLength(String childitemvalue) {
        int lengthAuthor = 0;
        try {

            JSONArray rootJsonArray = new JSONArray(strJson);
            //Iterate the jsonArray and print the info of JSONObjects

            for(int i=0; i < rootJsonArray.length(); i++){
                JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                String authorNameStr = author_pointJsonObject.optString(author_name);
                if(childitemvalue.equals(authorNameStr)){

               lengthAuthor++;
                }
            }

        } catch (JSONException error1105) {
            String errStr = "Error in parseMyJson in StoryList: " + error1105.getMessage();
            Log.e(HomeActivity.LogTag,"\n  GS Error-1105. : "+ error1105);
            Utility.appendExceptionLogsOnServer(context, error1105, Utility.exceptionURL);
            Utility.showDialog(StoryListActivity.this, "Unable to get author list length.\n" + error1105.getLocalizedMessage() + "\n" + gs_error, "GS Error-1105");

        }
    //    Log.d(HomeActivity.LogTag,"\n  lengthAuthor. : "+ lengthAuthor);
        return lengthAuthor;
    }

    private void dailogBoxEnterQuantity() {
        // TODO Auto-generated method stub
        final Dialog dialog = new Dialog(StoryListActivity.this);
        Window window = dialog.getWindow();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // window.setBackgroundDrawable(new
        // ColorDrawable(Color.TRANSPARENT));
       // dialog.setCancelable(false);
        dialog.setContentView(R.layout.list1);
        ExpandableListView expListView;
        expListView = (ExpandableListView) dialog.findViewById(R.id.lvExp);
       // expListView = (ExpandableListView) findViewById(R.id.lvExp);
        // preparing list data
        prepareListData(strJson);

    //    Log.d(HomeActivity.LogTag, "this: "+this+"\nexpListView: " +expListView);

        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);

        if(listAdapter == null)
        {
     //       Log.d(HomeActivity.LogTag, "\n\n Bholenath raksha kare.....\n\n");
        }
        else{
            // setting list adapter
            expListView.setAdapter(listAdapter);
        }

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                // Toast.makeText(getApplicationContext(),
                // "Group Clicked " + listDataHeader.get(groupPosition),
                // Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Expanded",
//                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Collapsed",
//                        Toast.LENGTH_SHORT).show();

            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
//                Toast.makeText(
//                        getApplicationContext(),
//                        listDataHeader.get(groupPosition)
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT)
//                        .show();

                 childitemvalue = listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition) ;
                 superitemvalue = listDataHeader.get(groupPosition) ;
//
              //  new StoryListActivity(childitemvalue1);
               //Log.d(HomeActivity.LogTag, "childitemvalue: " +childitemvalue);

                dialog.dismiss();
                applyFilterOnMyStoryList();

                return false;
            }
        });

        ImageView cancelbtn = (ImageView) dialog.findViewById(R.id.button1);

        cancelbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                if(dialog != null){
                    dialog.dismiss();
        //            Log.d(HomeActivity.LogTag,"\n  cancelbtn. : "+ childitemvalue);
                }
            }
        });
        dialog.show();
    }


    /*
  * Preparing the list data
  */
    private void prepareListData(String strJson) {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
        //parseMyArtistAndAuthoreListJson();
        // Adding child data
        listDataHeader.add(artistCompare);
        listDataHeader.add(autherCompare);
       //listDataHeader.add(directorCompare);
       // Log.d(HomeActivity.LogTag,"\n  strJson. : "+ strJson);
        // Adding child data
        List<String> artist = new ArrayList<String>();
//        artist.add("The Shawshank Redemption");
//        artist.add("The Godfather");
//        artist.add("The Godfather: Part II");
//        artist.add("Pulp Fiction");
//        artist.add("The Good, the Bad and the Ugly");
//        artist.add("The Dark Knight");
//        artist.add("12 Angry Men");

        List<String> authore = new ArrayList<String>();
//        authore.add("The Conjuring");
//        authore.add("Despicable Me 2");
//        authore.add("Turbo");
//        authore.add("Grown Ups 2");
//        authore.add("Red 2");
//        authore.add("The Wolverine");

    //    List<String> directorlist = new ArrayList<String>();
//        comingSoon.add("2 Guns");
//        comingSoon.add("The Smurfs 2");
//        comingSoon.add("The Spectacular Now");
//        comingSoon.add("The Canyons");
//        comingSoon.add("Europa Report");

        try {
            JSONArray rootJsonArray = new JSONArray(strJson);

            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < rootJsonArray.length(); i++){

                JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                String directorStr = jsonObject.optString(director);
                JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                String authorNameStr = author_pointJsonObject.optString(author_name);

                JSONObject voice_artistJsonObject = jsonObject.getJSONObject(voice_artist);
                String artistnameStr = voice_artistJsonObject.optString(artist_name);

                authore.add(authorNameStr);
                artist.add(artistnameStr);
               // directorlist.add(directorStr);
            }

            Set<String> autherSet = new HashSet<>();
            autherSet.addAll(authore);
            authore.clear();
            authore.addAll(autherSet);

            Set<String> artistSet = new HashSet<>();
            artistSet.addAll(artist);
            artist.clear();
            artist.addAll(artistSet);

//            Set<String> directorSet = new HashSet<>();
//            directorSet.addAll(directorlist);
//            directorlist.clear();
//            directorlist.addAll(directorSet);
        } catch (JSONException error1106) {
            String errStr = "Error in parseMyJson in StoryList: " + error1106.getMessage();
            Log.e(HomeActivity.LogTag,"\n  GS Error-1106. : "+ error1106);
            Utility.appendExceptionLogsOnServer(context, error1106, Utility.exceptionURL);
            Utility.showDialog(StoryListActivity.this, "Unable to set artist list and author list.\n" + error1106.getLocalizedMessage() + "\n" + gs_error, "GS Error-1106");

        }

        listDataChild.put(listDataHeader.get(0), artist); // Header, Child data
        listDataChild.put(listDataHeader.get(1), authore);
       // listDataChild.put(listDataHeader.get(2), directorlist);
    }

//    private void parseMyArtistAndAuthoreListJson() {
//    }

/*    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                finish();


                return true;
            case R.id.action_settings1:
                recreate();
                return true;

            case R.id.action_settings2:
                Intent intent1 = new Intent(StoryListActivity.this,PromoCodeActivity.class);
                startActivity(intent1);

                return true;

            case R.id.action_settings3:
                showTermsAndCondition();

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private void showTermsAndCondition() {
        UserRegisterationActivity.myMsg = new TextView(StoryListActivity.this);
        UserRegisterationActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        UserRegisterationActivity.myMsg.setTextSize(20);
        UserRegisterationActivity.myMsg.setText(UserRegisterationActivity.terms);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCustomTitle(UserRegisterationActivity.myMsg);

        // set dialog message
        alertDialogBuilder.setMessage(UserRegisterationActivity.msg);
        alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
        alertDialog.getWindow().setLayout(400,600);
    }*/

   /* public void loadStoryDataOnBackThread (View v){
        Thread thread = new Thread(new Runnable(){
            @Override
            public void run() {
                loadJsonString();
            }
        });
        thread.start();
    }*/

    public void loadStoryAsAsyncTask(View v){
        //   StoryListModel tempObj = new StoryListModel();
        //   StoryListModel[] newValueArr = {tempObj};
        // String[] newValueArr = {"Loading.."};
        buttonOpenDialog.setBackgroundResource(R.drawable.filter);
        buttonOpenDialog.setText("");
        updateListViewWithDataAndBackGroundColor(null, Color.TRANSPARENT);
        onLoadHide();
        pdInStoryList = new ProgressDialog(this);
        pdInStoryList.setMessage("Loading story list..");
        pdInStoryList.setCancelable(false);
        pdInStoryList.show();
        try{
            ConnectivityManager connMgr = (ConnectivityManager)
            getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new DownloadWebpageTask().execute();
            }
            else {
                // display error
                String errMsg = "";
                if (networkInfo != null){
                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                }else{
                    errMsg = "\n\n No connection.. \n\n";
                    pdInStoryList.dismiss();
                    Utility.showDialog(StoryListActivity.this);
                   // Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();

                }
                 Log.e(HomeActivity.LogTag, errMsg);
                // String[] errArr = {errMsg};
                // updateListViewWithDataAndBackGroundColor(errArr, Color.RED);
            }
        } catch (Exception error1107) {
            String errStr = "\n\n Error in loadStoryAsAsyncTask: " + error1107+"\n\n";
            Log.e(HomeActivity.LogTag,"\n  GS Error-1107. : "+ error1107);
            // this.errorTextView.setText(errStr);
            Utility.appendExceptionLogsOnServer(context, error1107, Utility.exceptionURL);
            Utility.showDialog(StoryListActivity.this, "Unable to check network connection.\n" + error1107.getLocalizedMessage() + "\n" + gs_error, "GS Error-1107");
        }
    }


    private void onLoadHide() {
        authore.setVisibility(View.INVISIBLE);
        artist.setVisibility(View.INVISIBLE);
       // filter.setVisibility(View.INVISIBLE);
        story.setVisibility(View.INVISIBLE);
       // ade.setVisibility(View.INVISIBLE);
        imageView.setVisibility(View.INVISIBLE);
        linearLayout.setVisibility(View.INVISIBLE);
        buttonOpenDialog.setVisibility(View.INVISIBLE);

    }

    public void updateListViewWithDataAndBackGroundColor(StoryListModel[] detailsArr, int newBgColor){
        CustomListViewValues.clear();

        if (detailsArr != null){
            for (int j=0; j<detailsArr.length ; j++){
                CustomListViewValues.add(detailsArr[j]);
                //Log.d(HomeActivity.LogTag, "detailsArr in activity: " +detailsArr[j]);
            }
        }

        newBackGrounfColorForStoryList = newBgColor;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run( ) {
                if (pdInStoryList != null){
                    pdInStoryList.dismiss();
                }
                storyListView.setBackgroundColor(newBackGrounfColorForStoryList);
                afterLoadShow();
                storyArrayAdapter.notifyDataSetChanged();
            }
        });
    }

    private void afterLoadShow() {

        authore.setVisibility(View.VISIBLE);
        artist.setVisibility(View.VISIBLE);
       // filter.setVisibility(View.VISIBLE);
        story.setVisibility(View.VISIBLE);
       // ade.setVisibility(View.VISIBLE);
        imageView.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);
        buttonOpenDialog.setVisibility(View.VISIBLE);

    }

    private class DownloadWebpageTask extends AsyncTask<String, String, String> {
        String serverResponse;

        @Override
        protected String doInBackground(String... urls) {
            // Log.e(MainActivity.LogTag, "\n\n"+"doInBackground"+"\n\n");
            String str= "";
            // params comes from the execute() call: params[0] is the url.
            try {

                // Creating service handler class instance
               /* ServiceHandler sh = new ServiceHandler();

                // Making a request to url and getting response
                strJson = sh.makeServiceCall(pointerStroryListApiBaseUrlStr, ServiceHandler.GET);

                Log.d("Response: ", "> " + strJson);*/
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
              // Create a URL for the desired page
             URI uri = new URI(pointerStroryListApiBaseUrlStr);
              String serverAddress = uri.toString();
              URL url = new URL(serverAddress);
              // URL url = new URL(stroryListApiBaseUrl);
              // Open an HTTP  connection to  the URTL
              HttpURLConnection conn = (HttpURLConnection) url.openConnection();
              conn.setDoInput(true); // Allow Inputs
              // conn.setDoOutput(true); // Only use for POST requests
              conn.setUseCaches(false); // Don't use a Cached Copy
             // conn.setRequestProperty(Appery_Database_Request_Header, Appery_Database_Id);
              // conn.setRequestProperty("Connection", "Keep-Alive");
              conn.setRequestMethod("POST");
              conn.connect();
              int lengthOfFile = conn.getContentLength();
              //Log.e(MainActivity.LogTag, "\n lengthOfFile : " + lengthOfFile + " \n");
              int serverResponseCode = conn.getResponseCode();
              String servResp = conn.getResponseMessage();
              serverResponse = "serverResponse: "+serverResponseCode+", "+servResp;
        //       Log.d(HomeActivity.LogTag, "serverResponse : " + serverResponse);
              //InputStream input = new BufferedInputStream(url.openStream(),10*1024);
              BufferedReader input = new BufferedReader(new InputStreamReader(conn.getInputStream()),10*1024);
              //InputStream input = new BufferedInputStream(url.openStream(),10*1024);
              // Output stream to write file in SD card
              byte data[] = new byte[1024];
              long currentProgress = 0;
              int count = 0;
              strJson="";
//                long total = 0;
//                while ((count = input.read(data)) != -1) {
//                    total += count;
//                    // Publish the progress which triggers onProgressUpdate method
//                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
//
//                    // Write data to file
//                    output.write(data, 0, count);
//                }

                while ((str = input.readLine()) != null) {
                    strJson = str ;
                    currentProgress += count;
                    // Publish the progress which triggers onProgressUpdate method
                    publishProgress("" + ((currentProgress * 100) / lengthOfFile));
                    // Write data to file
                }
       //         Log.d(HomeActivity.LogTag, "\n\n strJsonupdated : " + strJson.length()+"\n\n");
        //        Log.d(HomeActivity.LogTag, "\n\n strJsonupdated : " + strJson+"\n\n");
        //        Log.d(HomeActivity.LogTag, "\n\n strJsonupdated : " + strJson+"\n\n");
                input.close();
                conn.disconnect();
            } catch (Exception error1108) {
                String errStr = "Error in load Story JsonString: " + error1108;
                Log.e(HomeActivity.LogTag,"\n  GS Error-1108. : "+ error1108);
                //   Log.e(MainActivity.LogTag, errStr);
                Utility.appendExceptionLogsOnServer(context,error1108, Utility.exceptionURL);
                Utility.showDialog(StoryListActivity.this, "Unable to load Story JsonString.\n" + error1108.getLocalizedMessage() + "\n" + gs_error, "GS Error-1108");
            }
            return strJson;
        }

        protected void onProgressUpdate (String progressValue){
            //   Log.e(MainActivity.LogTag, "\n progressValue: " + progressValue + "\n");
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            if (pdInStoryList != null){
                pdInStoryList.dismiss();
            }
            Utility.saveDataOnPhysicalMemory(strJson, "pointerStroyListJson.txt", getBaseContext());
            // String[] newValueArr = {serverResponse};
            // updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);
            parseMyStoryListJson();
        }
    }

   /* private String downloadUrl(String myUrl) throws IOException {
        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 500;

        try {
            URL url = new URL(myUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds );*/
        /*    conn.setConnectTimeout(15000 /* milliseconds );
      /*      conn.setRequestMethod("GET");
            conn.setDoInput(true);
            // Starts the query
            conn.connect();
            int response = conn.getResponseCode();
         //   Log.d(MainActivity.LogTag, "The response is: " + response);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            String contentAsString = readIt(is, len);
            return contentAsString;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, "UTF-8");
        char[] buffer = new char[len];
        reader.read(buffer);
        return new String(buffer);
    }

   /* public void loadJsonString(){
        try {

//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);

            // Create a URL for the desired page
            URL url = new URL("https://iosish.iriscouch.com/nishant/TestNishant");

        //    Log.e(MainActivity.LogTag, "\nLoding Story data from server\n");

            // Read all the text returned by the server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

            String str;
            while ((str = in.readLine()) != null) {
                // str is one line of text; readLine() strips the newline character(s)
                // Log.e(LogTag, "\nData from server : "+str+"\n");
                strJson = str;
            }
        //    Log.e(MainActivity.LogTag, "\nStory data: " + strJson + "\n");
            // strJson = str;
            in.close();


            if(strJson.length()>1){
                parseMyStoryListJson();
            }
        } catch (Exception e) {
            String errStr = "Error in load Story JsonString: " + e;
            // this.errorTextView.setText(errStr);
        //    Log.e(MainActivity.LogTag, errStr);
        }
    }*/

 /* public File createFileUnderGoStoriesDir(String fileName)
    {
        File storyListLocalJSONFile = null;
        try{
            File GoStoriesDir = new File(Environment.getExternalStorageDirectory().getPath()+ File.separator+"GoStories");
            if (!GoStoriesDir.exists()) {
                if(GoStoriesDir.mkdir()){
                //    Log.e(MainActivity.LogTag, "\n GoStories directory creation SUCCESS \n");
                }else {
                //    Log.e(MainActivity.LogTag, "\n GoStories directory creation FAILED \n");
                }
            }
            storyListLocalJSONFile = new File(GoStoriesDir+ File.separator+fileName);

            if(storyListLocalJSONFile.exists()){
            //    Log.e(MainActivity.LogTag, "\n storyListLocalJSONFile created \n");
            }else {
                if (storyListLocalJSONFile.createNewFile()) {
                //    Log.e(MainActivity.LogTag, "\n storyListLocalJSONFile creation SUCCESS \n");
                }else {
                //    Log.e(MainActivity.LogTag, "\n storyListLocalJSONFile creation FAILED \n");
                }
            }
        }catch (Exception e){

        }
        return storyListLocalJSONFile;
    }*/

    public void parseMyStoryListJson() {
        try {

            JSONArray rootJsonArray = new JSONArray(strJson);
            //Get the instance of JSONArray that contains JSONObjects
            //  JSONArray jsonArray = jsonRootObject.optJSONArray("Employee");
            //  jsonRootObject.getJSONArray();
            StoryListModel[] newValueArr = new StoryListModel[rootJsonArray.length()];
            //Iterate the jsonArray and print the info of JSONObjects
            for(int i=0; i < rootJsonArray.length(); i++){

                JSONObject jsonObject = rootJsonArray.getJSONObject(i);
                String storyNameStr = jsonObject.optString(story_name);
                String booknameStr = jsonObject.optString(book_name);
                String directorStr = jsonObject.optString(director);
                String storyId = jsonObject.optString(story_Id);
                String storyduration = jsonObject.optString(story_duration);

                JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                String authorNameStr = author_pointJsonObject.optString(author_name);
                String authorId = author_pointJsonObject.optString(author_id);

                JSONObject voice_artistJsonObject = jsonObject.getJSONObject(voice_artist);
                String artistnameStr = voice_artistJsonObject.optString(artist_name);
                String artistId = voice_artistJsonObject.optString(artist_id);
                final StoryListModel sched = new StoryListModel();
                newValueArr[i] = sched;
                /******* Firstly take data in model object ******/
                sched.setStoryid(storyId);
                sched.setStoryname(storyNameStr);
                sched.setBookname(booknameStr);
                sched.setDirector(directorStr);
                sched.setTimeduration(storyduration);
                sched.setAuthorname(authorNameStr);
                sched.setAuthoreid(authorId);
                sched.setArtistname(artistnameStr);
                sched.setArtistid(artistId);
                /******** Take Model Object in ArrayList **********/
                //   CustomListViewValues.add(sched);
            }
            // System.out.print(CustomListViewValues);
            updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);

        } catch (JSONException error1109) {
            String errStr = "Error in parseMyJson in StoryList: " + error1109.getMessage();
            Log.e(HomeActivity.LogTag, strJson);
            Log.e(HomeActivity.LogTag,"\n  GS Error-1109. : "+ error1109);
            Utility.appendExceptionLogsOnServer(context, error1109, Utility.exceptionURL);
            Utility.showDialog(StoryListActivity.this, "Unable to parse Story JsonString.\n" + error1109.getLocalizedMessage() + "\n" + gs_error, "GS Error-1109");
            //this.errorTextView.setText(errStr);
            //   Log.e(MainActivity.LogTag, errStr);
        }
    }
   /* public  void parseMyStoryListJson(String fileName) {

        try {

            File dir = Environment.getExternalStorageDirectory();
            File jsonFile = new File(dir + File.separator + "GoStories" + File.separator + fileName);
            FileInputStream stream = new FileInputStream(jsonFile);
            String jString = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                /* Instead of using default, pass in a decoder. */
        /*        jString = Charset.defaultCharset().decode(bb).toString();
            } finally {
                stream.close();
            }

            JSONArray jObject = new JSONArray(jString);


            StoryListModel[] newValueArr = new StoryListModel[jObject.length()];
            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jObject.length(); i++) {
                JSONObject jsonObject = jObject.getJSONObject(i);
                String storyNameStr = jsonObject.optString(story_name);
                String booknameStr = jsonObject.optString(book_name);
                String directorStr = jsonObject.optString(director);
                String storyId = jsonObject.optString(story_Id);
                String storyduration = jsonObject.optString(story_duration);

                JSONObject author_pointJsonObject = jsonObject.getJSONObject(author_point);
                String authorNameStr = author_pointJsonObject.optString(author_name);
                String authorId = author_pointJsonObject.optString(author_id);

                JSONObject voice_artistJsonObject = jsonObject.getJSONObject(voice_artist);
                String artistnameStr = voice_artistJsonObject.optString(artist_name);
                String artistId = voice_artistJsonObject.optString(artist_id);
                final StoryListModel sched = new StoryListModel();
                newValueArr[i] = sched;
                /******* Firstly take data in model object ******/
        /*        sched.setStoryid(storyId);
                sched.setStoryname(storyNameStr);
                sched.setBookname(booknameStr);
                sched.setDirector(directorStr);
                sched.setTimeduration(storyduration);
                sched.setAuthorname(authorNameStr);
                sched.setAuthoreid(authorId);
                sched.setArtistname(artistnameStr);
                sched.setArtistid(artistId);


            }
            updateListViewWithDataAndBackGroundColor(newValueArr, Color.LTGRAY);
        }catch (Exception e)
        {
            e.printStackTrace();
        }


    }*/

//    @Override
//    public void onBackPressed() {
//        super.onBackPressed();
//        Intent intent = new Intent(StoryListActivity.this,HomeActivity.class);
//        startActivity(intent);
//        finish();
//    }
}
