package com.creo.gostories;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin_2 on 13-08-2015.
 */
public class UserRegisterationActivity extends Activity implements View.OnClickListener {

    static String MY_PREFS_NAME = CommonVariable.MY_PREFS_NAME;
    static String email = CommonVariable.email;
    static String terms = CommonVariable.terms;
    EditText emailID;
    static String resultJson = "";
    Boolean parsing_error = false;
    EditText mobileNumber;
    Button register, technicalDetails;
    Context context = this;
    static String success = CommonVariable.success;
    static String error = CommonVariable.error;
    Boolean success_response = false;
    Boolean error_response = false;
    static boolean inUserRegistration = false;
    String success_msg = CommonVariable.success_msg;
    //static String currency_type = "";
    static String not_connected = CommonVariable.not_connected;
    static String connected = CommonVariable.connected;
    String url = CommonVariable.userRegistrationUrl;
  //  String url = "http://gostories.co.in/Sandeep/GoStoriesHost/PHP/newRegistration.php";
    // String url = "http://27.251.53.163:81/gostories/userCreation.php";
  //  String url = "http://192.168.1.14/GoStoriesHost/PHP/newRegistration.php";
    ProgressDialog pd;
    static String emailString = "";
//    static String promoCode;
    static String totalPoint = "";
    static String promo_code = CommonVariable.promo_code;
    static String total_point = CommonVariable.total_point;
    static String WALLET_BALANCE = CommonVariable.WALLET_BALANCE;
    TextView link;
    static String msg = CommonVariable.msg;
            /*"  It is important that you read all the terms and conditions carefully.\n" +
            "The following specifies the terms and conditions under which you may use the “Gostories” App, service, or product. By using, visiting or browsing the \"Gostories\" App, services or product, you accept and agree to be bound by the Terms of Use. If you don’t agree to the Terms of Use, you should not use the website or \"Gostories\" Application or service or product. We may revise the terms at any time by updating the Terms of Use on our site, \"Gostories\" Application, service or product. Continued use of the site, \"Gostories\" App, services or product after such modifications constitutes your acceptance of the revised terms updated by you. You should visit this page periodically to review the Terms.\n" +
            " \n" +
            "Electronic Communication\n" +
            "\n" +
            "When you visit \"Gostories\" App, service or product or send emails to us, you are communicating with us electronically and you likewise consent to receive communications from us. We will communicate with you by email or by posting notices on this site, \"Gostories\" App, service or product. You agree and admit that all agreements, notices, disclosures and other communications sent by you to you electronically satisfy the legal requirements that such communications are in writing. Your consent to electronic agreements is necessary for use of the \"Gostories\" App, service or product.\n" +
            "Copyright\n" +
            "\n" +
            "Content from \"Gostories\" App, service or product may be used and played only for your personal use and you undertake not to use the same for any commercial purpose or any purpose whatsoever. You agree not to modify, reproduce, retransmit, transfer, distribute, sell, broadcast or otherwise exploit the content of “Gostories” app service or product for any commercial purpose without the express written consent of Stay Tuned Media. All content included on the \"Gostories\" App, service or product such as text, graphics, logos, audio clips, video clips, digital downloads, data compilations and software is the sole property of Stay Tuned Media and software developer company “Vision Future Combine” and any infringement by any person will expose themselves action under Copyright Law.\n" +
            "Usage Requirements\n" +
            "\n" +
            "You must be at least 18 years old to access the sections other than kids collection. You must have internet access, a valid email address, and a valid credit/debit card to use website, \"Gostories\" App, service or product. If you use a password to access this site, \"Gostories\" Application, service or product or any portion of it, then you are responsible for maintaining the confidentiality of the password and for restricting access to your computer, mobile or device and you agree to accept responsibility for all activities that occur on your account or with your password. If you are under 18, you may use \"Gostories\" App, service or product only with involvement of a parent or guardian. You agree to ensure that all personal details provided by you will be true, current, complete and accurate and you agree to maintain and update the information on your “Edit Profile” page as required. You agree that we may store and use information you provide (including credit / debit card information) for use in maintaining your account(s) and billing \"Gostories\" App, services, product fees to your credit / debit card.\n" +
            "Subscription Terms\n" +
            "\n" +
            "Members agree to subscribe and pay Monthly / Quarterly / half yearly / annual fee by credit / debit card on permitting us to charge membership dues and any applicable taxes, until the account is cancelled. Stay Tuned Media has the sole discretion to change the Prices of all \"Gostories\" App, service or product content at any time. All such revised prices shall be promptly posted to the \"Gostories\" App, service or product. Members may change their membership plan at any time from “My Account” page after they are Signed In. When a member changes his /her Membership Plan, the change will be implemented from their next billing date.\n" +
            "Reviews and comments\n" +
            "\n" +
            "We may permit visitors to post reviews and/or comments on the \"Gostories\" App, service or product. You agree that any content you contribute will not be illegal, obscene, threatening, defamatory, invasive of privacy, infringing of intellectual property rights, or otherwise objectionable and does not consist of or certain software viruses, political campaigning, commercial solicitation, chain letters, mass mailings or any form of “spam”. We reserve the right to remove or edit such content.\n" +
            "Cancellation Policy\n" +
            "\n" +
            "You may cancel your membership at any time. Your credit / debit card will not be charged during the next billing cycle. To cancel your Membership Plan, you may do so from “Edit Profile” page within the \"Gostories\" App, service or product after you are signed in.\n" +
            "Refund Policy\n" +
            "\n" +
            "We do not refund any subscription charges.\n" +
            "Termination\n" +
            "\n" +
            "We reserve the right to terminate your use of \"Gostories\" App, service or product, without notice, at any time if we determine, in our sole discretion, that you have violated these Terms of Use or any other policy posted on this \"Gostories\" App, service or product.\n" +
            "Limitation of Liability\n" +
            "\n" +
            "The \"Gostories\" App, service, product and content are provided on an “As Is” and “As Available” basis. We make no representation or warranties on any kind, express or implied, as to the operation of the \"Gostories\" App service product and content. Neither we nor any of our supplier Vision Future Combine will be liable to you under any circumstances for any damages arising in connection with \"Gostories\" App service product and/or content.\n" +
            "Privacy Policy\n" +
            "\n" +
            "We do not / will not sell, rent, share or disclose personal information to third parties without prior consent, except in the limited circumstances where we may contract with others to perform tasks or services on our behalf. For example, we might retain a company to process credit / debit card payments.\n" +
            "Security\n" +
            "\n" +
            "This “Gostories” app, service or product and its content are well protected under Copyright Law. You agree that you are not authorized to log into server or account, access data or materials not intended for you, test the vulnerability of our network and security systems or otherwise breach or circumvent our security in any way.";
       */
        static String termAndCondition = "";
        static  String SELF_PROMO_CODE = CommonVariable.SELF_PROMO_CODE;
        static String gss_error301 = CommonVariable.gss_error301;
       // String gss_error302 = "GSS Error-302 : Contact GoStories support team with this error code.friendz@gostories.co.in";
        Boolean cancel = false;
 //       private CheckBox checkbox ;
        static String email_ID = "";
        String mobile_No = "";
        static TextView myMsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registeration_activity);

        emailID = (EditText) findViewById(R.id.editTextEmailID);
        mobileNumber = (EditText) findViewById(R.id.editMoblieNumber);
        register = (Button) findViewById(R.id.register);
        technicalDetails = (Button) findViewById(R.id.technicaldetails);
//        checkbox   = (CheckBox) findViewById(R.id.checkbox);
        emailString = getEmail(context);
//        Log.d(HomeActivity.LogTag, emailString);
        emailID.setText(emailString);
        link = (TextView) findViewById(R.id.link);
        // Linkify.addLinks(link, Linkify.ALL);
 //       link.setMovementMethod(LinkMovementMethod.getInstance());
        String text = "<a href=''> Terms and Conditions </a>";
        link.setText(Html.fromHtml(text));
//        checkbox.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (!checkbox.isChecked()){
//                    checkbox.setBackgroundResource(R.drawable.unchecked);
//                }
//                if (checkbox.isChecked()){
//                    checkbox.setBackgroundResource(R.drawable.checked);
//                }
//            }
//        });
        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
//                startActivity(browserIntent);
                showTermsAndCondition();
            }
        });
        register.setOnClickListener(this);
        technicalDetails.setOnClickListener(this);
        String activeNetwork = "AN  " + Utility.getActiveNetworkType(context);
        String deviceName ="DMN  " + android.os.Build.MODEL;
        String deviceMan = "DM  " + android.os.Build.MANUFACTURER;
        HomeActivity.deviceTechnicalDetailsString = String.valueOf(BuildConfig.VERSION_NAME);
        HomeActivity.deviceTechnicalDetailsString = HomeActivity.deviceTechnicalDetailsString + "\n" + Utility.getOSDetails() + deviceMan  + "\n" + deviceName + "\n" + activeNetwork;
        String msg = "CAV  " + HomeActivity.deviceTechnicalDetailsString;
        Utility.appendLogsOnServerForTechDetail(context, msg, CommonVariable.userRegisterationURL);
      //  Utility.appendLogsOnServerForTechDetail(context, msg, CommonVariable.registrationDetailsURL);

    }

    private void showTermsAndCondition() {
        myMsg = new TextView(UserRegisterationActivity.this);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextSize(20);
        myMsg.setText(terms);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setCustomTitle(myMsg);
        // set dialog message
        if(termAndCondition.length()==0)
        {
            termAndCondition = msg;
        }
        alertDialogBuilder.setMessage(termAndCondition);
        alertDialogBuilder
                .setCancelable(false)
                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        // if this button is clicked, just close
                        // the dialog box and do nothing
                        dialog.cancel();
                    }
                });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
        alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        alertDialog.getWindow().setBackgroundDrawableResource(R.drawable.tandc);
    }

    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        } return account;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.technicaldetails:

                String deviceName ="Device Model Name : " + android.os.Build.MODEL;
                String deviceMan = "Device Manufacturer : " + android.os.Build.MANUFACTURER;
                HomeActivity.deviceTechnicalDetailsString = String.valueOf(BuildConfig.VERSION_NAME);
                HomeActivity.deviceTechnicalDetailsString = HomeActivity.deviceTechnicalDetailsString +"\n" + "\n" +Utility.getOSDetails() +"\n" + "\n" + deviceMan +"\n" + "\n" + deviceName;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MODEL;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MANUFACTURER;
                String msg = "Current App Version : " + HomeActivity.deviceTechnicalDetailsString;
                String titleForTechDetail = "Technical Details";
                Utility.appendLogsOnServerForTechDetail(context, msg, CommonVariable.userRegisterationURL);
                Utility.showDialog(UserRegisterationActivity.this, msg, titleForTechDetail);

                break;

            case R.id.register:
                 email_ID = emailID.getText().toString();
                 mobile_No = mobileNumber.getText().toString();

                String title = "";
                boolean invalidInputs = false;
                if (email_ID.equals("")) {
                    title = "Please Enter Email ID";
                    invalidInputs = true;
                }

                else if (!isValidEmail(email_ID)) {
                    title = "Please Enter valid Email ID";
                    invalidInputs = true;
                }

//                else if (!checkbox.isChecked()){
//                    title = "Please check terms and condition";
//                    invalidInputs = true;
//                }

                if (invalidInputs){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    // set title
                    alertDialogBuilder.setTitle(title);
                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                    //  Toast.makeText(getApplicationContext(), "Please Enter Email ID", Toast.LENGTH_SHORT).show();
                }

                else{
//                    Intent i_register = new Intent(UserRegisterationActivity.this, HomeActivity.class);
//                    startActivity(i_register);
//                    finish();
//                    SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
//                    editor.putBoolean("key_name1", true);
//                    editor.commit();
                    if(isConnected()){
                        Log.d(HomeActivity.LogTag, connected);
//                        pd = new ProgressDialog(UserRegisterationActivity.this);
//                        pd.setMessage("Uploading info...");
//                        pd.setCancelable(false);
//                        pd.show();
//
//                        new HttpAsyncTask().execute(url);
                        //TelephonyManager TelephonyMgr = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
                       // String m_deviceId = TelephonyMgr.getDeviceId();
                        String m_deviceId = Utility.getDeviceID(context);
                        HashMap<String, String> details = new HashMap<String, String>();
                        details.put(Utility.TAG_EMAIL_ID, email_ID);
                        details.put(Utility.RegisteredUDId, m_deviceId);
                        details.put(Utility.RegisteredPhoneNumber, mobile_No);

                        loadAsAsyncTask(details, url);
                    }

                    else{
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle(not_connected);
                        alertDialogBuilder.setMessage("Please check internet connection");
                        // set dialog message
                        alertDialogBuilder
                                .setCancelable(false)
                                .setNegativeButton("Ok",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // if this button is clicked, just close
                                        // the dialog box and do nothing
                                        dialog.cancel();
                                    }
                                });
                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();
                        // Toast.makeText(getApplicationContext(), not_connected, Toast.LENGTH_SHORT).show();
                        Log.d(HomeActivity.LogTag, not_connected);
                    }
                }
                break;
        }
    }

    public void loadAsAsyncTask(HashMap<String, String> details, String targetURL){
        pd = new ProgressDialog(this);
        pd.setMessage("Connecting to GS server...");
        pd.setCancelable(false);
        pd.show();
        try{
            ConnectivityManager connMgr = (ConnectivityManager)
                    getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {
                new SendHttpRequestTask(details,targetURL).execute();
            } else {

                String errMsg = "";
                if (networkInfo != null){
                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
                }else{
                    errMsg = "\n\n No connection.. \n\n";
                    pd.dismiss();
                    Utility.showDialog(UserRegisterationActivity.this);
                    //Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();

                }
                Log.d(HomeActivity.LogTag, errMsg);
            }
        } catch (Exception error1107) {
            String errStr = "\n\n Error in loadStoryAsAsyncTask: " + error1107+"\n\n";
            Log.e(HomeActivity.LogTag,"\n  error1107. : "+ error1107);
            Log.e(HomeActivity.LogTag,"\n  error1107. : "+  errStr);
            Utility.appendExceptionLogsOnServer(context, error1107, CommonVariable.userRegisterationURL);
            Utility.showDialog(UserRegisterationActivity.this, "Unable to check network connection.\n" + error1107.getLocalizedMessage() + "\n" + CommonVariable.gs_error_contact_msg, "GS Error-1107");
        }
    }


    private class SendHttpRequestTask extends AsyncTask<String, Void, String> {
        HashMap<String, String> newDetails;
        String targetURL;

        public SendHttpRequestTask(HashMap<String, String> details, String targetURL) {
            this.newDetails = details;
            this.targetURL = targetURL;
        }

        @Override
        protected String doInBackground(String... params) {

            try {
                inUserRegistration = true;
                resultJson = Utility.sendAsHtmlMultipartForm(newDetails, targetURL);
                parsePromoCodeResponce();

            } catch (Exception error1301) {
                error1301.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error1301. : " + error1301);
                Utility.appendExceptionLogsOnServer(context, error1301, CommonVariable.userRegisterationURL);
                parsing_error = true;
            }
            return null;
        }

        private void parsePromoCodeResponce() {

            try {

                JSONObject jsonObject = new JSONObject(resultJson);

                if (jsonObject.has(success)) {
                    JSONObject successObj = jsonObject.getJSONObject(success);
                    String promoCode = successObj.optString(SELF_PROMO_CODE);
                    String walletBalance = successObj.optString(WALLET_BALANCE);
                    // String totalBalance = successObj.optString(TOTAL_BALANCE);
                //    Log.d(HomeActivity.LogTag, "\n  promoCode. : " + promoCode);
                 //   Log.d(HomeActivity.LogTag, "\n  walletBalance. : " + walletBalance);
                    // Log.d(HomeActivity.LogTag, "\n  totalBalance. : " + totalBalance);
                    SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();

                    editor.putString(email, email_ID);
                    editor.putString(promo_code,promoCode);
                    editor.putString(total_point,walletBalance);
                    editor.commit();
                    success_response = true;
                }

                if (jsonObject.has(error)) {

                    JSONObject errorObj = jsonObject.getJSONObject(UserRegisterationActivity.error);
                //    Log.d(HomeActivity.LogTag, "\n  UserRegisterationActivity.error. : " + UserRegisterationActivity.error);
                    EarnPointsActivity.errorValue = errorObj.optString(EarnPointsActivity.errorCode);
                //    Log.d(HomeActivity.LogTag, "\n  errorValue. : " + PromoCodeActivity.errorValue);
                    EarnPointsActivity.errorMessageValue = errorObj.optString(EarnPointsActivity.errorMessage);
                //    Log.d(HomeActivity.LogTag, "\n  errorMessageValue. : " +PromoCodeActivity.errorMessageValue);
                    error_response = true;
                }
            } catch (JSONException error1302) {
                error1302.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n  error1302. : " + error1302);
                Utility.appendExceptionLogsOnServer(context, error1302, CommonVariable.userRegisterationURL);
                parsing_error = true;
            }
        }

        @Override
        protected void onPostExecute(String data) {

            if (pd != null) {
                pd.dismiss();
            }

            myMsg = new TextView(UserRegisterationActivity.this);
            myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            myMsg.setTextSize(20);


            if (success_response) {

                EarnPointsActivity.pref=getApplication().getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, MODE_PRIVATE);
                totalPoint = EarnPointsActivity.pref.getString(total_point, "");

                myMsg.setText(EarnPointsActivity.congratulations);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                String final_msg = success_msg +totalPoint+ SelectedStoryActivity.goStoriesPoints;
                alertDialogBuilder.setCustomTitle(myMsg);
                // set dialog message
                alertDialogBuilder.setMessage(final_msg);
                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("Start Enjoying", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                                Intent i_register = new Intent(UserRegisterationActivity.this, MenuActivity.class);
                                startActivity(i_register);
                                finish();
//                                cancel = true;
//                                afterCancel();
                            }

                            private void afterCancel() {

                                if (cancel) {
                                    Intent i_register = new Intent(UserRegisterationActivity.this, MenuActivity.class);
                                    startActivity(i_register);
                                    finish();
                                }
                            }
                        });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();

            } else {
                if (error_response) {

                    myMsg.setText(EarnPointsActivity.errorValue);
                    EarnPointsActivity.finalMsg = EarnPointsActivity.errorMessageValue;

                } else if (Utility.connection_error) {

                    myMsg.setText("GS Error - C1");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                } else {
                    // set title
                    myMsg.setText("GS Error - P1");
                    EarnPointsActivity.finalMsg = UserRegisterationActivity.gss_error301;
                }

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                alertDialogBuilder.setCustomTitle(myMsg);
                // set dialog message
                alertDialogBuilder.setMessage(EarnPointsActivity.finalMsg);
                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
        }
        }


 /*   private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            TelephonyManager TelephonyMgr = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
            String m_deviceId = TelephonyMgr.getDeviceId();
            //Toast.makeText(getApplicationContext(),m_deviceId , Toast.LENGTH_SHORT).show();
            Log.e(HomeActivity.LogTag, m_deviceId);
            Registration registration = new Registration();
            registration.setEmailID(email_ID);
            registration.setMobileNumber(mobile_No);
            registration.setUdidNumber(m_deviceId);

            HashMap<String, String> details = new HashMap<String, String>();
            details.put(Utility.TAG_EMAIL_ID, email_ID);
            details.put(Utility.RegisteredUDId, m_deviceId);
            details.put(Utility.RegisteredPhoneNumber, mobile_No);

            Log.d(HomeActivity.LogTag, "\n  email_ID. : " + email_ID);
            Log.d(HomeActivity.LogTag, "\n  mobile_No. : " + mobile_No);
            return POST(urls[0],registration);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            myMsg = new TextView(UserRegisterationActivity.this);
            myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            myMsg.setTextSize(20);
            pd.dismiss();
            if(registrationNew!= null) {
                String walletBalance = registrationNew.getWalletBalance();
                String error = registrationNew.getError();
                String errorKey = registrationNew.getErrorKey();
                Log.d(HomeActivity.LogTag, "\n  WalletBalance. : " + walletBalance);
                Log.d(HomeActivity.LogTag, "\n  error. : " + error);

                    if (success_response) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        // set title
                        myMsg.setText(success);
                      //  alertDialogBuilder.setTitle(success);
                        String final_msg = success_msg + currency_type;
                        alertDialogBuilder.setCustomTitle(myMsg);
                        // set dialog message
                        alertDialogBuilder.setMessage(final_msg);
                        alertDialogBuilder
                                .setCancelable(false)
                                .setNegativeButton("Start Enjoying", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        cancel = true;
                                        afterCancel();
                                    }

                                    private void afterCancel() {

                                        if(cancel){
                                            SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                                            promoCode = registrationNew.getPromoCode();
                                            totalPoint = registrationNew.getTotalPoint();
                                            editor.putString(email, emailID.getText().toString());
                                            editor.putString(promo_code,promoCode);
                                            editor.putString(total_point,totalPoint);
                                            editor.commit();

                                            Intent i_register = new Intent(UserRegisterationActivity.this, HomeActivity.class);
                                            startActivity(i_register);
                                            finish();
                                        }
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();

                        // Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
                    }

                else if(error_response) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    // set title
                    myMsg.setText(errorKey);
                    alertDialogBuilder.setTitle(errorKey);
                    alertDialogBuilder.setCustomTitle(myMsg);
                    // set dialog message
                    alertDialogBuilder.setMessage(error);
                    alertDialogBuilder
                            .setCancelable(false)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                    //  Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                    Log.e(HomeActivity.LogTag, "result" + result);
                }
            }
            else{

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                // set title

                myMsg.setText("GS Error - C1");
                alertDialogBuilder.setCustomTitle(myMsg);
                // set dialog message
                alertDialogBuilder.setMessage(gss_error301);
                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();

            }


        }
    }*/
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

//    public String POST(String url,Registration details){
//        InputStream inputStream = null;
//        String result = "";
//        try{
//            ConnectivityManager connMgr = (ConnectivityManager)
//                    getSystemService(Context.CONNECTIVITY_SERVICE);
//            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
//            if (networkInfo != null && networkInfo.isConnected()) {
//                new SendHttpRequestTask(details,url).execute();
//            }
//            else {
//
//                String errMsg = "";
//                if (networkInfo != null){
//                    errMsg = "\n\n ExtraInfo: " + networkInfo.getExtraInfo() + ". Reason: " + networkInfo.getReason()+"\n\n";
//                }else{
//                    errMsg = "\n\n No connection.. \n\n";
//                    pd.dismiss();
//                    Toast.makeText(getApplicationContext(), "Please check internet connection", Toast.LENGTH_LONG).show();
//
//                }
//                Log.e(HomeActivity.LogTag, errMsg);
//
//
//            }

//            HttpClient httpclient = new DefaultHttpClient();
//            HttpPost httpPost = new HttpPost(url);
//            String json = "";
//            JSONObject jsonObject = new JSONObject();
//            jsonObject.accumulate("GivenPhoneNumber", registration.getMobileNumber());
//            jsonObject.accumulate("GivenEmailId", registration.getEmailID());
//            jsonObject.accumulate("GivenUDId", registration.getUdidNumber());
//            json = jsonObject.toString();
//
//            StringEntity se = new StringEntity(json);
//            httpPost.setEntity(se);
//          //  httpPost.setHeader("Accept", "application/json");
//            httpPost.setHeader("Content-type", "application/json");
//            HttpResponse httpResponse = httpclient.execute(httpPost);
//            inputStream = httpResponse.getEntity().getContent();
//            if(inputStream != null) {
//                result = convertInputStreamToString(inputStream);
//                registrationNew = parseMyStoryListJson(result);
//            }
//            else
//                result = "Not Register";
//
//        } catch (Exception error131) {
//
//            result = error131.getLocalizedMessage();
//            Log.d(HomeActivity.LogTag,"error131 :"+ result);
//        }
//
//        return result;
//    }

  /*  public Registration parseMyStoryListJson(String strJson) {
        Registration registration = new Registration();

        TextView myMsg = new TextView(UserRegisterationActivity.this);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextSize(20);
        try {

           JSONObject jsonObject = new JSONObject(strJson);
           if(jsonObject.has(success)) {
               JSONObject successObj = jsonObject.getJSONObject(success);

               String promoCode = successObj.optString(SELF_PROMO_CODE);
               String walletBalance = successObj.optString(WALLET_BALANCE);
              // String totalBalance = successObj.optString(TOTAL_BALANCE);
               Log.d(HomeActivity.LogTag, "\n  promoCode. : " + promoCode);
               Log.d(HomeActivity.LogTag, "\n  walletBalance. : " + walletBalance);
              // Log.d(HomeActivity.LogTag, "\n  totalBalance. : " + totalBalance);
               registration.setPromoCode(promoCode);
               registration.setWalletBalance(walletBalance);
               registration.setTotalPoint("1000");
               success_response = true;

           }
            if(jsonObject.has(error)) {

                JSONObject errorObj = jsonObject.getJSONObject(error);
//                for(int i = 0;i<errorObj.length();i++) {//
//                    String error = errorObj.toString(i);
//                    Log.d(HomeActivity.LogTag, "\n  error. in loop : " + error);
                Iterator keys = errorObj.keys();
                String currentDynamicKey = (String)keys.next();
                Log.d(HomeActivity.LogTag, "\n  currentDynamicKey. : " + currentDynamicKey);
                // get the value of the dynamic key
                String currentDynamicValue = errorObj.optString(currentDynamicKey);
              //  JSONObject currentDynamicValue = errorObj.getJSONObject(currentDynamicKey);
                Log.d(HomeActivity.LogTag, "\n  currentDynamicValue. : " + currentDynamicValue);
                registration.setError(currentDynamicValue);
                registration.setErrorKey(currentDynamicKey);
                error_response = true;
                // Log.d(HomeActivity.LogTag, "\n  error. : " + errorStr);
           /*     while(keys.hasNext()) {
                        // loop to get the dynamic key
                        String currentDynamicKey = (String)keys.next();
                        Log.d(HomeActivity.LogTag, "\n  currentDynamicKey. : " + currentDynamicKey);
                        // get the value of the dynamic key
                        JSONObject currentDynamicValue = errorObj.getJSONObject(currentDynamicKey);
                        Log.d(HomeActivity.LogTag, "\n  currentDynamicValue. : " + currentDynamicValue);
                        // do something here with the value...
                    }
                    if(!error.equals("")) {

                       // String errorStr = errorObj.;
                        //registration.setError(errorStr);
                       // Log.d(HomeActivity.LogTag, "\n  error. : " + errorStr);
                    }
     /*               String error1 = errorObj.optString(gssError1);
                    String error2 = errorObj.optString(gssError2);
                    String error3 = errorObj.optString(gssError3);
                    String error4 = errorObj.optString(gssError4);

                    Log.d(HomeActivity.LogTag, "\n  error1. : " + error1);
                    Log.d(HomeActivity.LogTag, "\n  error2. : " + error2);
                    Log.d(HomeActivity.LogTag, "\n  error3. : " + error3);
                    Log.d(HomeActivity.LogTag, "\n  error4. : " + error4);

                    if (!error1.equals("")) {
                        registration.setError(error1);
                        Log.d(HomeActivity.LogTag, "\n  error1. : " + error1);
                    }
                    if (!error2.equals("")) {
                        registration.setError(error2);
                        Log.d(HomeActivity.LogTag, "\n  error2. : " + error2);
                    }
                    if (!error3.equals("")) {
                        registration.setError(error3);
                        Log.d(HomeActivity.LogTag, "\n  error3. : " + error3);
                    }
                    if (!error4.equals("")) {
                        registration.setError(error4);
                        Log.d(HomeActivity.LogTag, "\n  error4. : " + error4);
                    }*/
               // }
/*            }
        }
        catch (JSONException error132) {
            String errStr = "Error in parseMyJson in StoryList: " + error132.getMessage();
            Log.d(HomeActivity.LogTag,"\n  error132. : "+ error132);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            // set title

            myMsg.setText("GS Error - P1");
            alertDialogBuilder.setCustomTitle(myMsg);
            // set dialog message
            alertDialogBuilder.setMessage(gss_error302);
            alertDialogBuilder
                    .setCancelable(false)
                    .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();
        }
        return registration;
    }*/

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }
}
