package com.creo.gostories;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Admin_2 on 22-07-2015.
 */
public class Utility {
    static final String PrevBalance = CommonVariable.prevBalance;
    static final String NewBalance = CommonVariable.newBalance;
    static final String TAG_EMAIL_ID = CommonVariable.TAG_EMAIL_ID;
    static final String EMAIL_ID = CommonVariable.EMAIL_ID;
    static final String TAG_PROMO_CODE = CommonVariable.TAG_PROMO_CODE;
    static final String RegisteredUDId = CommonVariable.RegisteredUDId;
    static final String dataString = CommonVariable.dataString;
    static final String RegisteredPhoneNumber = CommonVariable.RegisteredPhoneNumber;
    static final String StoryID = CommonVariable.StoryID;
    static final String isPurchased = CommonVariable.isPurchased;
    static AlertDialog alertDialog;
    static Boolean sdCardAvailable;
    static boolean isConnected = false;
    static Boolean connection_error = false;
    static FileOutputStream fos;
    static String isPurchasedStoryURL = CommonVariable.isPurchasedStoryURL;
    static String purchaseStoryURL = CommonVariable.purchaseStoryURL;
    static SharedPreferences pref;
    static final String exceptionURL = CommonVariable.exceptionURL;
    static final String userActivityURL = CommonVariable.userActivityURL;
    static final String userTechnicalDetailURL = CommonVariable.userTechnicalDetailyURL;
    //private static String algorithm = "AES";
  //  static SecretKey yourKey = null;
    private static final String ALGO = CommonVariable.ALGO;

    private static final byte[] keyValue = CommonVariable.keyValue;
    static String deviceID = "";
    static String emailID = "";

    public static String getDeviceID(Context context) {
        TelephonyManager TelephonyMgr = (TelephonyManager)context.getSystemService(context.TELEPHONY_SERVICE);
        deviceID = TelephonyMgr.getDeviceId();
        Log.d(HomeActivity.LogTag, "In getDeviceID method" + deviceID);
        return deviceID;
    }

    public Bitmap getOptimizedImageFile(File f){

        try {

            //Decode image size
            BitmapFactory.Options o1 = new BitmapFactory.Options();
            o1.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o1);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.

            // Set width/height of recreated image
            final int REQUIRED_SIZE=85;

            int width_tmp=o1.outWidth, height_tmp=o1.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with current scale values
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            System.out.print("in utility"+bitmap);
            return bitmap;

        } catch (FileNotFoundException error119) {
            Log.e(HomeActivity.LogTag, "error119" + error119);
        }
        catch (IOException error120) {
            error120.printStackTrace();
            Log.e(HomeActivity.LogTag, "error120" + error120);
        }
        return null;
    }

    public static int getImageById(String authorId, Context context) {
        int id = context.getResources().getIdentifier(authorId, "drawable", context.getPackageName());
        return id;
    }


    public static File createFileUnderGoStoriesDir(String fileName)
    {
        File storyListLocalJSONFile = null;
        try{
            File GoStoriesDir = new File(Environment.getExternalStorageDirectory().getPath()+ File.separator+".GoStories");
            if (!GoStoriesDir.exists()) {
                if(GoStoriesDir.mkdir()){
                    Log.e(HomeActivity.LogTag, "\n GoStories directory creation SUCCESS \n");
                }else {
                    Log.e(HomeActivity.LogTag, "\n GoStories directory creation FAILED \n");
                }
            }
            storyListLocalJSONFile = new File(GoStoriesDir+ File.separator+fileName);

            if(storyListLocalJSONFile.exists()){
                Log.e(HomeActivity.LogTag, "\n storyListLocalJSONFile created \n");
            }else {
                if (storyListLocalJSONFile.createNewFile()) {
                    Log.e(HomeActivity.LogTag, "\n storyListLocalJSONFile creation SUCCESS \n");
                }else {
                    Log.e(HomeActivity.LogTag, "\n storyListLocalJSONFile creation FAILED \n");
                }
            }
        }catch (Exception error121){
            Log.e(HomeActivity.LogTag, "error121" + error121);

        }
        return storyListLocalJSONFile;
    }




    public static void saveStringOnSDCardUnderFile(String strJson, String filename) {

        try{
            String fileName = filename = "";
            File reqFile = createFileUnderGoStoriesDir(fileName);

            if (reqFile != null){
                FileWriter writer = new FileWriter(reqFile.getAbsolutePath());
                writer.append(strJson);
                writer.flush();
                writer.close();
            }else {
                Log.e(HomeActivity.LogTag, "Could not save file on sd");
            }
            // JSONObject tempObj = new JSONObject(strJson);
            // String tmpStr = tempObj.toString(3);
            // Log.e(MainActivity.LogTag, strJson);

        }catch (Exception error122){

            String errStr = "\n\nError in Saving JsonString to file: " + error122 +"\n\n";
            Log.e(HomeActivity.LogTag, errStr);
            Log.e(HomeActivity.LogTag, "error122" + error122);

        }

    }



    public static boolean isExternalStorageAvailable() {

        String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "true"+"\n");

        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "true"+"\n");
            Log.e(HomeActivity.LogTag, "\n mExternalStorageWriteable: " + "false"+"\n");

        } else {
            // Something else is wrong. It may be one of many other states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "false"+"\n");
        }

        if (mExternalStorageAvailable == true
                && mExternalStorageWriteable == true) {
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "true"+"\n");
            return true;
        } else {
            return false;
        }
    }


    public static void saveDataOnPhysicalMemory(String strJson, String fileName, Context context) {


        sdCardAvailable = isExternalStorageWritable();



        if(sdCardAvailable)
        {
            Log.e(HomeActivity.LogTag, "\n external storage: " +  "\n");
           // saveStringOnSDCardUnderFile(strJson, fileName);
            saveFile(strJson, fileName);
        }
        else{


            try {
                fos = context.openFileOutput("pointerStroyListJson.txt", Context.MODE_PRIVATE);

                //default mode is PRIVATE, can be APPEND etc.
                fos.write(strJson.getBytes());
                fos.close();
                Log.e(HomeActivity.LogTag, "\n SUCCESS internal storage: " +  "\n");
            } catch (FileNotFoundException error123) {
                error123.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n FAILED internal storage: " + error123.getLocalizedMessage()+ "\n");


            }
            catch (IOException error124) {
                error124.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n FAILED internal storage: " + error124.getLocalizedMessage()+ "\n");
            }
        }



    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }



    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            Log.e(HomeActivity.LogTag, "\n external storage: true" +  "\n");
            return true;


        }
        Log.e(HomeActivity.LogTag, "\n external storage:false " +  "\n");
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


   static void saveFile(String stringToSave,String encryptedFileName) {
        try {
            String fileName = encryptedFileName;
            File reqFile = createFileUnderGoStoriesDir(fileName);

            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator, encryptedFileName);
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(reqFile));
            Key yourKey = generateKey();
            Log.e(HomeActivity.LogTag, "yourKey in save method" + yourKey);
            byte[] filesBytes = encodeFile(yourKey, stringToSave.getBytes());
            bos.write(filesBytes);
            bos.flush();
            bos.close();
        } catch (FileNotFoundException error125) {
            error125.printStackTrace();
            Log.e(HomeActivity.LogTag, "error125" + error125);
        } catch (IOException error126) {
            error126.printStackTrace();
            Log.e(HomeActivity.LogTag, "error126" + error126);
        } catch (Exception error127) {
            error127.printStackTrace();
            Log.e(HomeActivity.LogTag, "error127" + error127);
        }
    }



  /*  public static SecretKey generateKey(char[] passphraseOrPin, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Number of PBKDF2 hardening rounds to use. Larger values increase
        // computation time. You should select a value that causes computation
        // to take >100ms.
        final int iterations = 1000;

        // Generate a 256-bit key
        final int outputKeyLength = 256;

        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(passphraseOrPin, salt, iterations, outputKeyLength);
        SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
        return secretKey;
    }*/

    public static Key generateKey() throws NoSuchAlgorithmException, NoSuchPaddingException {
        // Generate a 256-bit key
      //  final int outputKeyLength = 256;
     //   SecureRandom secureRandom = new SecureRandom();
        // Do *not* seed secureRandom! Automatically seeded from system entropy.
      //  KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
       // keyGenerator.init(outputKeyLength, secureRandom);
    /*    Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);*/
       Key yourKey = new SecretKeySpec(keyValue, ALGO);
       // yourKey = keyGenerator.generateKey();
    //    Log.d(HomeActivity.LogTag, "yourKey  in generator " + yourKey.toString());
        return yourKey;
    }

    public static byte[] encodeFile(Key yourKey, byte[] fileData)
            throws Exception {
        byte[] encrypted = null;
        byte[] data = yourKey.getEncoded();
        SecretKeySpec skeySpec = new SecretKeySpec(data, 0, data.length,
                ALGO);
        Cipher cipher = Cipher.getInstance(ALGO);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(
                new byte[cipher.getBlockSize()]));
        encrypted = cipher.doFinal(fileData);
        return encrypted;
    }

    public static byte[] decodeFile(Key yourKey, byte[] fileData)
            throws Exception {
        byte[] decrypted = null;

        Log.e(HomeActivity.LogTag, "yourKey in decode method" + yourKey);
        Cipher cipher = Cipher.getInstance(ALGO);

        System.out.println(" yourKey : " + yourKey);

        cipher.init(Cipher.DECRYPT_MODE, yourKey, new IvParameterSpec(
                new byte[cipher.getBlockSize()]));
        decrypted = cipher.doFinal(fileData);
        return decrypted;
    }


    static String decodeFile(String encryptedFileName) {

        String str = null;
        try {
            Key yourKey = generateKey();

            byte[] decodedData = decodeFile((SecretKey) yourKey, readFile(encryptedFileName));

            System.out.println("DECODED yourKey : " + yourKey);
            Log.e(HomeActivity.LogTag, "yourKey in decode" + yourKey);
            str = new String(decodedData);
            System.out.println("DECODED FILE CONTENTS : " + str);
        } catch (Exception error128) {
            error128.printStackTrace();
            Log.e(HomeActivity.LogTag, "error128" + error128);
        }
        return str;
    }

    public static byte[] readFile(String encryptedFileName) {
        byte[] contents = null;

        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator+ ".GoStories", encryptedFileName);
        int size = (int) file.length();
        contents = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(file));
            try {
                buf.read(contents);
                buf.close();
            } catch (IOException error129) {
                error129.printStackTrace();
                Log.e(HomeActivity.LogTag, "error129" + error129);
            }
        } catch (FileNotFoundException error130) {
            error130.printStackTrace();
            Log.e(HomeActivity.LogTag, "error130" + error130);
        }
        return contents;
    }

//
//    public static ArrayList<HashMap<String, String>> detailsToSendForPromoCode( String email,String promoCode) {
//        ArrayList<HashMap<String, String>> promoList;
//        promoList = new ArrayList<HashMap<String, String>>();
//        Log.e(HomeActivity.LogTag, "promocode " + promoCode+"  email "+email);
//        HashMap<String, String> details = new HashMap<String, String>();
//        details.put(TAG_EMAIL_ID, email);
//        details.put(TAG_PROMO_CODE, promoCode);
//
//        promoList.add(details);
//            return promoList;
//    }


    public static String sendAsHtmlMultipartForm(HashMap<String, String> details, String targetURL) {
        HttpURLConnection connection = null;
        DataOutputStream outputStream = null;
        InputStream inputStream = null;

        String twoHyphens = "--";
        String boundary = "---------------------------------------";
        String lineEnd = "\r\n";

        String result = "";


        try {
            URL url = new URL(targetURL);
            connection = (HttpURLConnection) url.openConnection();

            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            connection.setRequestMethod("POST");
            connection.setRequestProperty("Connection", "Keep-Alive");
           // connection.setRequestProperty("User-Agent", "Android Multipart HTTP Client 1.0");
            connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);

            outputStream = new DataOutputStream(connection.getOutputStream());
//            outputStream.writeBytes(twoHyphens + boundary + lineEnd);

            // Upload POST Data
            Iterator<String> keys = details.keySet().iterator();
            while (keys.hasNext()) {
                String key = keys.next();
                String value = details.get(key);

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"" + key + "\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(value);
                outputStream.writeBytes(lineEnd);
            }

            outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            if (200 != connection.getResponseCode()) {

                Log.e(HomeActivity.LogTag, "connection responce not coming" );
            }
            inputStream = connection.getInputStream();
            result = convertStreamToString(inputStream);
            Log.e(HomeActivity.LogTag, "Result is  "+result );
            inputStream.close();
            outputStream.flush();
            outputStream.close();


        } catch (Exception error1140) {
            error1140.printStackTrace();
            result = error1140.getLocalizedMessage();
            connection_error = true;
            Log.e(HomeActivity.LogTag, "error1140" + error1140);
            if (UserRegisterationActivity.inUserRegistration) {
                Utility.appendExceptionLogsOnServer(error1140, CommonVariable.registrationDetailsURL);
            }
        }

        return result;
    }

    private static void appendExceptionLogsOnServer(Exception exception, String targetURL) {

        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String msg = sw.toString();
        if (UserRegisterationActivity.inUserRegistration) {
//            String deviceName ="Device Model Name : " + android.os.Build.MODEL;
//            String deviceMan = "Device Manufacturer : " + android.os.Build.MANUFACTURER;
//            HomeActivity.deviceTechnicalDetailsString = String.valueOf(BuildConfig.VERSION_NAME);
//            HomeActivity.deviceTechnicalDetailsString = HomeActivity.deviceTechnicalDetailsString +"\n" + "\n" +Utility.getOSDetails() +"\n" + "\n" + deviceMan +"\n" + "\n" + deviceName;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MODEL;
//                deviceTechnicalDetailsString = deviceTechnicalDetailsString +"\n" + android.os.Build.MANUFACTURER;
            msg = msg + "\nCurrent App Version : " + HomeActivity.deviceTechnicalDetailsString;
        }
        appendLogsOnServer(msg, targetURL);
    }

     static void appendLogsOnServer(String msg,final String targetURL) {

        String email_ID = Utility.emailID;;
        String m_deviceId = Utility.deviceID;
        if(email_ID.equals("")){
            email_ID = UserRegisterationActivity.emailString;
        }
        final HashMap<String, String> details = new HashMap<String, String>();
        details.put(EMAIL_ID, email_ID);
        details.put(RegisteredUDId, m_deviceId);
        details.put(dataString, msg);
        Thread thread1 = new Thread(new Runnable(){
            @Override
            public void run() {
                sendAsHtmlMultipartForm(details, targetURL);
            }
        });
        thread1.start();
    }

    static void appendLogsOnServerForTechDetail(Context context, String msg, final String targetURL) {
//        String email_ID = Utility.emailID;;
//        String m_deviceId = Utility.deviceID;
            pref = context.getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, 0);
            String email_ID = pref.getString(UserRegisterationActivity.email, "");
            if(email_ID.equals("")){
                email_ID = UserRegisterationActivity.getEmail(context);
            }
            String m_deviceId = Utility.getDeviceID(context);
            final HashMap<String, String> details = new HashMap<String, String>();
            details.put(EMAIL_ID, email_ID);
            details.put(RegisteredUDId, m_deviceId);
            details.put(dataString, msg);
            Thread thread1 = new Thread(new Runnable(){
                @Override
                public void run() {
                String result = sendAsHtmlMultipartForm(details, targetURL);
           // Log.d(HomeActivity.LogTag, "result in UTILITY" + result);
            if(result.equals("SUCCESS")){
                pref.edit().putBoolean("technicalDetail", true).commit();
                HomeActivity.technicalDetail =  pref.getBoolean("technicalDetail", false);
                Log.d(HomeActivity.LogTag, "technicalDetail in UTILITY" + HomeActivity.technicalDetail);
            }
            }
        });
        thread1.start();

        }

    private static String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException error160) {
            error160.printStackTrace();
            Log.e(HomeActivity.LogTag, "error160" + error160);
        } finally {
            try {
                is.close();
            } catch (IOException error161) {
                error161.printStackTrace();
                Log.e(HomeActivity.LogTag, "error161" + error161);
            }
        }
        return sb.toString();
    }

    public static void showDialog(Activity activity) {
        EarnPointsActivity.myMsg = new TextView(activity);
        EarnPointsActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        EarnPointsActivity.myMsg.setTextSize(20);
        EarnPointsActivity.myMsg.setText(UserRegisterationActivity.not_connected);

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCustomTitle(EarnPointsActivity.myMsg);
        builder.setMessage("Please check internet connection");
        builder.setPositiveButton("OK", null);

        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
    }

    public static void showDialog(Activity activity, String msg, String title) {
        EarnPointsActivity.myMsg = new TextView(activity);
        EarnPointsActivity.myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        EarnPointsActivity.myMsg.setTextSize(20);
        EarnPointsActivity.myMsg.setText(title);
        Log.d(HomeActivity.LogTag, "Title" + title);
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setCustomTitle(EarnPointsActivity.myMsg);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", null);

        alertDialog = builder.create();
        alertDialog.setCancelable(false);
        alertDialog.show();
        Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AudioPlayerTest.dialogOnce = true;
                AudioPlayerTest.dialogExceptionOnce = true;
                alertDialog.dismiss();
            }
        });
    }

    static void appendLogsOnServer(Context context, String msg, final String targetURL){
        Log.d(HomeActivity.LogTag, "In appendLogsOnServer method");
        pref = context.getSharedPreferences(UserRegisterationActivity.MY_PREFS_NAME, 0);
        String email_ID = pref.getString(UserRegisterationActivity.email, "");
        if(email_ID.equals("")){
            email_ID = UserRegisterationActivity.getEmail(context);
        }
       // String m_deviceId = Utility.getDeviceID(context);
        final HashMap<String, String> details = new HashMap<String, String>();
        details.put(EMAIL_ID, email_ID);
        //details.put(RegisteredUDId, m_deviceId);
        details.put(dataString, msg);
        Thread thread1 = new Thread(new Runnable(){
            @Override
            public void run() {
                sendAsHtmlMultipartForm(details, targetURL);
            }
        });
        thread1.start();
    }

    static void appendExceptionLogsOnServer(Context context, Exception exception, String targetURL){
        Log.d(HomeActivity.LogTag, "In appendExceptionLogsOnServer method");
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String msg = sw.toString();
        appendLogsOnServer(context, msg, targetURL);
    }

    public static String getActiveNetworkType(Context context){
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
         isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
        String activeNetworkInfo = "";
        if(!isConnected){
          activeNetworkInfo = "No Connecton";
        }else{
            activeNetworkInfo = activeNetwork.toString();
        }
        return activeNetworkInfo;
        }

    public static String getOSDetails(){
        String osDetails = "";
        try{
            //StringBuilder builder = new StringBuilder();
            // builder.append("android : ").append(Build.VERSION.RELEASE);
            osDetails = osDetails + "Android Version Number : "+Build.VERSION.RELEASE + "\n"+"\n" ;

            Field[] fields = Build.VERSION_CODES.class.getFields();
            for (Field field : fields) {
                String fieldName = field.getName();
                int fieldValue = -1;
                fieldValue = field.getInt(new Object());
                if (fieldValue == Build.VERSION.SDK_INT) {
                    osDetails = osDetails+ "Android Version Name : "+fieldName + "\n"+"\n" ;
                    osDetails = osDetails  + "Android Version Code : "+fieldValue ;
                    break;
                    // builder.append(" : ").append(fieldName).append(" : ");
                    // builder.append("sdk=").append(fieldValue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            String errMsg = "Failed to retrieve OS details : "+e.getLocalizedMessage();
            Log.e(HomeActivity.LogTag, errMsg);
        }
        return osDetails;
    }

}
